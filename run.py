# Data Canopy Reproducibility -- Main script to run all experiments from the paper.

##
import os
import os.path
import sys
import time
##

def isFile(filename):
    return os.path.exists("./graph/"+figure_name+".pdf")



def runExperiment(experiment_name, mode):

    status = False

    # Run the experiment and see if it generated a file
    if mode == "small":
        os.system("python ./dc_exp/" + experiment_name + "_small.py")
        status = os.path.exists("./graph/"+experiment_name+"_small.pdf")
    else:
        os.system("python ./dc_exp/"+experiment_name+".py")
        status = os.path.exists("./graph/"+experiment_name+".pdf")

    return status

# Terminal color definitions
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
##

# stats
current = 1
total_to_reproduce = 7
total_reproduced = 0
success = 0
##

## Check if the correct option was selected
if len(sys.argv) != 2 or (sys.argv[1] != "small" and sys.argv[1] != "full"):
    print  bcolors.HEADER + "\n**************************************************************" + bcolors.ENDC
    print  bcolors.WARNING + "Incorrect option selected" + bcolors.ENDC
    print  bcolors.HEADER + "Usage:" + bcolors.ENDC
    print  bcolors.HEADER + "\tRun small version: " + bcolors.ENDC + "python run.py small"
    print  bcolors.HEADER + "\tRun full version:  " + bcolors.ENDC + "python run.py full"
    print  bcolors.HEADER + "**************************************************************" + bcolors.ENDC
    exit()

compile = True

### Welcome Message ###
print  bcolors.HEADER + "\n**************************************************************" + bcolors.ENDC
print  bcolors.HEADER + "Welcome to Data Canopy reproducibility script!" + bcolors.ENDC
print  bcolors.OKGREEN + "* All reproduced figures will be found in the 'graph' folder" + bcolors.ENDC
print  bcolors.HEADER + "**************************************************************" + bcolors.ENDC
###

## ## ##
# Compiling the code
if compile:
    print  bcolors.HEADER + "\n**************************************************************" + bcolors.ENDC
    print  bcolors.HEADER + "[DC] \t Compiling the code" +bcolors.ENDC
    os.system("./compile.sh")
    print  bcolors.HEADER + "[DC] \t Compilation completed" +bcolors.ENDC
## ## ##

## ## ##
# Set the version to run
if sys.argv[1] == "small":
    mode = "small"
    print  bcolors.HEADER + "[DC] \t Running the small version of the experiments" +bcolors.ENDC
else:
    mode = ""
    print  bcolors.HEADER + "[DC] \t Running the full version of the experiments" +bcolors.ENDC
## ## ##

##########################################################################################
# List of all figures
figure_names =["figure11","figure12","figure13","figure14","figure15","figure16"]

start = time.time()
###########################################################################################
# Run the experiments and reproduce them
for figure_name in figure_names:
    print  bcolors.HEADER + "\n**************************************************************" + bcolors.ENDC
    print  bcolors.HEADER + "[DC] \t Reproducing "+ figure_name + "\t("+str(current)+" of "+str(total_to_reproduce)+")"+bcolors.ENDC

    if runExperiment(figure_name,mode):
        print  bcolors.HEADER + "[DC] \t "+figure_name+" reproduced!"  + bcolors.ENDC
        print  bcolors.HEADER + "**************************************************************" + bcolors.ENDC
        success += 1
    else:
        print  bcolors.FAIL + "[DC] \t "+figure_name+" not reproduced!"  + bcolors.ENDC
        print  bcolors.HEADER + "**************************************************************" + bcolors.ENDC
    end = time.time()
    current += 1
    print "Total time elapsed: " +str(end-start) + " seconds"

###########################################################################################


### END message! ###
print  bcolors.HEADER + "\n\n\n**************************************************************" + bcolors.ENDC

print  bcolors.HEADER + "Data Canopy reproducibility script ended!" + bcolors.ENDC
print  bcolors.OKGREEN + "* All reproduced figures can be found in the 'graph' folder" + bcolors.ENDC
print  bcolors.OKGREEN + "* Have questions, contact Abdul Wasay (awasay@seas.harvard.edu)" + bcolors.ENDC
print  bcolors.OKGREEN + "* Thank you for your time." + bcolors.ENDC
print  bcolors.HEADER + "**************************************************************" + bcolors.ENDC

#
#
# #TODO: Add stats here!
# ##########################################################################################
