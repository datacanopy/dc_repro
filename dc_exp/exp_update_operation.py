import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 80
row_number = 1000000
# query_number = 10000000
lb = row_number / 20
ub = row_number / 10
query_file = "data/exp_update_operation.txt"
chunk_size = 60

query_number = 10000
result_file = "result/exp_update_operation.txt"

q_len_lb = lb
q_len_up = ub
query_list = []
for i in range(query_number):
    col1 = random.randint(0, col_number - 1)
    col2 = random.randint(0, col_number - 1)
    if col1 > col2:
        col1, col2 = col2, col1
    real_qlen = random.randint(q_len_lb, q_len_up)
    beg = random.randint(0, row_number - real_qlen - 1)
    query_list.append("%d %d %d %d %d\n" % (random.randint(1, 5), col1, col2, beg, beg + real_qlen))

def output_to_file(q_list, file_name):
    with open(file_name, 'w') as fout:
        for row in q_list:
            fout.write(row)

ans = []
for update_number in [0, 101, 526, 1111, 3333]:
    tmp = query_list + ["-1 0 0 0 0\n" for i in range(update_number)]
    random.shuffle(tmp)
    output_to_file(tmp, query_file)
    os.system("./exp_query_process_updates %d %d %d %s > %s"
              % (col_number, row_number, chunk_size, query_file, result_file))
    with open(result_file) as fin:
        content = fin.read().split('\n')[4:]
        ans.append([float(content[0]), float(content[1])])
pd.DataFrame(ans, columns=["Query time", "Update time"]).to_csv("result/exp_update_operation.csv", index=False)
