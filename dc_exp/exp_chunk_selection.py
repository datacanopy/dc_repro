import query_generator
import os
import random

def generate_uniform_query(col_number, row_number, query_number, q_len_lb, q_len_up, query_file):
    fout = open(query_file, 'w')
    fout.write("4 0 1 %d %d\n" % (0, row_number - 1))
    for i in range(query_number):
        col1 = random.randint(0, col_number - 1)
        col2 = random.randint(0, col_number - 1)
        if col1 > col2:
            col1, col2 = col2, col1
        real_qlen = random.randint(q_len_lb, q_len_up)
        beg = random.randint(0, row_number - real_qlen - 1)
        fout.write("%d %d %d %d %d\n" % (random.randint(1, 5), col1, col2, beg, beg + real_qlen))
        #if i != query_number - 1:
        #    fout.write("-1 0 0 0 0\n")

    fout.close()

col_number = 2
row_number = 1000000
query_number = 1000000
ub = row_number / 10
lb = row_number / 10
# lb = row_number / 100 * 5

chunk_size_set = [1, 2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200,
                  210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400,
                  410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 1000, 2000, 4000, 8000]
# chunk_size_set = [430, 440, 450, 460, 470, 480, 490, 500, 1000, 2000, 4000, 8000]
# chunk_size_set = [1 << i for i in range(9)]

# row_number = 1000000
# generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_1M.txt')
# query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_1M.txt')
# query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, 'data/data_z_u_1M.txt')
# query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, 'data/data_u_z_1M.txt')
# query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, 'data/data_z_z_1M.txt')

# for chunk_size in chunk_size_set:
#     cmd = "./exp_query_process_linked %d %d %d %s > %s" % (col_number, row_number, chunk_size, 'data/data_u_u_1M.txt', 'result/u_u_1M_%d.txt' % chunk_size)
#     print cmd
#     os.system(cmd)
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_u_1M.txt', 'result/z_u_1M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_u_z_1M.txt', 'result/u_z_1M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_z_1M.txt', 'result/z_z_1M_%d.txt' % chunk_size))

row_number *= 10
ub = row_number / 10
lb = row_number / 10
# generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_10M.txt')
# query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_10M.txt')
# query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, 'data/data_z_u_10M.txt')
# query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, 'data/data_u_z_10M.txt')
# query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, 'data/data_z_z_10M.txt')
# #
# for chunk_size in chunk_size_set:
#     cmd = "./exp_query_process_linked %d %d %d %s> %s" % (col_number, row_number, chunk_size, 'data/data_u_u_10M.txt', 'result/u_u_10M_%d.txt' % chunk_size)
#     print cmd
#     os.system(cmd)
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_u_10M.txt', 'result/z_u_10M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_u_z_10M.txt', 'result/u_z_10M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_z_10M.txt', 'result/z_z_10M_%d.txt' % chunk_size))

row_number *= 10
ub = row_number / 10
lb = row_number / 10
# generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_100M.txt')
# query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_100M.txt')
# query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, 'data/data_z_u_100M.txt')
# query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, 'data/data_u_z_100M.txt')
# query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, 'data/data_z_z_100M.txt')
# #
# for chunk_size in chunk_size_set:
#     cmd = "./exp_query_process_linked %d %d %d %s> %s" % (col_number, row_number, chunk_size, 'data/data_u_u_100M.txt', 'result/u_u_100M_%d.txt' % chunk_size)
#     print cmd
#     os.system(cmd)
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_u_100M.txt', 'result/z_u_100M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_u_z_100M.txt', 'result/u_z_100M_%d.txt' % chunk_size))
    # os.system("./exp_query_process_linked %d %d %d %s> %s"
    #           % (col_number, row_number, chunk_size, 'data/data_z_z_100M.txt', 'result/z_z_100M_%d.txt' % chunk_size))

row_number *= 10
ub = row_number / 10
lb = row_number / 10
generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/data_u_u_1000M.txt')
for chunk_size in chunk_size_set:
    cmd = "./exp_query_process_linked %d %d %d %s> %s" % (col_number, row_number, chunk_size, 'data/data_u_u_1000M.txt', 'result/u_u_1000M_%d.txt' % chunk_size)
    print cmd
    os.system(cmd)


import pandas as pd
chunk_size_set = [1, 2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200,
                  210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400,
                  410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 1000, 2000, 4000, 8000]
# chunk_size_set = [1 << i for i in range(9)]
query_number = 1000000
# data_set = ['u_u', 'u_z', 'z_u', 'z_z']
data_set = ['u_u']
data_size = [1, 10, 100, 1000]
# for data in data_set:
#     for row in data_size:
#         ans = [[0.0 for j in range(len(chunk_size_set))] for i in range(query_number / 100)]
#         for j in range(len(chunk_size_set)):
#             chunk_size = chunk_size_set[j]
#             with open("result/%s_%dM_%d.txt" % (data, row, chunk_size)) as fin:
#                 content = fin.read().split('\n')[4:-5]
#                 for i in range(query_number):
#                     ans[i/100][j] += float(content[i].split(' ')[0]) * 1000
#         pd.DataFrame(ans, index = [i * 100 for i in range(query_number / 100)], columns=chunk_size_set).to_csv('result/%s_%dM.csv' % (data, row))


for i in range(len(data_set)):
    ans = [[0.0 for data in range(len(data_size))] for j in range(len(chunk_size_set))]
    for row in range(len(data_size)):
        for j in range(len(chunk_size_set)):
            chunk_size = chunk_size_set[j]
            print chunk_size
            with open("result/%s_%dM_%d.txt" % (data_set[i], data_size[row], chunk_size)) as fin:
                content = fin.read().split('\n')[5:]
                for r in range(query_number):
                    ans[j][row] += float(content[r].split(' ')[0])
    pd.DataFrame(ans, index=chunk_size_set, columns=['1M', '10M', '100M', '1B']).to_csv('result/%s_chunk_row_time.csv' % data_set[i])

# data_size = [10]
# data_set = ['u_u', 'u_z', 'z_u', 'z_z']
# for row in range(len(data_size)):
#     ans = [[0.0 for data in range(len(data_set))] for j in range(len(chunk_size_set))]
#     for i in range(len(data_set)):
#         for j in range(len(chunk_size_set)):
#             chunk_size = chunk_size_set[j]
#             with open("result/%s_%dM_%d.txt" % (data_set[i], data_size[row], chunk_size)) as fin:
#                 content = fin.read().split('\n')[4:]
#                 for r in range(query_number):
#                     ans[j][i] += float(content[r].split(' ')[0])
#     pd.DataFrame(ans, index=chunk_size_set, columns=data_set).to_csv('result/%dM_chunk_workload_time.csv' % data_size[row])
