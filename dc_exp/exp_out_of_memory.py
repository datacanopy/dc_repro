import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 90
row_number = 10000000
# query_number = 10000000
lb = row_number / 4
ub = row_number / 2
query_file = "data/exp_out_of_memory.txt"
memory_limit = 8589934592
chunk_size = 60
result_file = "result/exp_out_of_memory.txt"

# def query_generate(query_file, col_number, row_number):
#     fout = open(query_file, "w")
#     tmp = []
#     for i in range(col_number):
#         for j in range(i + 1, col_number):
#             if i != j :
#                 tmp.append("%d %d %d %d %d\n" % (5, i, j, 0, row_number - 1))
#     random.shuffle(tmp)
#     for i in tmp:
#         fout.write(i)
#     fout.close()
#
# result_file = "result/exp_out_of_memory_1.txt"
# query_generate(query_file, col_number, row_number)
# os.system("./exp_query_process_out_of_memory %d %d %d %s %d > %s"
#           % (col_number, row_number, chunk_size, query_file, memory_limit, result_file))

# with open(result_file) as fin:
#     content = fin.read().split('\n')[5:-1]
#     query_number = len(content)
#     step = query_number / 158
#     ans = [[0.0, 0.0] for j in range(query_number)]
#     index = []
#     for r in range(query_number):
#         ans[r][0] += float(content[r].split(' ')[-1])
#         ans[r][1] += float(content[r].split(' ')[0]) * 1000
#         # index.append(int(content[r].split(' ')[3]))
#     df = pd.DataFrame(ans, index=range(query_number))
#     df = df.groupby(lambda x: x / step).sum()
#     df /= step
#     df.index = range(step - 1, query_number, step)#range(step, step + query_number, step)
#     df.to_csv('result/exp_out_of_memory_1.csv')

# for i in range(2, 19):
#     os.system("rm dump_%d*" % i)
# os.system("rm dump_*")

query_number = 100000
result_file = "result/exp_out_of_memory.txt"
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)
os.system("build/exp_query_process_out_of_memory %d %d  %d %s %d > %s"
          % (col_number, row_number, chunk_size, query_file, memory_limit, result_file))

flag = -1

with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = 0
    ans = []
    for r in range(len(content)):
        if content[r][0] != 'P' and content[r][0] != 'S':
            ans.append([float(content[r].split(' ')[0]) * 1000])
            query_number += 1
        elif content[r][0] == 'P':
            if content[r][5] == '2' and flag == -1:
                flag = query_number
    step = query_number / 200
    df = pd.DataFrame(ans, index=range(query_number))
    df = df.groupby(lambda x: x / step).sum()
    df /= step
    df.index = range(step, step + query_number, step)
    df.to_csv('result/exp_out_of_memory.csv')

for i in range(10, 100):
    os.system("rm dump_%d*" % i)
os.system("rm dump_*")

os.system("gnuplot -e \"boundary = %s\" dc_exp/gnuplot/exp/exp_out_of_memory.gp" % flag)
