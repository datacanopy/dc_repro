from sqlalchemy import create_engine
from cubes.tutorial.sql import create_table_from_csv
import data as dt
import numpy as np
import pandas as pd
import sqlite3
import math
import sys
import json
from cubes import Workspace, Cell, RangeCut
import time

class CubesAdapter:
    def __init__(self, db_url, table_name):
        self.workspace = Workspace()
        self.workspace.register_default_store("sql", url = db_url)
        self.table_name = table_name


    def query(self, aggregate_type, col1, col2, left, right):
        model = self.__generate_model(aggregate_type, col1, col2)
        cube_name = model["cubes"][0]["name"]
        self.workspace.import_model(model)
        browser = self.workspace.browser(cube_name)
        # MEAN
        if aggregate_type == 1:
            res = browser.aggregate(Cell(self.workspace.cube(cube_name), [RangeCut("id", [left+1], [right])]),
                                    aggregates=["col%d_avg" % col1])
            return float(res.summary["col%d_avg" % col1])
        # VARIANCE
        elif aggregate_type == 2:
            res = browser.aggregate(Cell(self.workspace.cube(cube_name), [RangeCut("id", [left + 1], [right])]),
                                    aggregates=["col%d_avg" % col1, "col%d_col%d_avg" % (col1, col1)])
            return float(res.summary["col%d_col%d_avg" % (col1, col1)] - res.summary["col%d_avg" % col1])
        # STANDARD_DEVIATION
        elif aggregate_type == 3:
            res = browser.aggregate(Cell(self.workspace.cube(cube_name), [RangeCut("id", [left + 1], [right])]),
                                    aggregates=["col%d_avg" % col1, "col%d_col%d_avg" % (col1, col1)])
            return math.sqrt(res.summary["col%d_col%d_avg" % (col1, col1)] - res.summary["col%d_avg" % col1])
        elif aggregate_type == 4:
            res = browser.aggregate(Cell(self.workspace.cube(cube_name), [RangeCut("id", [left + 1], [right])]),
                                    aggregates=["rows_count",
                                                "col%d_avg" % col1, "col%d_avg" % col2,
                                                "col%d_col%d_avg" % (col1, col1), "col%d_col%d_avg" % (col2, col2),
                                                "col%d_col%d_avg" % (col1, col2)])
            n = res.summary["rows_count"]
            avg1 = res.summary["col%d_avg" % col1]
            avg2 = res.summary["col%d_avg" % col2]
            avg11 = res.summary["col%d_col%d_avg" % (col1, col1)]
            avg22 = res.summary["col%d_col%d_avg" % (col2, col2)]
            avg12 = res.summary["col%d_col%d_avg" % (col1, col2)]
            std1 = math.sqrt(avg11 - avg1 * avg1)
            std2 = math.sqrt(avg22 - avg2 * avg2)
            return float(avg12 * n - avg1 * avg2 * n) / (std1 * std2)
        elif aggregate_type == 5:
            res = browser.aggregate(Cell(self.workspace.cube(cube_name), [RangeCut("id", [left + 1], [right])]),
                                    aggregates=["rows_count",
                                                "col%d_avg" % col1, "col%d_avg" % col2,
                                                "col%d_col%d_avg" % (col1, col2)])
            n = res.summary["rows_count"]
            avg1 = res.summary["col%d_avg" % col1]
            avg2 = res.summary["col%d_avg" % col2]
            avg12 = res.summary["col%d_col%d_avg" % (col1, col2)]
            return float(avg12 / (avg1 * avg2))

    def __generate_model(self, aggregate_type, col1, col2):
        model = {
            "dimensions": [
                {
                    "name": "id",
                    "role": "time",
                }],
            "cubes": [
                {
                    "name": "%d_%d_%d" % (aggregate_type, col1, col2),
                    "dimensions": ["id"],
                    "measures": [
                        {
                            "name": ("col%d" % col1),
                            "label": ("col%d" % col1)
                        }
                    ],
                    "aggregates": [
                        {
                            "name": "rows_count",
                            "function": "count",
                        },
                        {
                            "name": "col%d" % col1 + "_avg",
                            "function": "avg",
                            "measure": "col%d" % col1
                        }
                    ],
                    "mappings": {
                        "id": "id",
                        ("%s.%s" % (self.table_name, "col%d" % col1)): ("col%d" % col1),
                    },
                    "fact": self.table_name,
                }
            ]
        }
        # MEAN
        if aggregate_type == 1:
            pass
        # VARIANCE
        elif aggregate_type == 2:
            model["cubes"][0]["aggregates"].append(
                {
                    "name": "col%d" % col1 + '_' + "col%d" % col1 + "_avg",
                    "expression": "avg(%s * %s)" % ("col%d" % col1, "col%d" % col1),
                }
            )
        #STANDARD_DEVIATION
        elif aggregate_type == 3:
            model["cubes"][0]["aggregates"].append(
                {
                    "name": "col%d" % col1 + '_' + "col%d" % col1 + "_avg",
                    "expression": "avg(%s * %s)" % ("col%d" % col1, "col%d" % col1),
                }
            )
        #CORRELATION
        elif aggregate_type == 4:
            if col1 != col2:
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": ("col%d" % col2),
                        "label": ("col%d" % col2)
                    }
                )
                model["cubes"][0]["mappings"]["%s.%s" % (self.table_name, "col%d" % col2)] = "col%d" % col2
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": "col%d" % col2 + "_avg",
                        "function": "avg",
                        "measure": "col%d" % col2
                    }
                )
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": "col%d" % col1 + '_' + "col%d" % col1 + "_avg",
                        "expression": "avg(%s * %s)" % ("col%d" % col1, "col%d" % col1),
                    }
                )
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": "col%d" % col2 + '_' + "col%d" % col2 + "_avg",
                        "expression": "avg(%s * %s)" % ("col%d" % col2, "col%d" % col2),
                    }
                )
            model["cubes"][0]["aggregates"].append(
                {
                    "name": "col%d" % col1 + '_' + "col%d" % col2 + "_avg",
                    "expression": "avg(%s * %s)" % ("col%d" % col1, "col%d" % col2),
                }
            )
        #COVARIANCE
        elif aggregate_type == 5:
            if col1 != col2:
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": ("col%d" % col2),
                        "label": ("col%d" % col2)
                    }
                )
                model["cubes"][0]["mappings"]["%s.%s" % (self.table_name, "col%d" % col2)] = "col%d" % col2
                model["cubes"][0]["aggregates"].append(
                    {
                        "name": "col%d" % col2 + "_avg",
                        "function": "avg",
                        "measure": "col%d" % col2
                    }
                )
            model["cubes"][0]["aggregates"].append(
                {
                    "name": "col%d" % col1 + '_' + "col%d" % col2 + "_avg",
                    "expression": "avg(%s * %s)" % ("col%d" % col1, "col%d" % col2),
                }
            )
        return model


class ExpCubes:
    def __init__(self, query_file = "query.txt", data_file = "data.csv"):
        data = pd.read_csv(data_file, delimiter=',', index_col=False)

        fin = open(query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()

        fields = []
        for col in range(data.shape[1]):
            fields.append((data.columns[col], "float"))
        engine = create_engine('postgresql://awasay:15121991@localhost:5432/dc')
        create_table_from_csv(engine,
                              data_file,
                              table_name="test",
                              fields=fields,
                              create_id=True
                              )

    def run(self):
        result = []
        ca = CubesAdapter("postgresql://awasay:15121991@localhost:5432/dc", "test")
        start_time = time.time()
        for query in self.query_list:
            if query[0] == -1:
                result.append((time.time() - start_time))
                start_time = time.time()
            else:
                ca.query(query[0], query[1], query[2], query[3], query[4])
        result.append((time.time() - start_time))
        return result