import getopt
import sys
import pandas as pd
import numpy as np
import os
import monetdb.sql
import time

class ExpMonetdb:
    def __init__(self, query_file, col_number, row_number):
        self.col_number = col_number
        #data = pd.read_csv(data_file, nrows=2)
        #column_number = data.shape[1]
        self.connection = monetdb.sql.connect(username="monetdb", password="monetdb", hostname="localhost", port=65432,
                                         database="dc")
        self.cursor = self.connection.cursor()

        if self.cursor.execute("select name from tables where name = 'test';") > 0:
            self.cursor.execute("select count(*) from test;")
            if self.cursor.fetchone()[0] != row_number:
                self.cursor.execute("drop table test;")
            else:
                self.cursor.execute("select * from test limit 1;")
                if len(self.cursor.fetchone()) != col_number + 1:
                    self.cursor.execute("drop table test;")
        if self.cursor.execute("select name from tables where name = 'test';") == 0:
            data_file = "tmp.csv"
            pd.DataFrame(np.random.uniform(size=(row_number, col_number)).astype("double")).to_csv(data_file, index=False)

            create_table_sql = 'create table test ('
            for i in range(col_number):
                if i > 0:
                    create_table_sql += ','
                create_table_sql += "col%d double" % i
            create_table_sql += ');'
            self.cursor.execute(create_table_sql)
            self.cursor.execute("copy OFFSET 2 into test from '%s' USING DELIMITERS ',';" % os.path.abspath(data_file))
            self.cursor.execute("alter table test add column id int auto_increment primary key;")
            os.system("rm %s" % data_file)

        self.connection.commit()

        self.query_file = query_file


    def run(self):
        fin = open(self.query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()

        # for i in range(self.col_number):
        #     self.connection.execute("PREPARE select avg(col%d) from test where id BETWEEN ? and ?;" % i)
        # for i in range(self.col_number):
        #     self.connection.execute("PREPARE select var_pop(col%d) from test where id BETWEEN ? and ?;" % i)
        # for i in range(self.col_number):
        #     self.connection.execute("PREPARE select stddev_pop(col%d) from test where id BETWEEN ? and ?;" % i)
        #
        # for i in range(self.col_number):
        #     for j in range(self.col_number):
        #         self.connection.execute("PREPARE select corr(col%d, col%d) from test where id BETWEEN ? and ?;" % (i, j))


        result = []

        for query in self.query_list:
            col1 = query[1]
            col2 = query[2]
            left = query[3]
            right = query[4]
            start_time = time.time()
            if query[0] == 1:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2, left, right))
                self.cursor.execute("select avg(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 2:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + self.col_number, left, right))
                self.cursor.execute("select var_pop(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 3:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + 2 *self.col_number, left, right))
                self.cursor.execute("select stddev_pop(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 4:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                self.cursor.execute(
                    "select corr(col%d, col%d) from test where id BETWEEN %d and %d" % (col1, col2, left, right))
            elif query[0] == 5:
                # self.cursor.execute(
                #     "EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                self.cursor.execute(
                    "select corr(col%d, col%d) from test where id BETWEEN %d and %d" % (col1, col2, left, right))
            result.append((time.time() - start_time))
        return result

optlist, args = getopt.getopt(sys.argv[1:], "f:r:c:aX:o:")
col_number = row_number = query_file = avg = output_file = None
chunk = 1
for o, a in optlist:
    if o == '-f':
        query_file = a
    elif o == '-c':
        col_number = int(a)
    elif o == '-r':
        row_number = int(a)
    elif o == '-a':
        avg = True
    elif o == '-X':
        chunk = int(a)
    elif o == '-o':
        output_file = a

if output_file is None:
    output_file = query_file + "_monetdb_time.csv"

monetdb_exp = ExpMonetdb(query_file, col_number, row_number)
res = monetdb_exp.run()

ans = []
for i in range(len(res) / chunk):
    tmp = 0.0
    for j in range(chunk):
        tmp += res[i * chunk + j]
    if avg:
        tmp /= chunk
    ans.append(tmp)
df = pd.DataFrame(ans)
if avg:
    df.columns = ["Average Time(s)"]
else:
    df.columns = ["Total Time(s)"]
df.to_csv(output_file, index=False)
