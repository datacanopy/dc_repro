import numpy as np
import os
import data
import pandas as pd
import plot
import random

col_number = 100
row_number = 100000000
chunk_size = row_number * 0.001
# data_file = "data/exp4_data.csv"
# data.generateData(data_file, row_number, col_number)

query_file = "data/exp6_query_uniform.txt"
query_number = 10000

res = []

column_name = ["2%", "4%", "6%", "8%", "10%"]
mat = [[] for i in range(query_number)]

for i in [0.02, 0.04, 0.06, 0.08, 0.10]:
    fout = open(query_file, 'w')
    q_len = row_number * i
    for i in range(query_number):
        col1 = random.randint(0, col_number - 1)
        col2 = random.randint(0, col_number - 1)
        if col1 > col2:
            col1, col2 = col2, col1
        real_qlen = q_len
        beg = random.randint(0, row_number - real_qlen - 1)
        fout.write("%d %d %d %d %d\n" % (random.randint(1, 5), col1, col2, beg, beg + real_qlen))
        if i != query_number - 1:
            fout.write("-1 0 0 0 0\n")

    fout.close()
    result = []
    log = os.popen("./exp_query_process %d %d %d %s" % (col_number, row_number, chunk_size, query_file)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        result.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))
    for i in range(len(result)):
        mat[i].append(result[i][0])

df = pd.DataFrame(mat, index=range(query_number))
df.columns = column_name
df.to_csv("result/exp6_uniform.csv")
# plot.PlotBarChartFromExcel(df, "graph/exp4", "Query Sequence", "Total response time (s)")