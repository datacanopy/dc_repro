import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 100
row_number = 1000000
data_file = "data/exp8_data.csv"
query_file = "data/exp8_query.txt"
query_number = 10000
data.generateData(data_file, row_number, col_number)
query_generator.generate_good_query(col_number, row_number, 10, query_file)
print "Exp8 queries generated"

res = []

import r_util
import monetdb_util
import dc_util
import numpy_util

column_name = []

numpy_exp = numpy_util.ExpNumpy(query_file, data_file)
column_name.append('NumPy (Python)')
exp_R = r_util.ExpR(query_file, data_file)
column_name.append('Modeltools (R)')
exp_monetdb = monetdb_util.ExpMonetdb(query_file, data_file)
column_name.append('MonteDB')
dc_exp = dc_util.ExpDC(query_file, data_file, row_number * 0.001)
column_name.append('Data Canopy')


res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())
mat = [[0 for i in range(len(res))] for j in range(query_number)]
for i in range(query_number):
    for j in range(len(res) - 1):
        mat[i][j] = res[j][i]
    mat[i][len(res) - 1] = res[len(res) - 1][i][0]
df = pd.DataFrame(mat)
df.columns = column_name
df.index = range(query_number)
df.to_csv("result/exp8.csv")
# plot.PlotBarChartFromExcel(df, "graph/exp2", "Query Sequence", "Total response time (s)")