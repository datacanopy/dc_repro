import query_generator
import os
import pandas as pd

column_size = 100
row_size = 100000000
query_number = 10000
chunk_size = [50, 500, 5000, 50000]
lb = row_size / 100 * 5
ub = row_size / 10
query_file = "data/exp_chunk.txt"
query_generator.generate_uniform_query(column_size, row_size, query_number, lb, ub, query_file)

mat = [[] for i in range(query_number)]
vir_mat = [[] for i in range(query_number)]

for chunk in chunk_size:
    result = []
    log = os.popen("./exp_query_process %d %d %d %s" % (column_size, row_size, chunk, query_file)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        result.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2]),
                       float(row.split(' ')[3])))
    for i in range(len(result)):
        mat[i].append(result[i][0])
    for i in range(len(result)):
        vir_mat[i].append(result[i][3])


df = pd.DataFrame(mat, index=range(query_number))
df.columns = chunk_size
df.to_csv("result/exp_chunk_time.csv")

df = pd.DataFrame(vir_mat, index=range(query_number))
df.columns = chunk_size
df.to_csv("result/exp_chunk_mem.csv")
