import getopt
import sys
import pandas as pd
import os
import r_util
class ExpR:
    def __init__(self, query_file, col_number, row_number):
        self.query_file = query_file
        self.col_number = col_number
        self.row_number = row_number



    def run(self):
        result = []
        log = os.popen("Rscript ./query_process.R %d %d %s" % (self.col_number, self.row_number, self.query_file)).read()
        log_list = log.split('\n')[:-1]
        for row in range(1, len(log_list), 2):
            result.append(float(log_list[row]))
        return result

optlist, args = getopt.getopt(sys.argv[1:], "f:r:c:aX:o:")
col_number = row_number = query_file = avg = output_file = None
chunk = 1
for o, a in optlist:
    if o == '-f':
        query_file = a
    elif o == '-c':
        col_number = int(a)
    elif o == '-r':
        row_number = int(a)
    elif o == '-a':
        avg = True
    elif o == '-X':
        chunk = int(a)
    elif o == '-o':
        output_file = a

if output_file is None:
    output_file = query_file + "_r_time.csv"

r_exp = r_util.ExpR(query_file, col_number, row_number)
res = r_exp.run()

ans = []
for i in range(len(res) / chunk):
    tmp = 0.0
    for j in range(chunk):
        tmp += res[i * chunk + j]
    if avg:
        tmp /= chunk
    ans.append(tmp)
df = pd.DataFrame(ans)
if avg:
    df.columns = ["Average Time(s)"]
else:
    df.columns = ["Total Time(s)"]
df.to_csv(output_file, index=False)
