import monetdb.sql
import time
import pandas as pd
import numpy as np
import os
import random

class ExpMonetdb:
    def __init__(self, query_file, col_number, row_number):
        self.col_number = col_number
        #data = pd.read_csv(data_file, nrows=2)
        #column_number = data.shape[1]
        self.connection = monetdb.sql.connect(username="monetdb", password="monetdb", hostname="localhost", port=65432,
                                         database="dc")
        self.cursor = self.connection.cursor()

        while True:
            self.table_name = 'test%07d' % random.randint(0, 1000000)
            if self.cursor.execute("select name from tables where name = '%s';" % self.table_name) == 0:
                break

        data_file = "tmp.csv"
        pd.DataFrame(np.random.uniform(size=(row_number, col_number)).astype("double")).to_csv(data_file, index=False)

        create_table_sql = 'create table %s (' % self.table_name
        for i in range(col_number):
            if i > 0:
                create_table_sql += ','
            create_table_sql += "col%d double" % i
        create_table_sql += ');'
        self.cursor.execute(create_table_sql)
        self.cursor.execute("copy OFFSET 2 into %s from '%s' USING DELIMITERS ',';" % (self.table_name, os.path.abspath(data_file)))
        self.cursor.execute("alter table %s add column id int auto_increment primary key;" % self.table_name)
        os.system("rm %s" % data_file)

        self.connection.commit()

        self.query_file = query_file

        os.system('echo "create table %s"  >> monetdb_history.log' % self.table_name)

    def __del__(self):
        self.cursor.execute("drop table %s cascade " % self.table_name)
        os.system('echo "drop table %s"  >> monetdb_history.log' % self.table_name)

    def run(self):
        fin = open(self.query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()

        result = []

        for query in self.query_list:
            col1 = query[1]
            col2 = query[2]
            left = query[3]
            right = query[4]
            start_time = time.time()
            if query[0] == 1:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2, left, right))
                self.cursor.execute("select avg(col%d) from %s where id BETWEEN %d and %d" % (col1, self.table_name, left, right))
            elif query[0] == 2:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + self.col_number, left, right))
                self.cursor.execute("select var_pop(col%d) from %s where id BETWEEN %d and %d" % (col1, self.table_name, left, right))
            elif query[0] == 3:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + 2 *self.col_number, left, right))
                self.cursor.execute("select stddev_pop(col%d) from %s where id BETWEEN %d and %d" % (col1, self.table_name, left, right))
            elif query[0] == 4:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                self.cursor.execute(
                    "select corr(col%d, col%d) from %s where id BETWEEN %d and %d" % (col1, col2, self.table_name, left, right))
            elif query[0] == 5:
                # self.cursor.execute(
                #     "EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                self.cursor.execute(
                    "select corr(col%d, col%d) from %s where id BETWEEN %d and %d" % (col1, col2, self.table_name, left, right))
            result.append((time.time() - start_time))
        return result