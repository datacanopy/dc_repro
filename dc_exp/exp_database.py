from postgre_util import *
import pandas as pd
column_size = 100
row_size = 1000000
query_number = 100
lb = row_size / 100
ub = row_size / 10
query_file = "data/exp_database.txt"

# pg_exp = ExpPostgre(query_file)
# res = pg_exp.run()
# df = pd.DataFrame(res, index=range(query_number))
# df.columns = ['PostgreSQL']
# df.to_csv("result/exp_database.csv")
import query_generator
# query_file = "data/exp_database_short.txt"
# query_generator.generate_uniform_query(column_size, row_size, query_number * 10, lb / 10, ub / 10, query_file)
query_file = "data/exp_database_chunk.txt"
pg_exp = ExpPostgre(query_file)
res = pg_exp.run()
df = pd.DataFrame(res, )#index=range(query_number))
df.columns = ['PostgreSQL']
df.to_csv("result/exp_database_shorter.csv")