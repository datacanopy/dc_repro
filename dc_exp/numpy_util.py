import time
import numpy as np
class ExpNumpy:
    def __init__(self, query_file, col_number, row_number):
        self.data = np.random.uniform(size=(col_number, row_number)).astype("double")
        self.query_file = query_file

    def run(self):
        result = []
        fin = open(self.query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()
        start_time = time.time()
        for query in self.query_list:
            start_time = time.time()
            if query[0] == 1:
                np.mean(self.data[query[1], query[3]:query[4]])
            elif query[0] == 2:
                np.var(self.data[query[1], query[3]:query[4]])
            elif query[0] == 3:
                np.std(self.data[query[1], query[3]:query[4]])
            elif query[0] == 4:
                np.corrcoef(self.data[query[1], query[3]:query[4]], self.data[query[2], query[3]:query[4]])
            elif query[0] == 5:
                np.cov(self.data[query[1], query[3]:query[4]], self.data[query[2], query[3]:query[4]])
            result.append((time.time() - start_time))
        return result
