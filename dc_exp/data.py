import numpy as np
import os

def generateData(filename, num_rows, num_cols, force = False):
    # Generate random data between 0 and ~MAXINT
    if (not os.path.exists(filename) or force):
        os.system("./data_generator %d %d %s" % (num_cols, num_rows, filename))

    print "Data Generated"

