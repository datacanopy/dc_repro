import pandas as pd
import sys
import numpy as np
import plot
if __name__ == '__main__':
    file_name = sys.argv[1]
    part = int(sys.argv[2])
    if len(sys.argv) > 3:
        top_number = int(sys.argv[3])
        data = pd.read_csv("result/%s.csv" % file_name)[:top_number]
    else:
        data = pd.read_csv("result/%s.csv" % file_name)
    res = data.groupby(lambda x: x / (data.shape[0] / part)).aggregate(np.average).iloc[:,1:]
    # res /= (data.shape[0] / part)
    res *= 1000
    res.index = [i * (data.shape[0] / part) for i in range(1, part + 1)]
    tmp_name = "result/.csv"
    res.to_csv(tmp_name)

