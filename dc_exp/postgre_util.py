import numpy as np
import pandas as pd
import time
import psycopg2 as pg


class ExpPostgre:
    def __init__(self, query_file, col_number=100):
        self.query_file = query_file
        self.col_number = col_number

    def run(self):
        result = []
        fin = open(self.query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()

        # Define connection string
        conn_string = "host='localhost' dbname='dc' user='awasay' password='15121991'"

        # print the connection string used to connect
        print "Connecting to database\n	->%s" % (conn_string)

        # get a connection, if a connect cannot be made an exception will be raised here
        conn = pg.connect(conn_string)

        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        cur = conn.cursor()
        print "Connected!\n"

        # for i in range(self.col_number):
        #     cur.execute(
        #         "prepare col%d_mean(int, int) as with workload as (select col%d from test offset $1 limit $2) select avg(col%d) from workload;" % (
        #         i, i, i))
        #
        # for i in range(self.col_number):
        #     cur.execute(
        #         "prepare col%d_var(int, int) as with workload as (select col%d from test offset $1 limit $2) select var_pop(col%d) from workload;" % (
        #         i, i, i))
        #
        # for i in range(self.col_number):
        #     cur.execute(
        #         "prepare col%d_stddev(int, int) as with workload as (select col%d from test offset $1 limit $2) select stddev(col%d) from workload;" % (
        #         i, i, i))
        #
        # for i in range(self.col_number):
        #     cur.execute(
        #         "prepare col%d_corr(int, int) as with workload as (select col%d from test offset $1 limit $2) select corr(col%d, col%d) from workload;" % (
        #         i, i, i, i))
        #
        # for i in range(self.col_number):
        #     cur.execute(
        #         "prepare col%d_cov(int, int) as with workload as (select col%d from test offset $1 limit $2) select covar_pop(col%d, col%d) from workload;" % (
        #         i, i, i, i))
        #
        # for i in range(self.col_number):
        #     for j in range(self.col_number):
        #         if i != j:
        #             cur.execute(
        #                 "prepare col%d_col%d_corr(int, int) as with workload as (select col%d, col%d from test offset $1 limit $2) select corr(col%d, col%d) from workload;" % (
        #                     i, j, i, j, i, j))
        #
        # for i in range(self.col_number):
        #     for j in range(self.col_number):
        #         if i != j:
        #             cur.execute(
        #                 "prepare col%d_col%d_cov(int, int) as with workload as (select col%d, col%d from test offset $1 limit $2) select covar_pop(col%d, col%d) from workload;" % (
        #                     i, j, i, j, i, j))
        #
        #
        # for query in self.query_list:
        #     query_statement = "EXECUTE "
        #     if query[0] == 1:
        #         query_statement += "col%d_mean(" % query[1]
        #     elif query[0] == 2:
        #         query_statement += "col%d_var(" % query[1]
        #     elif query[0] == 3:
        #         query_statement += "col%d_stddev(" % query[1]
        #     elif query[0] == 4:
        #         if query[1] == query[2]:
        #             query_statement += "col%d_corr(" % query[1]
        #         else:
        #             query_statement += "col%d_col%d_corr(" % (query[1], query[2])
        #     elif query[0] == 5:
        #         if query[1] == query[2]:
        #             query_statement += "col%d_cov(" % query[1]
        #         else:
        #             query_statement += "col%d_col%d_cov(" % (query[1], query[2])
        #
        #     query_statement += "%d, %d);" % (query[3], query[4] - query[3] + 1)
        #     start_time = time.time()
        #     cur.execute(query_statement,)
        #     result.append((time.time() - start_time))

        for query in self.query_list:
            col1 = query[1]
            col2 = query[2]
            left = query[3]
            right = query[4]
            start_time = time.time()
            if query[0] == 1:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2, left, right))
                cur.execute("select avg(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 2:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + self.col_number, left, right))
                cur.execute("select var_pop(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 3:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 + 2 + 2 *self.col_number, left, right))
                cur.execute("select stddev(col%d) from test where id BETWEEN %d and %d" % (col1, left, right))
            elif query[0] == 4:
                # self.cursor.execute("EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                cur.execute(
                    "select corr(col%d, col%d) from test where id BETWEEN %d and %d" % (col1, col2, left, right))
            elif query[0] == 5:
                # self.cursor.execute(
                #     "EXEC %d(%d, %d);" % (col1 * self.col_number + col2 + 2 + 3 * self.col_number, left, right))
                cur.execute(
                    "select covar_pop(col%d, col%d) from test where id BETWEEN %d and %d" % (col1, col2, left, right))
            result.append((time.time() - start_time))
        return result
