import query_generator
import os
import random
import pandas as pd

col_number = 10
row_number = 1000000
query_number = 2000
# lb = row_number / 100 * 5

chunk_size_set = [100]
# chunk_size_set = [1 << i for i in range(9)]

row_number = 1000000
ub = row_number / 10
lb = row_number / 10
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/exp_update_1')
row_number *= 2
ub = row_number / 10
lb = row_number / 10
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/exp_update_2')
col_number *= 2
ub = row_number / 10
lb = row_number / 10
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/exp_update_3')
row_number *= 2
col_number *= 2
ub = row_number / 10
lb = row_number / 10
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, 'data/exp_update_4')

col_number = 10
row_number = 1000000
for chunk_size in chunk_size_set:
    os.system("./exp_update %d %d %d %s > %s"
              % (col_number, row_number, chunk_size, 'data/exp_update', 'result/exp_update.txt'))

step = query_number
with open("result/exp_update.txt") as fin:
    ans = [[0.0, 0.0, 0.0] for i in range(query_number * 4 / step)]
    content = fin.read().split('\n')
    content1 = content[5:5 + query_number] + content[5 + query_number + 4: 5 + query_number + 4 + query_number] + \
              content[5 + 2 * (query_number + 4): 5 + 2 * (query_number + 4) + query_number] + \
              content[5 + 3 * (query_number + 4): 5 + 3 * (query_number + 4) + query_number]
    for i in range(len(content1)):
        ans[i / step][0] += float(content1[i].split(' ')[0]) #* 1000
    content2 = content[5 + 4 * (query_number + 4):5 + 4 * (query_number + 4) + query_number] + \
               content[5 + 5 * (query_number + 4):5 + 5 * (query_number + 4) + query_number] + \
               content[5 + 6 * (query_number + 4):5 + 6 * (query_number + 4) + query_number] + \
               content[5 + 7 * (query_number + 4):5 + 7 * (query_number + 4) + query_number]
    for i in range(len(content2)):
        ans[i / step][1] += float(content2[i].split(' ')[0]) #* 1000
    # content3 = content[5 + 8 * (query_number + 4):5 + 8 * (query_number + 4) + query_number] + \
    #            content[5 + 9 * (query_number + 4):5 + 9 * (query_number + 4) + query_number] + \
    #            content[5 + 10 * (query_number + 4):5 + 10 * (query_number + 4) + query_number] + \
    #            content[5 + 11 * (query_number + 4):5 + 11 * (query_number + 4) + query_number]
    # for i in range(len(content3)):
    #     ans[i / step][2] += float(content3[i].split(' ')[0])  # * 1000
    df = pd.DataFrame(ans, columns=['Update', 'Reconstruct Data Canopy'], index=["Phase %d" % i for i in range(1,5)])
    # df /= step
    df.to_csv('result/exp_update.csv')

step = query_number / 100
with open("result/exp_update.txt") as fin:
    ans = [[0.0, 0.0, 0.0] for i in range(query_number * 4 / step)]
    content = fin.read().split('\n')
    content1 = content[5:5 + query_number] + content[5 + query_number + 4: 5 + query_number + 4 + query_number] + \
              content[5 + 2 * (query_number + 4): 5 + 2 * (query_number + 4) + query_number] + \
              content[5 + 3 * (query_number + 4): 5 + 3 * (query_number + 4) + query_number]
    for i in range(len(content1)):
        ans[i / step][0] += float(content1[i].split(' ')[0]) * 1000
    content2 = content[5 + 4 * (query_number + 4):5 + 4 * (query_number + 4) + query_number] + \
               content[5 + 5 * (query_number + 4):5 + 5 * (query_number + 4) + query_number] + \
               content[5 + 6 * (query_number + 4):5 + 6 * (query_number + 4) + query_number] + \
               content[5 + 7 * (query_number + 4):5 + 7 * (query_number + 4) + query_number]
    for i in range(len(content2)):
        ans[i / step][1] += float(content2[i].split(' ')[0]) * 1000
    # content3 = content[5 + 8 * (query_number + 4):5 + 8 * (query_number + 4) + query_number] + \
    #                content[5 + 9 * (query_number + 4):5 + 9 * (query_number + 4) + query_number] + \
    #                content[5 + 10 * (query_number + 4):5 + 10 * (query_number + 4) + query_number] + \
    #                content[5 + 11 * (query_number + 4):5 + 11 * (query_number + 4) + query_number]
    # for i in range(len(content3)):
    #     ans[i / step][2] += float(content3[i].split(' ')[0]) * 1000
    df = pd.DataFrame(ans, columns=['Update', 'Reconstruct Data Canopy'], index=range(0, query_number * 4, step))
    # df /= step
    df.to_csv('result/exp_update_curve.csv')