import getopt
import sys
import pandas as pd
import time
import numpy as np
import scidbpy

class ExpScidb:
    def __init__(self, query_file, col_number, row_number):
        self.sdb = scidbpy.connect()
        self.matrix = [self.sdb.random(row_number, dtype='double') for i in range(col_number)]
        self.query_file = query_file

    def run(self):
        result = []
        fin = open(self.query_file)
        q_str = fin.read().split('\n')
        if q_str[-1] == '':
            q_str = q_str[:-1]
        self.query_list = [[int(val) for val in query.split(' ')] for query in q_str]
        fin.close()
        start_time = time.time()
        for query in self.query_list:
            start_time = time.time()
            if query[0] == 1:
                self.matrix[query[1]][ query[3]:query[4]].mean().eval()
            elif query[0] == 2:
                self.matrix[query[1]][ query[3]:query[4]].var().eval()
            elif query[0] == 3:
                self.matrix[query[1]][ query[3]:query[4]].std().eval()
            elif query[0] == 4:
                (self.matrix[query[1]][ query[3]:query[4]] * self.matrix[query[2]][ query[3]:query[4]]
                 * self.matrix[query[1]][ query[3]:query[4]].sum() * self.matrix[query[2]][ query[3]:query[4]].sum()
                 / self.matrix[query[1]][ query[3]:query[4]].std() / self.matrix[query[2]][ query[3]:query[4]].std()).sum().eval()
            elif query[0] == 5:
                (self.matrix[query[1]][query[3]:query[4]] * self.matrix[query[2]][query[3]:query[4]]
                 * self.matrix[query[1]][query[3]:query[4]].sum() * self.matrix[query[2]][ query[3]:query[4]].sum()).sum().eval()
            result.append((time.time() - start_time))
        return result

optlist, args = getopt.getopt(sys.argv[1:], "f:r:c:aX:o:")
col_number = row_number = query_file = avg = output_file = None
chunk = 1
for o, a in optlist:
    if o == '-f':
        query_file = a
    elif o == '-c':
        col_number = int(a)
    elif o == '-r':
        row_number = int(a)
    elif o == '-a':
        avg = True
    elif o == '-X':
        chunk = int(a)
    elif o == '-o':
        output_file = a

if output_file is None:
    output_file = query_file + "_scidb_time.csv"

scidb_exp = ExpScidb(query_file, col_number, row_number)
res = scidb_exp.run()

ans = []
for i in range(len(res) / chunk):
    tmp = 0.0
    for j in range(chunk):
        tmp += res[i * chunk + j]
    if avg:
        tmp /= chunk
    ans.append(tmp)
df = pd.DataFrame(ans)
if avg:
    df.columns = ["Average Time(s)"]
else:
    df.columns = ["Total Time(s)"]
df.to_csv(output_file, index=False)
