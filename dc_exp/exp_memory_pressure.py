import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 100
row_number = 1000000
# query_number = 10000000
query_file = "data/exp_memory_pressure.txt"
memory_limit = 8589934592
chunk_size = 60



ans = []
for row_number in [2400000, 3900000, 5700000, 6600000, 7900000, 9600000]:
    query_number = 100000
    lb = row_number / 20
    ub = row_number / 10
    result_file = "result/exp_memory_pressure.txt"
    query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)
    ans.append([0.0, 0.0])

    os.system("./exp_query_process_memory_pre_built %d %d %d %s %d> %s"
          % (col_number, row_number, chunk_size, query_file, memory_limit, result_file))
    with open(result_file) as fin:
        content = fin.read().split('\n')[5:-1]
        query_number = len(content)
        pos = 0
        for r in range(query_number):
            if content[r][0] != 'P' and content[r][0] != 'S':
                ans[len(ans) - 1][pos] += float(content[r])
                pos += 1

    for i in range(10, 100):
        os.system("rm dump_%d*" % i)
    os.system("rm dump_*")

df = pd.DataFrame(ans,
                  index=[2400000, 3900000, 5700000, 6600000, 7900000, 9600000],
                  columns=['DC', 'StatSys'])
df.to_csv('result/exp_memory_pressure.csv')