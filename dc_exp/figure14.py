import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

print "Running experiment for figure14"

col_number = 25
query_number = 2000

print "Number of columns: " + str(col_number)
print "Number of rows: varied"

import dc_util

query_file = "test.csv"

column_name = []

count = 0
mat = [[0 for i in range(16)] for j in range(query_number)]

for row_number in [100000000, 250000000, 500000000, 1000000000]:
    ub = row_number / 10
    lb = row_number / 20

    dc_exp = dc_util.ExpDC(query_file, col_number, row_number, 60)

    query_file = "data/exp_scaling_data_u_u.txt"
    query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i]
    count += 1


    query_file = "data/exp_scaling_data_z_u.txt"
    query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i]
    count += 1

    query_file = "data/exp_scaling_data_u_zoomin.txt"
    query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i]
    count += 1

    query_file = "data/exp_scaling_data_z_zoomin.txt"
    query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i]
    count += 1

df = pd.DataFrame(mat)
df.columns = ["u_u_100M", "z_u_100M", "u_zoomin_100M", "z_zoomin_100M",
              "u_u_250M", "z_u_250M", "u_zoomin_250M", "z_zoomin_250M",
              "u_u_500M", "z_u_500M", "u_zoomin_500M", "z_zoomin_500M",
              "u_u_1B", "z_u_1B", "u_zoomin_1B", "z_zoomin_1B",]
df = df.sum()
df = pd.DataFrame([[df[i * 4 + j] for j in range(4)] for i in range(4)],
                  columns=['U', 'Z', 'U_+', 'Z_+'], index=['100M', '250M', '500M', '1B'])
# df *= 1000
df.to_csv("result/figure14.csv")

os.system("gnuplot dc_exp/gnuplot/exp/figure14.gp")
