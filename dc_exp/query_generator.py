import random
import bisect
import numpy as np

def generate_uniform_query(col_number, row_number, query_number, q_len_lb, q_len_up, query_file):
    fout = open(query_file, 'w')
    for i in range(query_number):
        col1 = random.randint(0, col_number - 1)
        col2 = random.randint(0, col_number - 1)
        if col1 > col2:
            col1, col2 = col2, col1
        real_qlen = random.randint(q_len_lb, q_len_up)
        beg = random.randint(0, row_number - real_qlen - 1)
        fout.write("%d %d %d %d %d\n" % (random.randint(1, 5), col1, col2, beg, beg + real_qlen))
        #if i != query_number - 1:
        #    fout.write("-1 0 0 0 0\n")

    fout.close()


def generate_zipfian_query(col_number, row_number, query_number, q_len_lb, q_len_up, query_file):
    fout = open(query_file, 'w')
    distri = [1 / float(i + 1) for i in range(col_number)]
    for i in range(1, col_number):
        distri[i] += distri[i - 1]

    for i in range(query_number):
        col1 = bisect.bisect_right(distri, random.random() * distri[col_number - 1])
        col2 = random.randint(0, col_number - 1)
        if col1 > col2:
            col1, col2 = col2, col1
        real_qlen = random.randint(q_len_lb, q_len_up)
        beg = random.randint(0, row_number - real_qlen - 1)
        fout.write("%d %d %d %d %d\n" % (random.randint(1, 5), col1, col2, beg, beg + real_qlen))
        # if i != query_number - 1:
        #     fout.write("-1 0 0 0 0\n")


def settolist(query_list):
    ans = [i for i in query_list]
    return ans


def quick_generate(query_list, col1, col2, left, right):
    query_list.add((1, col1, col2, left, right))
    query_list.add((2, col1, col2, left, right))
    query_list.add((3, col1, col2, left, right))
    query_list.add((4, col1, col2, left, right))
    query_list.add((5, col1, col2, left, right))

def generate_u_zoomin_query(col_number, row_number, depth, query_number, query_file):
    query_list = set()
    for col1 in range(col_number):
        col2 = random.randint(0, col_number - 1)
        left = 0
        right = row_number - 1
        quick_generate(query_list, col1, col2, left, right)
        for dep in range(depth):
            mid = (left + right) / 2
            quick_generate(query_list, col1, col2, left, mid)
            quick_generate(query_list, col1, col2, mid + 1, right)
            if random.randint(0, 1) == 0:
                right = mid
            else:
                left = mid + 1
    query_list = settolist(query_list)
    query_list = sorted(query_list, key=lambda x: (x[4] - x[3]), reverse=True)[:query_number]
    fout = open(query_file, 'w')
    for i in query_list:
        fout.write("%d %d %d %d %d\n" % i)
    fout.close()

def generate_z_zoomin_query(col_number, row_number, depth, query_number, query_file):
    query_list = set()
    distri = [1 / float(i + 1) for i in range(col_number)]
    for i in range(1, col_number):
        distri[i] += distri[i - 1]
    while (len(query_list) < query_number):
        col1 = bisect.bisect_right(distri, random.random() * distri[col_number - 1])
        col2 = random.randint(0, col_number - 1)
        left = 0
        right = row_number - 1
        quick_generate(query_list, col1, col2, left, right)
        for dep in range(depth):
            mid = (left + right) / 2
            quick_generate(query_list, col1, col2, left, mid)
            quick_generate(query_list, col1, col2, mid + 1, right)
            if random.randint(0, 1) == 0:
                right = mid
            else:
                left = mid + 1
    query_list = settolist(query_list)
    query_list = sorted(query_list, key=lambda x: (x[4] - x[3]), reverse=True)[:query_number]
    fout = open(query_file, 'w')
    for i in query_list:
        fout.write("%d %d %d %d %d\n" % i)
    fout.close()