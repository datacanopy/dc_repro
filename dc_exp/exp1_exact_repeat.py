import numpy as np
import os
import data
import pandas as pd
import plot

col_number = 2
row_number = 1000000000
data_file = "data/exp1_data.csv"
query_file = "data/exp1_query.txt"
data.generateData(data_file, row_number, col_number)
fout = open(query_file, 'w')
for i in range(col_number):
    fout.write("1 %d 0 0 %d\n" % (i, row_number))
for i in range(col_number):
    fout.write("3 %d 0 0 %d\n" % (i, row_number))
for i in range(col_number):
    for j in range(i + 1, col_number):
        fout.write("5 %d %d 0 %d\n" % (i, j, row_number))

for i in range(col_number):
    for j in range(i + 1, col_number):
        fout.write("4 %d %d 0 %d\n" % (i, j, row_number))

for i in range(col_number):
    for j in range(i + 1, col_number):
        fout.write("5 %d %d 0 %d\n" % (i, j, row_number))
for i in range(col_number):
    fout.write("3 %d 0 0 %d\n" % (i, row_number))
for i in range(col_number):
    fout.write("1 %d 0 0 %d\n" % (i, row_number))
fout.close()

res = []

import r_util
import monetdb_util
import dc_util
import numpy_util

column_name = []

numpy_exp = numpy_util.ExpNumpy(query_file, col_number, row_number)
column_name.append('NumPy (Python)')
exp_R = r_util.ExpR(query_file, col_number, row_number)
column_name.append('Modeltools (R)')
exp_monetdb = monetdb_util.ExpMonetdb(query_file, data_file)
column_name.append('MonteDB')
dc_exp = dc_util.ExpDC(query_file, col_number, row_number)
column_name.append('Data Canopy')


res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())
mat = [[0 for i in range(len(res))] for j in range(11)]
for i in range(11):
    for j in range(len(res) - 1):
        mat[i][j] = res[j][i]
    mat[i][len(res) - 1] = res[len(res) - 1][i][0]
df = pd.DataFrame(mat)
df.columns = column_name
# df.index = ["Avg1", "Std1", "Cov1", "Cor1",
#             "Cov2", "Std2", "Avg2",]
df.to_csv("result/exp1.csv")
#plot.PlotBarChartFromExcel(df, "graph/exp1", "Operation type", "Total response time (s)")