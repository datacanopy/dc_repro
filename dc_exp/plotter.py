import csv
import plot as plt
import pandas as pd
import numpy as np
import os

chunk_small = pd.read_csv("result/exp_chunk_mem_small.csv", index_col=0)
chunk_large = pd.read_csv("result/exp_chunk_mem_large.csv", index_col=0)
step = chunk_small.shape[0] / 20
mat = []
for i in range(step - 1, chunk_small.shape[0], step):
    tmp = []
    for j in range(chunk_small.shape[1]):
        tmp.append(chunk_small.iloc[i, j])
    for j in range(chunk_large.shape[1]):
        tmp.append(chunk_large.iloc[i, j])
    mat.append(tmp)
df = pd.DataFrame(mat, index=range(step - 1, chunk_small.shape[0], step))
df /= 1024
df.columns = ["50", "500", "5000", "50000", "250K", "500K", "1M", "2M"]
df.to_csv("result/chunk_mem_tmp.csv")
