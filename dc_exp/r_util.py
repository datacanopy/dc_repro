import os

class ExpR:
    def __init__(self, query_file, col_number, row_number):
        self.query_file = query_file
        self.col_number = col_number
        self.row_number = row_number



    def run(self):
        result = []
        log = os.popen("Rscript dc_exp/query_process.R %d %d %s" % (self.col_number, self.row_number, self.query_file)).read()
        log_list = log.split('\n')[:-1]
        for row in range(1, len(log_list), 2):
            result.append(float(log_list[row]))
        return result
