import os

class ExpDC:
    def __init__(self, query_file, col_num, row_num, chunk_size = 10000):
        self.col_num = col_num
        self.row_num = row_num
        self.query_file = query_file
        self.chunk_size = chunk_size


    def     run(self):
        result = []
        log = os.popen("build/exp_query_process %d %d %d %s" % (self.col_num, self.row_num, self.chunk_size, self.query_file)).read()
        log_list = log.split('\n')[:-1]
        for row in log_list[4:]:
            result.append(float(row.split(' ')[0]))
        return result
