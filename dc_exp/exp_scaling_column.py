import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

# col_number = 25
row_number = 1000000

query_number = 10000

import dc_util

query_file = "test.csv"

column_name = []

count = 0
mat = [[0 for i in range(24)] for j in range(query_number)]

for col_number in [100, 200, 400, 800, 1600, 3200]:
    ub = row_number / 10
    lb = row_number / 20

    dc_exp = dc_util.ExpDC(query_file, col_number, row_number, 60)

    query_file = "data/exp_scaling_column_u_u.txt"
    query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1


    query_file = "data/exp_scaling_column_z_u.txt"
    query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

    query_file = "data/exp_scaling_column_u_zoomin.txt"
    query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

    query_file = "data/exp_scaling_column_z_zoomin.txt"
    query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, query_file)

    dc_exp.query_file = query_file
    res = dc_exp.run()
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

df = pd.DataFrame(mat)
df.columns = ["u_u_100C", "z_u_100C", "u_zoomin_100C", "z_zoomin_100C",
              "u_u_200C", "z_u_200C", "u_zoomin_200C", "z_zoomin_200C",
              "u_u_400C", "z_u_400C", "u_zoomin_400C", "z_zoomin_400C",
              "u_u_800C", "z_u_800C", "u_zoomin_800C", "z_zoomin_800C",
              "u_u_1600C", "z_u_1600C", "u_zoomin_1600C", "z_zoomin_1600C",
              "u_u_3200C", "z_u_3200C", "u_zoomin_3200C", "z_zoomin_3200C",
              ]
df = df.sum()
df = pd.DataFrame([[df[i * 4 + j] for j in range(4)] for i in range(6)],
                  columns=['U', 'Z', 'U_+', 'Z_+'], index=[100, 200, 400, 800, 1600, 3200])
df *= 1000
df.to_csv("result/exp_scaling_column.csv")
os.system("gnuplot dc_exp/gnuplot/exp/exp_scaling_columns.gp")