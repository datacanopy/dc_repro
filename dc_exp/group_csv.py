import pandas as pd
import sys
import numpy as np
import plot
if __name__ == '__main__':
    file_name = sys.argv[1]
    part = int(sys.argv[2])
    xlabel_name = sys.argv[3]
    ylabel_name = sys.argv[4]
    dot = sys.argv[5]
    if len(sys.argv) > 6:
        top_number = int(sys.argv[6])
        data = pd.read_csv("result/%s.csv" % file_name)[:top_number]
    else:
        data = pd.read_csv("result/%s.csv" % file_name)
    res = data.groupby(lambda x: x / (data.shape[0] / part)).aggregate(np.average).iloc[:,1:]
    # res /= (data.shape[0] / part)
    res *= 1000
    res.index = [i * (data.shape[0] / part) for i in range(1, part + 1)]
    tmp_name = "result/tmp.csv"
    res.to_csv(tmp_name)
    if len(sys.argv) > 6:
        plot.PlotSimpleLinePlot(tmp_name, "graph/%s_%d" % (file_name, top_number), xlabel_name, ylabel_name, 1, dot)
    else:
        plot.PlotSimpleLinePlot(tmp_name, "graph/%s" % file_name, xlabel_name, ylabel_name, 1, dot)
