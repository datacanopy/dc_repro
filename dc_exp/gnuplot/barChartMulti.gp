### n: change this parameter to equal the number of data sets to be plotted
n = 2
# t: top margin in pixels
t = 75.0
# b: key height in pixels (bottom margin)
b = 300.0
# h: height of output in pixels
h = 300.0*n + t + b

### define functions to help set top/bottom margins
top(i,n,h,t,b) = 1.0 - (t+(h-t-b)*(i-1)/n)/h
bot(i,n,h,t,b) = 1.0 - (t+(h-t-b)*i/n)/h

### first set up some basic plot parameters
set terminal pdf enhanced font 'times,16' 
set datafile separator ","
set output ofile

# Labels, plot axis and tics
set format y '%.1tx10^{%S}'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label
#set xtics rotate by 45 offset -0.8,-1.8

# logscale or not
set logscale y

# Plot specific formatting
set style data histograms
set boxwidth 1
set style fill pattern


set multiplot layout (n+1),1

### First plot
# change only plot command here
currentplot = 1
set tmargin at screen top(currentplot,n,h,t,b)
set bmargin at screen bot(currentplot,n,h,t,b)
unset key
#unset xtics
# Plot 1
plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
	"" using 8 with histograms lc 9 fillstyle pattern 3,\
	"" using 5 with histograms lc 9 fillstyle pattern 6,\

### Middle plot
# copy and paste this code to make more middle plots
currentplot = currentplot + 1.6
set tmargin at screen top(currentplot,n,h,t,b)
set bmargin at screen bot(currentplot,n,h,t,b)
unset title
plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
	"" using 8 with histograms lc 9 fillstyle pattern 3,\
	"" using 5 with histograms lc 9 fillstyle pattern 6,\

### Last (key) plot
set tmargin at screen bot(n,n,h,t,b)
set bmargin at screen 0
set key horizontal center center
set key autotitle columnhead
set key inside
set border 0
unset tics
unset xlabel
unset ylabel

