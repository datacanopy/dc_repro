 ### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'times,12'
set datafile separator ","

# Output settings
set output ofile

###
# Plot specific formatting
set style data histograms
set boxwidth 1
set style fill pattern
set autoscale y
#set tmargin 0
#set bmargin 0
set lmargin 13
set rmargin 8

###
# Individual font sizes
set key font "times,12"
set xtics font "times,12" 
set xlabel font "times,12"
set ylabel font "times,12"

# multiple plots
set multiplot layout 2,1

# Location
set size 1,0.3
set origin 0,0.7

# logscale or not
#set logscale y

# Plot 1 settings
# Appearance
set key vertical top left 
set key autotitle columnhead
set xrange [-1:7]
unset xtics
set ylabel y_label_1

# Labels, plot axis and tics
set format x '%.0tx10^{%S}'
#set format y '%.1tx10^{%S}'

# line styles
set style line 1 lt 7 lw 3 lc 9
set style line 2 lt 9 lw 3 lc 9

# Plot 1
x = (1,2,3,4,5,6,7)
plot ifileone using 10 with lp ls 1,\
	"" using 11 with lp ls 2,\
	


# Plot 2 settings
# Appearance
set key vertical top right 
set key autotitle columnhead
set autoscale x
# Labels, plot axis and tics
set format y '%.1tx10^{%S}'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label_2
set xtics 1
# logscale or not
set logscale y

set size 1,0.72

# Plot 2
plot ifiletwo using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
	"" using 8 with histograms lc 9 fillstyle pattern 7,\
	"" using 9 with histograms lc 9 fillstyle pattern 9,\
	"" using 5 with histograms lc 9 fillstyle pattern 3,\