### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","

x_label = "Query sequence"
y_label = "Response time (s)"

# Appearance
#set key outside 
#set key horizontal top outside
set key vertical top left inside
# set key horizontal top right inside #maxcols 2
set key autotitle columnhead font 'Times-Roman,12'
# set key off
set offset 0.0,0,graph 0.0,0

# Labels, plot axis and tics
# set format y '%f'
set format x '%g%%'
# set format x '%.0f'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror  scale 0#font 'Times-Roman,12'
# set xtics 2000#
set xtics add ("0" 0) font 'Times-Roman,12'
# logscale or not


# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 10 pointsize 0 pi 10
set style line 2 lt 2 lw 3 lc 9 pt 4 pointsize 0 pi 10
set style line 3 lt 1 lw 3 lc 9 pt 12 pointsize 1.5 pi 3
set style line 4 lt 1 lw 3 lc 9 pt 8 pointsize 1.5 pi 3
set style line 5 lt 1 lw 3 lc 9 pt 11 pointsize 2.5 pi 3
set style line 6 lt 1 lw 3 lc 9 pt 7 pointsize 2.5 pi 3
set style line 7 lt 1 lw 3 lc 9 pt 13 pointsize 2.5 pi 3
set style line 8 lt 1 lw 3 lc 9 pt 9 pointsize 2.5 pi 3

###

###

# Plot specific formatting
set style data histograms
set style histogram gap 1
set boxwidth 1
# Plot

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
#set yrange [1:]
# set logscale y
set autoscale y
# set yrange [0.001:5]
# set logscale x
set autoscale x

#set ytics add ("1" 1)
#set ytics add ("0.1" 0.1)
#set xrange [0: 10000]

ifile = "result/exp_update_curve.csv"
ofile = "graph/exp_update_curve.pdf"
set output ofile
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    # "" using 1:4  with lp ls 3 ,\

y_label = "Total Execution time (s)"
ifile = "result/exp_update.csv"
ofile = "graph/exp_update.pdf"
# Output settings
set output ofile
set xtics  #rotate by -45 offset -2.8,-0.25
set xtics nomirror scale 0

# logscale or not
#set logscale y 2
#set ytics add ("1" 1)

###

# Plot specific formatting
set style data histograms
set style histogram gap 1
set boxwidth 1
#set style fill pattern
#set style line solid
# set logscale y
set autoscale y
# set yrange [: 1e11]
# set yrange [0.1:1000]


# Plot
plot ifile using 2:xtic(1) with histograms lc 0 fillstyle pattern 1,\
    "" using 3 with histograms fillstyle pattern 0 border dt 1,\
    # "" using 4 with histograms fillstyle pattern 2 border dt 1

