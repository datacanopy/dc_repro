### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","

x_label = "Query sequence"
y_label = "Average response time (ms)"

# Appearance
#set key outside 
#set key horizontal top outside
#set key vertical top right inside
set key horizontal top right inside maxcols 2
set key autotitle columnhead #font 'Times-Roman,12'
set offset 0.0,0,graph 0.0,0

# Labels, plot axis and tics
set format y '%.0f'
#set format x '%g%%'
# set format x '%g'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror #font 'Times-Roman,12'
set xtics 2000#

# logscale or not


# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 10 pointsize 1.5 pi 3
set style line 2 lt 1 lw 3 lc 9 pt 4 pointsize 1.5 pi 3
set style line 3 lt 1 lw 3 lc 9 pt 12 pointsize 1.5 pi 3
set style line 4 lt 1 lw 3 lc 9 pt 8 pointsize 1.5 pi 3
set style line 5 lt 1 lw 3 lc 9 pt 11 pointsize 2.5 pi 3
set style line 6 lt 1 lw 3 lc 9 pt 7 pointsize 2.5 pi 3
set style line 7 lt 1 lw 3 lc 9 pt 13 pointsize 2.5 pi 3
set style line 8 lt 1 lw 3 lc 9 pt 9 pointsize 2.5 pi 3

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
#set yrange [1:]
set autoscale y
# set yrange [0:100]
set autoscale x

#set ytics add ("1" 1)
#set ytics add ("0.1" 0.1)
#set xrange [0: 10000]

ifile = "result/u_u_1M.csv"
ofile = "graph/u_u_1M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/u_u_10M.csv"
ofile = "graph/u_u_10M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/u_u_100M.csv"
ofile = "graph/u_u_100M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\

ifile = "result/u_z_1M.csv"
ofile = "graph/u_z_1M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/u_z_10M.csv"
ofile = "graph/u_z_10M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/u_z_100M.csv"
ofile = "graph/u_z_100M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\

ifile = "result/z_u_1M.csv"
ofile = "graph/z_u_1M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/z_u_10M.csv"
ofile = "graph/z_u_10M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/z_u_100M.csv"
ofile = "graph/z_u_100M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\

ifile = "result/z_z_1M.csv"
ofile = "graph/z_z_1M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/z_z_10M.csv"
ofile = "graph/z_z_10M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\


ifile = "result/z_z_100M.csv"
ofile = "graph/z_z_100M.pdf"
# Output settings
set output ofile

# Plot
plot ifile using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\
    "" using 1:6  with lp ls 5 ,\
    "" using 1:7  with lp ls 6 ,\
    "" using 1:8  with lp ls 7 ,\
