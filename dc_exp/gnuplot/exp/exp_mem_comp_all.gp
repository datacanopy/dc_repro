### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,16' monochrome size 13, 3
set datafile separator ","

ifile1 = "result/exp_mem_comp_u_u_plot_2000_test.csv"
ifile2 = "result/exp_mem_comp_z_u_plot_2000_test.csv"
ifile3 = "result/exp_mem_comp_u_zoomin_plot_2000_test.csv"
ifile4 = "result/exp_mem_comp_z_zoomin_plot_2000_test.csv"
ofile = "graph/exp_mem_comp_all.pdf"
x_label = "Query sequence"
y_label = "Response time (ms)"
# Appearance
#set key outside 
#set key horizontal top outside
#set key vertical top right inside
#set key horizontal top right outside #maxcols 2
set tmargin 2
set bmargin 3.75
#set offset 0.0,0,graph 0.0,0

# Labels, plot axis and tics
set format y '%.0f'
#set format x '%g%%'
set format x '%g'
#set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror #font 'Times-Roman,12'
set xtics 400#
set xrange [0:2000]


# logscale or not

# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 13 pointsize 2 pi 30
set style line 2 lt 1 lw 3 lc 9 pt 5 pointsize 2 pi 30
set style line 3 lt 1 lw 3 lc 9 pt 11 pointsize 2 pi 30
set style line 4 lt 1 lw 3 lc 9 pt 9 pointsize 2 pi 30

# set style line 1 dt 1 lt 1 lw 3 lc 9 pointsize 0
# set style line 2 dt 8 lt 1 lw 3 lc 9 pointsize 0
# set style line 3 dt 7 lt 1 lw 3 lc 9 pointsize 0
# set style line 4 dt 4 lt 1 lw 3 lc 9 pointsize 0

# Output settings
set output ofile

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern

set xtics add ("0" 0)

# logscale or not
#log = 1
set logscale y
set format y '10^{%T}'

set multiplot# layout 1,2

set label 1 x_label at screen 0.5, 0.05 center font 'Times-Roman,18'
# set label 2 y_label rotate 90 at screen 0.05, 0.5 center 
#set label 2 y_label at screen 0.01,0.5 rotate by 90

set xtics offset 0, 0
set xlabel offset 0, 0.5
# Plot
set lmargin at screen 0.075
set rmargin at screen 0.275
unset key
# set size 0.5, 1.0
set xlabel "(a) Workload U"
set yrange [1:1000]
set ytics (1, 10, 100, 1000)
plot ifile1 using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\

# set origin 0.45, 0.0

set lmargin at screen 0.300
set rmargin at screen 0.500
unset key
unset label 1
# unset label 2
set ytics
unset ylabel
set xlabel "(b) Workload Z"
set format y ""
# unset ytics
plot ifile2 using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\


set lmargin at screen 0.550
set rmargin at screen 0.750
unset key
set format y '10^{%T}'
set yrange [0.01:10000]
set ytics (0.01, 1, 100, 10000)
# set size 0.5, 1.0
set xlabel "(c) Workload U_+"
plot ifile3 using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\

# set origin 0.45, 0.0

set lmargin at screen 0.775
set rmargin at screen 0.975
unset key
set format y ''
# unset label 2
set ytics
unset ylabel
set xlabel "(d) Workload Z_+"
set format y ""
# unset ytics
plot ifile4 using 1:2  with lp ls 1 ,\
    "" using 1:3  with lp ls 2 ,\
    "" using 1:4  with lp ls 3 ,\
    "" using 1:5  with lp ls 4 ,\


unset border
unset tics
unset label
unset arrow
unset title
unset object
unset lmargin
unset rmargin
# show size
set origin 0, 0.0
set size 1,1 #however big you need it

#example key settings
# set key box 
# set key horizontal reverse samplen 1 width -4 maxrows 1 maxcols 12 
# set key at screen 0.5,screen 0.15 center top

unset xlabel
set key horiz outside #left top #at screen #maxrows 2 #at screen 0.5, 0.9 center
set key autotitle columnhead font 'Times-Roman,16'
#We need to set an explicit xrange.  Anything will work really.
# set xrange [-1:1]
# set yrange [-1:1]
# set origin 0.0, 1.0


plot NaN title 'NumPy (Python)' with lp ls 1 ,\
     NaN title 'Modeltools (R)' with lp ls 2,\
     NaN title 'MonteDB' with lp ls 3,\
     NaN title 'Data Canopy' with lp ls 4 

unset multiplot
#	"" using 1:6  with lp ls 5 ,\
#	"" using 1:7  with lp ls 6 ,\
#	"" using 1:8  with lp ls 7 ,\
#	"" using 1:9  with lp ls 8 ,\

#plot ifile using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
#	"" using 3 with histograms lc 9 fill solid 0.5,\


#plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
#	"" using 5 with histograms lc 9 fill solid 1,\