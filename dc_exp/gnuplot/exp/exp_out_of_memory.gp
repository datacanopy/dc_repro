### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,14' monochrome
set datafile separator ","

x_label = "Query sequence"
y_label = "Average response time (ms)"

# Appearance
#set key outside 
#set key horizontal top outside
#set key vertical top right inside
set key horizontal top right inside #maxcols 2
set key autotitle columnhead #font 'Times-Roman,12'
set key off
set offset 0.0,0,graph 0.0,0

# Labels, plot axis and tics
set format y '%.0f'
#set format x '%g%%'
# set format x '%g'
set format x '%.1tx10^{%S}'
# set format x2 '%g'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror #font 'Times-Roman,12'
# set xtics 2000#

# logscale or not


# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 10 pointsize 0 pi 3
set style line 2 lt 1 lw 3 lc 9 pt 4 pointsize 2.5 pi 3
set style line 3 lt 1 lw 3 lc 9 pt 12 pointsize 2.5 pi 3
set style line 4 lt 1 lw 3 lc 9 pt 4 pointsize 2.5 pi 3

# set arrow from 372, graph 0 to 372, graph 1 nohead lt 2
# set label 1 center rotate by -90 at 1000,5 "Phase 1" 
# set label 2 center at 2000,5.5 "Phase 2" 
# Output settings

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
set autoscale y
set yrange [0:]
set autoscale x

set arrow from boundary, graph 0 to boundary, graph 1 nohead lt 2
set xtics 20000
set xrange [0:100000]
set xtics add ("0" 0)
ifile = "result/exp_out_of_memory.csv"
ofile = "graph/exp_out_of_memory.pdf"
set output ofile
plot ifile using 1:2  with lp ls 1
