 ### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","
set rmargin 0.5
# Appearance
#set key outside 
#set key horizontal left top
ifile = "result/exp_vary_chunk_combined_bar.csv"
ofile = "graph/exp_vary_chunk_combined_bar.pdf"
x_label = "Chunk size"
y_label = "Total execution time(s)"

# set key horizontal top right inside # maxcols 4 width 1
# set key autotitle columnhead font "Times-Roman, 12"
# set key inside
set key off
set offset -1.2,-0.3,graph 0.0,0

# Labels, plot axis and tics
#set format y '%.1tx10^{%S}'
set format y '%g'
set format x '%.0tx10^{%S}'
set xlabel x_label 
set ylabel y_label
set xtics font "Times-Roman, 12" #rotate by -45 offset -2.8,-0.25
set xtics nomirror scale 0

# logscale or not
#set logscale y 2
#set ytics add ("1" 1)

# Output settings
#set output "./gnuplot/temp"
set output ofile

###

# Plot specific formatting
set style data histograms
set style histogram gap 1
set boxwidth 1
#set style fill pattern
#set style line solid
# set logscale y
set autoscale y
set yrange [0:]
# set yrange [0.1:1000]


# Plot
plot ifile using 2:xtic(1) with histograms lc 0 fillstyle pattern 1