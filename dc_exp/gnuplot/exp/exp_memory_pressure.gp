### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","

ifile = "result/exp_memory_pressure.csv"
ofile = "graph/exp_memory_pressure.pdf"
x_label = "Rows number"
y_label = "Total execution time (s)"

# Appearance
#set key outside 
#set key horizontal top outside
#set key vertical top right inside
set key horizontal top right inside #maxcols 2
set key autotitle columnhead font 'Times-Roman,13'
set offset -0.3,-0.3,graph 0.2,0

# Labels, plot axis and tics
set format y '%.1tx10^{%T}'
#set format x '%g%%'
set format x '%g'
# set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror #font 'Times-Roman,12'
# set xtics 200#
set logscale y
# logscale or not


# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 10 pointsize 2.5 #pi 3
set style line 2 lt 1 lw 3 lc 9 pt 4 pointsize 2.5 #pi 3
set style line 3 lt 1 lw 3 lc 9 pt 12 pointsize 2.5 #pi 3
set style line 4 lt 1 lw 3 lc 9 pt 4 pointsize 2.5 #pi 3


# Output settings
set output ofile

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
set autoscale y
set yrange [1:]
# set autoscale x
# set logscale x 2
# set ytics add (0.0, 0.5, 1.0, 1.5, 2.0)
# set yrange [0:2.2]

#set ytics add ("1" 1)
#set ytics add ("0.1" 0.1)
# set yrange [1000:]

# Plot
set style data histograms
set style histogram gap 1
plot ifile using 2:xtic(1) with histograms lc 0 fillstyle pattern 1,\
    "" using 3 with histograms fillstyle pattern 0 border dt 1,\
    # "" using 4 with histograms fillstyle pattern 2 border dt 1,\
    # "" using 5 with histograms fill transparent solid 0.25 border dt 1
# plot ifile using 1:2  with lp ls 1 ,\
    # "" using 1:3  with lp ls 2 ,\
#   "" using 1:4  with lp ls 3 ,\
#   "" using 1:5  with lp ls 4 ,\
#   "" using 1:6  with lp ls 5 ,\
#   "" using 1:7  with lp ls 6 ,\
#   "" using 1:8  with lp ls 7 ,\
#   "" using 1:9  with lp ls 8 ,\

#plot ifile using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
#   "" using 3 with histograms lc 9 fill solid 0.5,\


#plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
#   "" using 5 with histograms lc 9 fill solid 1,\