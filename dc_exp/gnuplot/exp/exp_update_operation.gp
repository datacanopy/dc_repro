 ### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","
set rmargin 0.5
# Appearance
#set key outside 
#set key horizontal left top
ifile = "result/exp_update_operation.csv"
ofile = "graph/exp_update_operation.pdf"
x_label = "Number of rows"
y_label = "Total execution time (s)"

set key horizontal top right inside #width 1# maxcols 4 
set key autotitle columnhead font 'Times-Roman,12'
set key inside
set offset -0.3,-0.3,graph 0.1,0

# Labels, plot axis and tics
# set format y '10^{%S}'
# set format y '%g'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label
#set xtics font "Times-Roman, 12" #rotate by -45 offset -2.8,-0.25
set xtics nomirror scale 0

# logscale or not
set autoscale y
set yrange [0:]
#set logscale y 2
# set ytics add ("10^{-1}" 0.1)
# set ytics add ("1" 1)
# set ytics add ("10" 10)
# set ytics add ("10^{2}" 100)
# set ytics add ("10^{3}" 1000)

# Output settings
#set output "./gnuplot/temp"
set output ofile

###

# Plot specific formatting
set style data histograms
# set boxwidth 1
#set style fill pattern
#set style line solid
# set logscale y
# set yrange [0.1:2000]
# set xtics (1, 10, 100, 1000)
set style histogram rowstacked gap 1
set boxwidth 0.5 relative
# set style histogram 
# Plot
plot ifile using 3:xtic(1) with histograms lc 0 fillstyle pattern 1,\
	"" using 2 with histograms fillstyle pattern 0 border dt 1,\
	# "" using 4 with histograms fillstyle pattern 2 border dt 1,\
	# "" using 5 with histograms fill transparent solid 0.25 border dt 1