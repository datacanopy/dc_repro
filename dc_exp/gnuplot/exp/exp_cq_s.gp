### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman,15' monochrome
set datafile separator ","

x_label = "Chunk size"
y_label = "Response time (s)"

# Appearance
#set key outside 
#set key horizontal top outside
set key vertical top right inside
# set key horizontal top right inside maxcols 2
set key autotitle columnhead font 'Times-Roman,10'
set offset 0.0,0,graph 0.3,0

# Labels, plot axis and tics
set format y '%.0f'
#set format x '%g%%'
set format x '%.0f'
# set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label# offset 3
#set xtics rotate by 45 offset -0.8,-1.8
set xtics nomirror #font 'Times-Roman,12'

# set xtics 2000#

# logscale or not


# Line styles
set style line 1 lt 1 lw 3 lc 9 pt 10 pointsize 1.5 #pi 3
set style line 2 lt 2 lw 3 lc 9 pt 4 pointsize 1.5 #pi 3
set style line 3 lt 3 lw 3 lc 9 pt 12 pointsize 1.5 #pi 3
set style line 4 lt 4 lw 3 lc 9 pt 8 pointsize 1.5 #pi 3
set style line 5 lt 1 lw 3 lc 9 pt 11 pointsize 2.5 #pi 3
set style line 6 lt 1 lw 3 lc 9 pt 7 pointsize 2.5 #pi 3
set style line 7 lt 1 lw 3 lc 9 pt 13 pointsize 2.5 #pi 3
set style line 8 lt 1 lw 3 lc 9 pt 9 pointsize 2.5 #pi 3

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
#set yrange [1:]
set autoscale y
set yrange [0:]
set logscale x
set autoscale x

#set ytics add ("1" 1)
#set ytics add ("0.1" 0.1)
# set xrange [0: 200]


ifile = "result/exp_cq_s.csv"
ofile = "graph/exp_cq_s.pdf"
# Output settings
set output ofile

# set multiplot

# Plot
plot ifile using 1:2  with lp ls 1 pointsize 0 ,\
    "" using 1:3  with lp ls 2  pointsize 0,\
    "" using 1:4  with lp ls 3  pointsize 0,\
    "" using 1:5  with lp ls 4  pointsize 0 ,\
    
# unset logscale x
# unset key
# set origin 0.09,0.47
# set size 0.60,0.5
# set xrange [20:500]
# set autoscale y
# set yrange [0:]
# unset xlabel
# unset ylabel
# unset label
# set ytics nomirror font 'Times-Roman,8' offset 0.5, 0.0
# set xtics nomirror font 'Times-Roman,8' offset 0.0, 0.5
# # unset object 1
# plot ifile using 1:2  with lp ls 1  pointsize 0 ,\
#     "" using 1:3  with lp ls 2  pointsize 0 ,\
#     "" using 1:4  with lp ls 3  pointsize 0 ,\
#     "" using 1:5  with lp ls 4  pointsize 0 ,\


# ifile = "result/u_z_chunk_row_time.csv"
# ofile = "graph/u_z_chunk_row_time.pdf"
# # Output settings
# set output ofile

# # Plot
# plot ifile using 1:2  with lp ls 1 ,\
#     "" using 1:3  with lp ls 2 ,\
#     "" using 1:4  with lp ls 3 ,\
    


# ifile = "result/z_u_chunk_row_time.csv"
# ofile = "graph/z_u_chunk_row_time.pdf"
# # Output settings
# set output ofile

# # Plot
# plot ifile using 1:2  with lp ls 1 ,\
#     "" using 1:3  with lp ls 2 ,\
#     "" using 1:4  with lp ls 3 ,\
    


# ifile = "result/z_z_chunk_row_time.csv"
# ofile = "graph/z_z_chunk_row_time.pdf"
# # Output settings
# set output ofile

# # Plot
# plot ifile using 1:2  with lp ls 1 ,\
#     "" using 1:3  with lp ls 2 ,\
#     "" using 1:4  with lp ls 3 ,\
    


