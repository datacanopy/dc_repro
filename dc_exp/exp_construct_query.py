import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 100

row_number = 40000000
query_number = 10000

import dc_util



column_name = []

count_t = 0
mat = [[0 for i in range(12)] for j in range(query_number)]
# mat = [[0 for i in range(4)] for j in range(query_number)]

ub = row_number / 10
lb = row_number / 20
query_file = "data/exp_construct_query_u_u.txt"
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)

query_file = "data/exp_construct_query_z_u.txt"
query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, query_file)

query_file = "data/exp_construct_query_u_zoomin.txt"
query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, query_file)

query_file = "data/exp_construct_query_z_zoomin.txt"
query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, query_file)

count = 0
for commend in ["build/exp_query_process %d %d %d " % (col_number, row_number, row_number + 1),
                "build/exp_query_process %d %d %d " % (col_number, row_number, 60),
                "build/exp_query_process %d %d %d " % (col_number, row_number, 60),]:
    count_t += 1
    tail_c = 0
    if count_t == 3:
        tail_c = 1
    else:
        tail_c = 0


    query_file = "data/exp_construct_query_u_u.txt"
    print (commend + "%s %d" % (query_file, tail_c))
    res = []
    log = os.popen(commend + "%s %d" % (query_file, tail_c)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        res.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

    query_file = "data/exp_construct_query_z_u.txt"
    print (commend + "%s %d" % (query_file, tail_c))

    res = []
    log = os.popen(commend + "%s %d" % (query_file, tail_c)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        res.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

    query_file = "data/exp_construct_query_u_zoomin.txt"
    print (commend + "%s %d" % (query_file, tail_c))

    res = []
    log = os.popen(commend + "%s %d" % (query_file, tail_c)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        res.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

    query_file = "data/exp_construct_query_z_zoomin.txt"
    print (commend + "%s %d" % (query_file, tail_c))

    res = []
    log = os.popen(commend + "%s %d" % (query_file, tail_c)).read()
    log_list = log.split('\n')[:-1]
    for row in log_list[4:]:
        res.append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))
    for i in range(query_number):
        mat[i][count] = res[i][0]
    count += 1

df = pd.DataFrame(mat)
df.columns = ["u_u_bd", "z_u_bd", "u_zoomin_bd", "z_zoomin_bd",
              "u_u_bf", "z_u_bf", "u_zoomin_bf", "z_zoomin_bf",
              "u_u_dy", "z_u_dy", "u_zoomin_dy", "z_zoomin_dy",]
df = df.sum()
df = pd.DataFrame([[df[i * 4 + j] for j in range(4)] for i in range(3)], columns=['U', 'Z', 'U_+', 'Z_+'], index=['StatSys', 'Online DC', 'Offline DC'])
df.to_csv("result/exp_construct_query_plot.csv")
os.system("gnuplot dc_exp/gnuplot/exp/exp_query_const.gp")
