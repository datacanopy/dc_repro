import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 100
row_number = 40000000


query_number = 1000000
ub = row_number / 10
lb = row_number / 100 * 5

import dc_util

query_file = "test.csv"

column_name = []

dc_exp = dc_util.ExpDC(query_file, col_number, row_number, 60)
column_name.append('Data Canopy')


query_file = "data/exp_scaling_queries_u_u.txt"
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)

dc_exp.query_file = query_file
res = dc_exp.run()
mat = [[0 for i in range(2)] for j in range(query_number)]
for i in range(query_number):
    mat[i][0] = res[i][0]

query_file = "data/exp_scaling_queries_z_u.txt"
query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, query_file)

dc_exp.query_file = query_file
res = dc_exp.run()
for i in range(query_number):
    mat[i][1] = res[i][0]

# query_file = "data/exp_scaling_queries_u_zoomin.txt"
# query_generator.generate_u_zoomin_query(col_number, row_number, 17, query_number, query_file)
#
# dc_exp.query_file = query_file
# res = []
# res.append(dc_exp.run())
# for i in range(query_number):
#     mat[2][len(res) - 1] = res[len(res) - 1][i][0]
#
# query_file = "data/exp_scaling_queries_u_zoomin.txt"
# query_generator.generate_u_zoomin_query(col_number, row_number, 17, query_number, query_file)
#
# dc_exp.query_file = query_file
# res = []
# res.append(dc_exp.run())
# for i in range(query_number):
#     mat[3][len(res) - 1] = res[len(res) - 1][i][0]

df = pd.DataFrame(mat)
df.columns = ["U", "Z"]
step = query_number / 20
df = df.groupby(lambda x:x/step).sum()
df /= step
df *= 1000
df.index = range(step, query_number + step, step)
df.to_csv("result/exp_scaling_queries_plot.csv")