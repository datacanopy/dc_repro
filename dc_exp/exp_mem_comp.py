import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

col_number = 10#0
row_number = 4000000#0


data_file = "data/exp_data.csv"

query_number = 1000
step = 10
ub = row_number / 10
lb = row_number / 100 * 5

import r_util
import monetdb_util
import dc_util
import numpy_util

query_file = "test.csv"

column_name = []

numpy_exp = numpy_util.ExpNumpy(query_file, col_number, row_number)
column_name.append('NumPy (Python)')
exp_R = r_util.ExpR(query_file, col_number, row_number)
column_name.append('Modeltools (R)')
exp_monetdb = monetdb_util.ExpMonetdb(query_file, col_number, row_number)
column_name.append('MonteDB')
dc_exp = dc_util.ExpDC(query_file, col_number, row_number, 60)
column_name.append('Data Canopy')


query_file = "data/exp_mem_comp_u_u.txt"
query_generator.generate_uniform_query(col_number, row_number, query_number, lb, ub, query_file)

numpy_exp.query_file = query_file
exp_R.query_file = query_file
exp_monetdb.query_file = query_file
dc_exp.query_file = query_file
res = []
res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())

mat = [[0 for i in range(len(res))] for j in range(query_number)]
for i in range(query_number):
    for j in range(len(res)):
        mat[i][j] = res[j][i]
df = pd.DataFrame(mat)
df.columns = column_name
df.index = range(query_number)
df = df.groupby(lambda x:x/step).sum()
df.index = range(step, query_number + step, 10)
df *= 1000
df.to_csv("result/exp_mem_comp_u_u_plot_2000_test.csv")


query_file = "data/exp_mem_comp_z_u.txt"
query_generator.generate_zipfian_query(col_number, row_number, query_number, lb, ub, query_file)

numpy_exp.query_file = query_file
exp_R.query_file = query_file
exp_monetdb.query_file = query_file
dc_exp.query_file = query_file

res = []
res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())
mat = [[0 for i in range(len(res))] for j in range(query_number)]
for i in range(query_number):
    for j in range(len(res)):
        mat[i][j] = res[j][i]
df = pd.DataFrame(mat)
df.columns = column_name
df.index = range(query_number)
df = df.groupby(lambda x:x/step).sum()
df.index = range(step, query_number + step, 10)
df *= 1000
df.to_csv("result/exp_mem_comp_z_u_plot_2000_test.csv")


query_file = "data/exp_mem_comp_u_zoomin.txt"
query_generator.generate_u_zoomin_query(col_number, row_number, 10, query_number, query_file)

numpy_exp.query_file = query_file
exp_R.query_file = query_file
exp_monetdb.query_file = query_file
dc_exp.query_file = query_file
res = []
res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())
mat = [[0 for i in range(len(res))] for j in range(query_number)]
for i in range(query_number):
    for j in range(len(res)):
        mat[i][j] = res[j][i]
df = pd.DataFrame(mat)
df.columns = column_name
df.index = range(query_number)
df = df.groupby(lambda x:x/step).sum()
df.index = range(step, query_number + step, 10)
df *= 1000
df.to_csv("result/exp_mem_comp_u_zoomin_plot_2000_test.csv")


query_file = "data/exp_mem_comp_z_zoomin.txt"
query_generator.generate_z_zoomin_query(col_number, row_number, 10, query_number, query_file)

numpy_exp.query_file = query_file
exp_R.query_file = query_file
exp_monetdb.query_file = query_file
dc_exp.query_file = query_file
res = []
res.append(numpy_exp.run())
res.append(exp_R.run())
res.append(exp_monetdb.run())
res.append(dc_exp.run())
mat = [[0 for i in range(len(res))] for j in range(query_number)]
for i in range(query_number):
    for j in range(len(res)):
        mat[i][j] = res[j][i]
df = pd.DataFrame(mat)
df.columns = column_name
df.index = range(query_number)
df = df.groupby(lambda x:x/step).sum()
df.index = range(step, query_number + step, 10)
df *= 1000
df.to_csv("result/exp_mem_comp_z_zoomin_plot_2000_test.csv")