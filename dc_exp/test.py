import numpy as np
import time
import random
import pandas as pd
import signal
import subprocess
import os

col_number = 100
row_number = 40000000
query_number = 2000

# for query_file in ["data/exp_mem_comp_u_u.txt", "data/exp_mem_comp_z_u.txt",
#                   "data/exp_mem_comp_u_zoomin.txt", "data/exp_mem_comp_z_zoomin.txt"]:
#
#     pid = os.getpid()
#     numpy_data = np.random.uniform(size=(col_number, row_number)).astype("double")
#     numpy_query_file = query_file
#
#     result = []
#     fin = open(query_file)
#     q_str = fin.read().split('\n')
#     if q_str[-1] == '':
#         q_str = q_str[:-1]
#     query_list = [[int(val) for val in query.split(' ')] for query in q_str][:query_number]
#     fin.close()
#     pc = subprocess.Popen(["perf","stat", "-e", "'cache-misses'", "-p", "%d" % pid],
#                           shell=False, stderr=subprocess.PIPE)
#     start_time = time.time()
#     for query in query_list:
#         start_time = time.time()
#         if query[0] == 1:
#             np.mean(numpy_data[query[1], query[3]:query[4]])
#         elif query[0] == 2:
#             np.var(numpy_data[query[1], query[3]:query[4]])
#         elif query[0] == 3:
#             np.std(numpy_data[query[1], query[3]:query[4]])
#         elif query[0] == 4:
#             np.corrcoef(numpy_data[query[1], query[3]:query[4]], numpy_data[query[2], query[3]:query[4]])
#         elif query[0] == 5:
#             np.cov(numpy_data[query[1], query[3]:query[4]], numpy_data[query[2], query[3]:query[4]])
#         result.append((time.time() - start_time))
#
#     os.kill(pc.pid, signal.SIGINT)
#     print pc.stderr.read()


# for query_file in ["data/exp_mem_comp_u_u.txt", "data/exp_mem_comp_z_u.txt",
#                   "data/exp_mem_comp_u_zoomin.txt", "data/exp_mem_comp_z_zoomin.txt"]:
#     rpc = subprocess.Popen(["Rscript", "./query_process_test.R", "%d" % col_number, "%d" % row_number, "%s" % query_file],
#                            stdout=subprocess.PIPE)
#     print "Wait for init"
#     rpc.stdout.readline()
#     print "Start perf"
#     pc = subprocess.Popen(["perf","stat", "-e", "'cache-misses'", "-p", "%d" % rpc.pid],
#                           shell=False, stderr=subprocess.PIPE)
#     rpc.wait()
#
#     os.kill(pc.pid, signal.SIGINT)
#     print pc.stderr.read()

import monetdb_util
exp_monetdb = monetdb_util.ExpMonetdb('')
for query_file in ["data/exp_mem_comp_u_u.txt", "data/exp_mem_comp_z_u.txt",
                   "data/exp_mem_comp_u_zoomin.txt", "data/exp_mem_comp_z_zoomin.txt"]:
    exp_monetdb.query_file = query_file
    pid = 102577#os.getpid()
    pc = subprocess.Popen(["perf","stat", "-e", "'cache-misses'", "-p", "%d" % pid],
                          shell=False, stderr=subprocess.PIPE)
    exp_monetdb.run()
    os.kill(pc.pid, signal.SIGINT)
    print pc.stderr.read()