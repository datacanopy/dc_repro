import numpy as np
import os
import data
import pandas as pd
import plot
import random
import bisect
from query_generator import *

col_number = 100
row_number = 100000000
chunk_size = row_number * 0.001
# data_file = "data/exp4_data.csv"
# data.generateData(data_file, row_number, col_number)

query_file = "data/exp7_dc_mod1_zipfian.txt"
query_number = 10000
q_len = row_number / 100
q_len_lb = row_number * 0.05
q_len_up = row_number * 0.10

generate_zipfian_query(col_number, row_number, query_number, q_len_lb, q_len_up, query_file)

print "Exp7 queries generated"

res = []

column_name = []

result = [[],[]]
log = os.popen("./exp_query_process %d %d %d %s" % (col_number, row_number, chunk_size, query_file)).read()
log_list = log.split('\n')[:-1]
for row in log_list[4:]:
    result[0].append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))

column_name.append('Data Canopy')

log = os.popen("./exp_query_mod1_process %d %d %d %s" % (col_number, row_number, chunk_size, query_file)).read()
log_list = log.split('\n')[:-1]
for row in log_list[4:]:
    result[1].append((float(row.split(' ')[0]), int(row.split(' ')[1]), int(row.split(' ')[2])))

column_name.append('Data Canopy MOD1')

mat = []
for i in range(len(result[0])):
    mat.append([result[0][i][0], result[1][i][0]])

df = pd.DataFrame(mat, index=range(query_number))
df.columns = column_name
df.to_csv("result/exp7_dc_mod1_zipfian.csv")
# plot.PlotBarChartFromExcel(df, "graph/exp4", "Query Sequence", "Total response time (s)")