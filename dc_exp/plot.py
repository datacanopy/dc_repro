
import csv
import pandas as pd
import numpy as np
import Gnuplot as gp
import os as os
def PlotSimple(filename,x_value,y_qtys,outfile,ylabel,log):

	
	open_file = pd.read_csv(filename)
	x = np.array(open_file[x_value])

	# instantiate gnuplot
	g = gp.Gnuplot()

	# initial formatting options
	g("set key left")
	g("set terminal pdf enhanced font 'times,16'")
	#g("set grid")
	#g("set style pt 4")
	
	#Set line formats
	#Set line formats
	g("set style line 1 lt 7 lw 3 lc 9")
	g("set style line 2 lt 9 lw 3 lc 9")
	g("set style line 3 lt 5 lw 3 lc 9")

	# initialize an array for plot data
	plot_data = []
	i=1
	
	# create gnuplot data
	for y_qty in y_qtys:
		y = np.array(open_file[y_qty])
		y_label = y_qty.replace("_"," ")
		plot_data.append(gp.Data(x,y,with_="lp ls "+str(i),title=y_label))
		i+=1

	# format the labels
	x_label = x_value.replace("_"," ")

	# Output and labels
	g("set output '"+outfile+".pdf'")
	#g("set format y '%sx10^{%S}'")
	g("set xlabel '"+x_label+"'")
	g("set ylabel '"+ylabel+"'")
	
	g("set format y '%1.2e'")
	g("set format x '%1.0e'")

	if log:
		g("set logscale x")

	#plot
	g.plot(plot_data[2],plot_data[1],plot_data[0])
	g("set logscale y 2")
	g("set output '"+outfile+"_log.pdf'")
	g.plot(plot_data[2],plot_data[1],plot_data[0])


def PlotStacked(filename,x_value,y_qtys,outfile,ylabel,log):

	
	open_file = pd.read_csv(filename)
	x = np.array(open_file[x_value])

	# instantiate gnuplot
	g = gp.Gnuplot()

	# initial formatting options
	g("set key left")
	g("set terminal pdf enhanced font 'times,16'")
	g("set style fill  pattern border")
	#g(set grid")
	#g("set style pt 4")
	
	#Set line formats
	g("set style line 1 lt 7 lw 3")
	g("set style line 2 lt 9 lw 3")
	g("set style line 3 lt 0 lw 3")

	# initialize an array for plot data
	plot_data = []
	i=1
	
	# create gnuplot data
	
	y_so_far = np.zeros(len(np.array(open_file[y_qtys[0]])))
	
	pattern=[2,6,9]
	for j in range(0,len(y_qtys)):
		y = np.array(open_file[y_qtys[j]])
		y_so_far=y_so_far+y
		y_label = y_qtys[j].replace("_"," ")
		plot_data.append(gp.Data(x,y_so_far,np.zeros(len(np.array(open_file[y_qtys[0]]))),with_="filledcurves above fillstyle pattern "+str(pattern[i-1])+" ls "+str(i) ,title=y_label))
		i+=1

	# format the labels
	x_label = x_value.replace("_"," ")

	# Output and labels
	g("set output '"+outfile+".pdf'")
	#g("set format y '%sx10^{%S}'")
	g("set xlabel '"+x_label+"'")
	g("set ylabel '"+ylabel+"'")
	
	g("set format y '%1.2e'")
	g("set format x '%1.0e'")

	if log:
		g("set logscale x")

	#plot
	g.plot(plot_data[2],plot_data[1],plot_data[0])
	g("set logscale y 2")
	g("set output '"+outfile+"_log.pdf'")
	g.plot(plot_data[2],plot_data[1],plot_data[0])

def PlotSimpleStacked(filename,x_value,y_qtys,y_qtys_stacked,outfile,ylabel,log):
	open_file = pd.read_csv(filename)
	x = np.array(open_file[x_value])

	# instantiate gnuplot
	g = gp.Gnuplot()

	# initial formatting options
	g("set key left")
	g("set terminal pdf enhanced font 'times,16'")
	g("set style fill  pattern border")
	g("set tics front")

	#g("set key outside")
	g("set key horizontal left top")
	g("set key font 'times,14'")
	g("set key samplen 2 spacing .7")
	#g("set key autotitle columnhead")


	#g(set grid")
	#g("set grid")
	#g("set style pt 4")
	
	#Set line formats
	g("set style line 1 lt 7 lw 3 lc 7")
	g("set style line 2 lt 9 lw 3 lc 7")
	g("set style line 3 lt 5 lw 3 lc 9")
	g("set style line 4 lt 8 lw 3 lc 9")


	# initialize an array for plot data
	plot_data = []
	i=1
	
	# create gnuplot data
	for y_qty in y_qtys:
		y = np.array(open_file[y_qty])
		y_label = y_qty.replace("_"," ")
		plot_data.append(gp.Data(x,y,with_="lp ls "+str(i),title=y_label))
		i+=1

	# create gnuplot data stacked
	i=1
	y_so_far = np.zeros(len(np.array(open_file[y_qtys_stacked[0]])))
	
	pattern=[2,6,9]
	for j in range(0,len(y_qtys_stacked)):
		y = np.array(open_file[y_qtys_stacked[j]])
		y_so_far=y_so_far+y
		y_label = y_qtys_stacked[j].replace("_"," ")
		plot_data.append(gp.Data(x,y_so_far,np.zeros(len(np.array(open_file[y_qtys[0]]))),with_="filledcurves above lc 9 fillstyle pattern "+str(pattern[i-1]) ,title=y_label))
		i+=1

	print len(plot_data)
	# format the labels
	x_label = x_value.replace("_"," ")

	# Output and labels
	g("set output '"+outfile+".pdf'")
	g("set format y '%.2tx10^{%S}'")
	g("set format x '%.0tx10^{%S}'")
	g("set xlabel '"+x_label+"'")
	g("set ylabel '"+ylabel+"'")

	g("set xrange ["+str(min(x))+":"+str(max(x))+"]")
	
	
	if log:
		g("set logscale x")

	#plot
	g.plot(plot_data[5],plot_data[4],plot_data[3],plot_data[2],plot_data[1],plot_data[0])
	g("set logscale y 2")
	g("set output '"+outfile+"_log.pdf'")
	g.plot(plot_data[5],plot_data[4],plot_data[3],plot_data[2],plot_data[1],plot_data[0])

def PlotSimpleBarChart(filename,outfile,x_label,y_label):
	os.system("gnuplot -e \"ifile='"+filename+"';ofile='"+outfile+".pdf';x_label='"+x_label+"';y_label='"+y_label+"'\" ./gnuplot/barChart.gp")

def PlotSimpleBarChartMulti(filename,outfile,x_label,y_label):
	os.system("gnuplot -e \"ifile='"+filename+"';ofile='"+outfile+".pdf';x_label='"+x_label+"';y_label='"+y_label+"'\" ./gnuplot/barChartMulti.gp")

def PlotSimpleLinePlot(filename,outfile,x_label,y_label, log, dot):
	os.system("gnuplot -e \"ifile='"+filename+"';ofile='"+outfile+".pdf';x_label='"+x_label+"';y_label='"+y_label+"';log='"+str(log)+"';dot='"+str(dot)+ "'\" ./gnuplot/linePlot.gp")

def PlotMultiBarLine(filename1,filename2,outfile,x_label,y_label_1,y_label_2):
	os.system("gnuplot -e \"ifileone='"+filename1+"';ifiletwo='"+filename2+"';ofile='"+outfile+".pdf';x_label='"+x_label+"';y_label_1='"+y_label_1+"';y_label_2='"+y_label_2+"'\" ./gnuplot/multiBarLine.gp")


def parseFile(filename):

	f = open(filename)
	b = open('test.csv', 'w')
	a = csv.writer(b)
	i=0

	result =[]
	caption = []
	cap=False

	for line in f:
		
		if line[0]=="*":
			i=0
			#continue
			if not cap:	
				a.writerow(caption)
				cap = True
			a.writerow(result)
			result=[]
			caption=[]
			continue
		data = line.split(",")
		size = len(data)
		result.append(data[0].strip('\n'))
		caption.append(data[size-1].strip('\n'))
	b.close()

#parseFile('out')


def PlotBarChartFromExcel(excel, o_file, x_label, y_label):
	tmp_filename = "tmp.csv"
	excel.to_csv(tmp_filename)
	PlotSimpleBarChart(tmp_filename, o_file, x_label, y_label)
	os.system("rm %s" % tmp_filename)

