import numpy as np
import os
import data
import pandas as pd
import plot
import random
import query_generator

print "Running experiments for figure13 [Small version]"



col_number = 10
row_number = 4000000
n_clasees = 40

print "Number of columns: " + str(col_number)
print "Number of rows: " + str(row_number)


def GenerateSimpleLinearRegressionWorkload(col_number, row_number, query_file):
    with open(query_file, "w") as fout:
        for i in range(col_number):
            for j in range(i + 1, col_number):
                fout.write("%d %d %d %d %d\n" % (4, i, j, 0, row_number))
                fout.write("%d %d %d %d %d\n" % (3, i, -1, 0, row_number))
                fout.write("%d %d %d %d %d\n" % (3, j, -1, 0, row_number))
                fout.write("%d %d %d %d %d\n" % (1, i, -1, 0, row_number))
                fout.write("%d %d %d %d %d\n" % (1, j, -1, 0, row_number))


def GenerateBayesianClassificationWorkload(col_number, row_number, n_classes, query_file):
    item_in_classes = row_number / n_classes
    with open(query_file, "w") as fout:
        for c in range(n_classes):
            for i in range(col_number):
                for j in range(i + 1, col_number):
                    fout.write("%d %d %d %d %d\n" % (5, i, j, c * item_in_classes, (c + 1) * item_in_classes))
        for i in range(col_number):
            for j in range(n_classes):
                fout.write("%d %d %d %d %d\n" % (3, i, -1, j * item_in_classes, (j + 1) * item_in_classes))
                fout.write("%d %d %d %d %d\n" % (1, i, -1, j * item_in_classes, (j + 1) * item_in_classes))

def GenerateCollaborativeFilteringWorload(col_number, row_number, query_file):
    with open(query_file, "w") as fout:
        for i in range(col_number):
            for j in range(i + 1, col_number):
                fout.write("%d %d %d %d %d\n" % (4, i, j, 0, row_number))

chunk_size = 60
query_file = "data/exp_ml.txt"
result_file = "result/exp_ml.txt"
ans = [[0.0, 0.0, 0.0] for j in range(3)]

GenerateSimpleLinearRegressionWorkload(col_number, row_number, query_file)

os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, row_number + 1, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[0][0] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[0][1] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s 1 > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[0][2] += float(content[i].split(' ')[0])


GenerateBayesianClassificationWorkload(col_number, row_number, n_clasees, query_file)

os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, row_number + 1, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[1][0] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[1][1] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s 1 > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[1][2] += float(content[i].split(' ')[0])

GenerateCollaborativeFilteringWorload(col_number, row_number, query_file)

os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, row_number + 1, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[2][0] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[2][1] += float(content[i].split(' ')[0])
os.system("build/exp_query_process %d %d %d %s 1 > %s"
          % (col_number, row_number, chunk_size, query_file, result_file))
with open(result_file) as fin:
    content = fin.read().split('\n')[5:-1]
    query_number = len(content)
    for i in range(query_number):
        ans[2][2] += float(content[i].split(' ')[0])



pd.DataFrame(ans, columns=["Statsys", "Online", "Offline"],
             index=["Simple Linear Regression", "Bayesian Classification", "Collaborative Filtering"]).to_csv("result/figure13_small.csv")
os.system("gnuplot dc_exp/gnuplot/exp/figure13_small.gp")
