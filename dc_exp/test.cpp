#include <stdio.h>
#include <proc/readproc.h>

int main() {
  struct proc_t usage;
  look_up_our_self(&usage);
  printf("usage: %lu\n", usage.vsize);
  int *w = new int[10000];
  look_up_our_self(&usage);
  printf("usage: %lu\n", usage.vsize);
  delete []w;
  int *i = new int[1000];
  look_up_our_self(&usage);
  printf("usage: %lu\n", usage.vsize);
  return 0;
}