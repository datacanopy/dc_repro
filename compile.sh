rm -rf build
rm -rf data
rm -rf graph
rm -rf result
mkdir build
mkdir data
mkdir graph
mkdir result
cd build
cmake ../dc_system/
make -j4
cd ../

# #echo "Generating Figure 11..."
# #python2 dc_exp/exp_mem_comp.py
# echo "Generating Figure 12..."
# python2 dc_exp/exp_construct_query.py
# # echo "Generating Figure 13..."
# # python2 dc_exp/exp_ml.py
# # echo "Generating Figure 14..."
# # python2 dc_exp/exp_scaling_data.py
# # echo "Generating Figure 15..."
# # python2 dc_exp/exp_scaling_column.py
# # echo "Generating Figure 16..."
# # python2 dc_exp/exp_out_of_memory.py

