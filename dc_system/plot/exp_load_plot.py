import plot
import pandas as pd
for size in [7, 8]:
    dt = pd.read_csv("../result/exp_load_%d.csv" % size, index_col=False)
    dt.iloc[:, 0] = dt.iloc[:, 0].astype('int')
    dt.iloc[:, 4] = dt.iloc[:, 4].astype('int')
    dt.iloc[:, 3] = dt.iloc[:, 3] - dt.iloc[:, 6]
    tmp = dt.iloc[:, (0, 6, 3)]
    tmp.columns = ['Chunk size', 'Access base data', 'Inserting to DC']
    tmp.iloc[0,0] = "10^3"
    tmp.iloc[1,0] = "10^4"
    tmp.iloc[2,0] = "10^5"
    tmp.iloc[3,0] = "10^6"
    # sm = tmp.iloc[:, 1] + tmp.iloc[:, 2]
    # tmp.iloc[:, 1] = tmp.iloc[:, 1] / sm * 100
    # tmp.iloc[:, 2] = tmp.iloc[:, 2] / sm * 100
    tmp.to_csv('graph/exp_load_%d_time.csv' % size, index=False)

    plot.PlotSimpleBarChart('graph/exp_load_%d_time.csv' % size, 'graph/exp_load_%d_time' % size, 'Chunk size', 'Total execution time (s)', 0)
    plot.PlotSimpleBarChart('graph/exp_load_%d_time.csv' % size, 'graph/exp_load_%d_time_log' % size, 'Chunk size', 'Total execution time (s)', 1)
    # dt.iloc[:, (0, 4)].to_csv('graph/exp_load_8_caches_misses.csv', index=False)
    # plot.PlotSimpleBarChart('graph/exp_load_8_caches_misses.csv', 'graph/exp_load_8_caches_misses', 'Chunk size', 'Cache misses', 0)