### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'times,16'
set datafile separator ","

# Appearance
#set key outside 
#set key horizontal top outside
set key vertical top inside
set key autotitle columnhead
set offset 0,0,graph 0.35,0

# Labels, plot axis and tics
set format y '%.1tx10^{%S}'
set format x '%.1tx10^{%S}'
set xlabel x_label
set ylabel y_label
#set xtics rotate by 45 offset -0.8,-1.8

# logscale or not
if (log ==1) set logscale y

# Line styles
set style line 1 lt 7 lw 3 lc 9
set style line 2 lt 9 lw 3 lc 9
set style line 3 lt 5 lw 3 lc 9
set style line 4 lt 8 lw 3 lc 9

# Output settings
set output ofile

###

# Plot specific formatting
#set style data histograms
set boxwidth 1
set style fill pattern
#set yrange [1:]
set autoscale y

# logscale or not
if (log ==1) set logscale y
if (log ==1) set yrange [1:]



# Plot
plot ifile using 2:xtic(1) with lp ls 1,\
	"" using 3 with lp ls 2,\
	"" using 4 with lp ls 3,\
	"" using 5 with lp ls 4,\

#plot ifile using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
#	"" using 3 with histograms lc 9 fill solid 0.5,\
	

#plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
#	"" using 5 with histograms lc 9 fill solid 1,\