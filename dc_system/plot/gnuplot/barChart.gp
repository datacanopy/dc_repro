### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'Times-Roman, 15' monochrome
set datafile separator ","

# Appearance
set key inside
#set key outside
set key horizontal top right inside
set key autotitle columnhead font 'Times-Roman, 12'
#set key off
set offset -0.3,-0.3,graph 0.1,0

# Labels, plot axis and tics
#set format y '%.1tx10^{%S}'
set format y '%g'
set format x '%.0tx10^{%S}'
set xlabel x_label
set ylabel y_label
#set xtics rotate by -45 scale 0 autojustify
set xtics nomirror

# logscale or not
if (log ==1) set logscale y

# Output settings
set output ofile

###

# Plot specific formatting
set style data histograms
#set style histogram rowstacked
set boxwidth 1 relative
set style fill pattern
set yrange [:1000]
#set autoscale y



# Plot
plot ifile using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
	"" using 3 with histograms fill transparent solid 0.25 border dt 1,\
#	"" using 4 with histograms lc 9 fillstyle pattern 2,\
#	"" using 5 with histograms lc 9 fill solid 1,\
#	"" using 6 with histograms lc 9 fillstyle pattern 6,\

#plot ifile using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
#	"" using 3 with histograms lc 9 fill solid 0.5,\
	

#plot ifile using 4:xtic(1) with histograms lc 9 fillstyle pattern 2,\
#	"" using 5 with histograms lc 9 fill solid 1,\
