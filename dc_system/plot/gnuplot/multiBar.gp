 ### Generic Params

# Enviorment settings
set terminal pdf enhanced font 'times,12'
set datafile separator ","

# Output settings
set output ofile

###
# Plot specific formatting
set style data histograms
set boxwidth 1
set style fill pattern
#set autoscale y
set yrange [1:]
#set tmargin 0
#set bmargin 0
set lmargin 13
set rmargin 8


###
# Individual font sizes
set key font "times,12"
set xtics font "times,12" 
set xlabel font "times,12"
set ylabel font "times,12"

# multiple plots
set multiplot layout 2,1


# Location
set size 1,0.45
set origin 0,0.5

#set yrange [0:1]

#### PLOT 1 ####
	set offsets 0,0,graph 0.4,0
	if (log ==1) set logscale y
	# Appearance
	#set key horizontal 
	set key vertical top inside
	set key autotitle columnhead
	#unset key
	unset xtics
	set xlabel x_label_1
	set ylabel y_label_1

	# Labels, plot axis and tics
	set format x '%.0tx10^{%S}'
	set format y '%.0tx10^{%S}'

	# line styles
	set style line 1 lt 7 lw 3 lc 9
	set style line 2 lt 9 lw 3 lc 9

	# Plot 1
	plot ifileone using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
	"" using 3 with histograms lc 9 fill solid 0.5,\
	"" using 4 with histograms lc 9 fillstyle pattern 2,\
	"" using 5 with histograms lc 9 fill solid 1,	

# logscale or not
if (log ==1) set logscale y
#set yrange [1:100000000000000]


#### PLOT 2 ####
	set offsets 0,0,0,0
	# Appearance
	set key vertical top right 
	set key autotitle columnhead
	unset key
	set autoscale x
	
	# Labels, plot axis and tics
	set format y '%.0tx10^{%S}'
	set format x '%.0tx10^{%S}'
	set xlabel x_label_2
	set ylabel y_label_2
	set xtics 1
	
	# logscale or not
	if (log ==1) set logscale y

	set size 1,0.5

	# Plot 2
	plot ifiletwo using 2:xtic(1) with histograms lc 9 fillstyle pattern 1,\
	"" using 3 with histograms lc 9 fill solid 0.5,\
	"" using 4 with histograms lc 9 fillstyle pattern 2,\
	"" using 5 with histograms lc 9 fill solid 1,\