//
// Created by Xinding Wei on 16/10/3.
//
#include <random>
#include <cstdio>
#include <iostream>
#include <limits.h>
using namespace std;

int main(int argc, char **argv){
    string data_filename;
    int col_number, row_number;
    if (argc == 4) {
        col_number = atoi(argv[1]);
        row_number = atoi(argv[2]);
        data_filename = argv[3];
    }
    else{
        cout << "Usage: ./<binary filename> <col number> <row number> <output filename>" << endl;
        return 0;
    }
    srand(time(0));
    FILE *fin_data;
    fin_data = fopen(data_filename.c_str() , "w");
    for (int col = 0; col < col_number; col++)
        if(col)
            fprintf(fin_data, ",col%d", col);
        else
            fprintf(fin_data, "col%d", col);
    fprintf(fin_data, "\n");
    for (int row = 0; row < row_number; row++) {
        for (int col = 0; col < col_number; col++)
            if(col)
                fprintf(fin_data, ",%.3f", (double(rand()) / INT_MAX) * (INT_MAX / 100));
            else
                fprintf(fin_data, "%.3f", (double(rand()) / INT_MAX) * (INT_MAX / 100));
        fprintf(fin_data, "\n");
    }
    fclose(fin_data);
    return 0;
}