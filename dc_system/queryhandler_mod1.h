//
// Created by Xinding Wei on 16/8/18.
//

#ifndef QUERY_HANDLER_MOD1_H
#define QUERY_HANDLER_MOD1_H

#include "segmenttree.h"
#include "aggregatetype.h"
#include <map>
#include "data.h"
#include "tools/timer.hh"

#ifdef MEASURE_FETCH_TIME

#include "tools/timer.hh"

#endif

typedef std::pair<std::pair<int64_t, int64_t>, int> ColumnInfo;


template<class T>
class QueryHandlerMod1 {
    private:
        typedef std::shared_ptr<SegmentTree<T>> SegmentTreePtr;

        struct SegmentTreeInfo {
            SegmentTreeInfo(SizeType chunk_size, SegmentTreePtr st) : chunk_size(chunk_size), st(st) {
            }

            SegmentTreeInfo(const SegmentTreeInfo &x) {
                this->chunk_size = x.chunk_size;
                this->st = x.st;
            }

            SegmentTreeInfo &operator=(const SegmentTreeInfo &x) {
                this->chunk_size = x.chunk_size;
                this->st = x.st;
            }

            SizeType chunk_size;
            SegmentTreePtr st;
        };

        typedef std::map<ColumnInfo, SegmentTreeInfo> ColSegMap;

    public:
        QueryHandlerMod1(std::shared_ptr<Data<T>> data) : data_(data) {

        }

        double BuildAllColumnTest1(){
            using namespace std;
            double insert_time = 0;
            Timer timer;
            int chunk_size = GetChunkSize();
            int chunk_number = data_->GetNumberOfRows() / chunk_size;
            int col_number = data_->GetNumberOfColumns();
            shared_ptr<vector<pair<int64_t, T>>> values = make_shared<vector<pair<int64_t, T>>>();

            for (SizeType col = 0; col < col_number; col++) {
                values->clear();
                auto key = make_column_info(col, -1, SUM);
                for (SizeType i = 0; i < chunk_number; i++)
                    values->push_back(make_pair(i, data_->FetchData(col, 0, SUM, i * chunk_size,
                                                                    (i + 1) * chunk_size - 1)));
                timer.start();
                auto tree_ptr = dynamic_pointer_cast<SegmentTree<T>>(
                        make_shared<PrefixSumArray<T>>(values, 0, chunk_number - 1,
                                                       GetSupport<T>(SUM)));
                st_dict_.insert(make_pair(key, SegmentTreeInfo(chunk_size, tree_ptr)));
                timer.end();
                insert_time += timer.getDiff();

            }


//            for (SizeType col = 0; col < col_number; col++) {
//                values->clear();
//                auto key = make_column_info(col, -1, SQUARE_SUM);
//                for (SizeType i = 0; i < chunk_number; i++)
//                    values->push_back(make_pair(i, data_->FetchData(col, 0, SQUARE_SUM, i * chunk_size,
//                                                                    (i + 1) * chunk_size - 1)));
//
//                timer.start();
//                auto tree_ptr = dynamic_pointer_cast<SegmentTree<T>>(
//                        make_shared<PrefixSumArray<T>>(values, 0, chunk_number - 1,
//                                                       GetSupport<T>(SQUARE_SUM)));
//                st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, tree_ptr)));
//                timer.end();
//                insert_time += timer.getDiff();
//
//            }
            return insert_time;
        }
        double BuildAllColumnTest2(){
            using namespace std;
            int chunk_size = GetChunkSize();
            int chunk_number = data_->GetNumberOfRows() / chunk_size;
            int col_number = data_->GetNumberOfColumns();

            shared_ptr<vector<pair<int64_t, T>>> values1 = make_shared<vector<pair<int64_t, T>>>(),
                    values2 = make_shared<vector<pair<int64_t, T>>>();

            double insert_time = 0;
            Timer timer;
            for (SizeType col = 0; col < col_number; col++){
                for (int now = 0; now < chunk_number; now++) {
                    auto tmp = data_->FetchDataCombine(col, -1, SUM1_FLAG | SQ_SUM1_FLAG, now * chunk_size, (now + 1) * chunk_size - 1);
                    values1->push_back(make_pair(now, tmp.sum1));
                    values2->push_back(make_pair(now, tmp.sq_sum1));
                }
                auto key1 = make_column_info(col, -1, SUM);
                auto key2 = make_column_info(col, -1, SQUARE_SUM);

                timer.start();
                auto tree_ptr = dynamic_pointer_cast<SegmentTree<T>>(
                        make_shared<PrefixSumArray<T>>(values1, 0, chunk_number - 1,
                                                       GetSupport<T>(SUM)));
                st_dict_.insert(make_pair(key1, SegmentTreeInfo(chunk_size, tree_ptr)));
                tree_ptr = dynamic_pointer_cast<SegmentTree<T>>(
                        make_shared<PrefixSumArray<T>>(values2, 0, chunk_number - 1,
                                                       GetSupport<T>(SQUARE_SUM)));
                st_dict_.insert(make_pair(key2, SegmentTreeInfo(chunk_size, tree_ptr)));
                timer.end();
                insert_time += timer.getDiff();
            }
            return insert_time;
        }

        T
        QueryStatistic(SizeType col1, SizeType col2, StatisticTypeEnum statistic_type, SizeType left, SizeType right) {
            SizeType n = right - left;
            AggregateTypeFlag aggregate_flag = 0;
            switch (statistic_type) {
                case MEAN:
                    aggregate_flag = SUM1_FLAG;
                    break;
                case VARIANCE:
                    aggregate_flag = SUM1_FLAG | SQ_SUM1_FLAG;
                    break;
                case STANDARD_DEVIATION:
                    aggregate_flag = SUM1_FLAG | SQ_SUM1_FLAG;
                    break;
                case CORRELATION:
                    aggregate_flag = MUL_FLAG | SUM1_FLAG | SQ_SUM1_FLAG | SUM2_FLAG | SQ_SUM2_FLAG;
                    break;
                case COVARIANCE:
                    aggregate_flag = MUL_FLAG | SUM1_FLAG | SUM2_FLAG;
                    break;
            }


            SizeType chunk_size = GetChunkSize();
            SizeType col_chunk_number = data_->GetNumberOfRows() / chunk_size;
            T ans;
            bool flag = false;
            SizeType left_chunk = left / chunk_size, right_chunk = (right - 1) / chunk_size;

            if(aggregate_flag & MUL_FLAG)
                LoadChunkMultiColumn(col1, col2, left_chunk, right_chunk, chunk_size);
            else
                LoadChunkSingleColumn(col1, left_chunk, right_chunk, chunk_size);


            switch (statistic_type) {
                case MEAN:
                    return Query(col1, col2, SUM, left, right) / n;
                case VARIANCE: {
                    T square = Query(col1, col2, SQUARE_SUM, left, right);
                    T avg = Query(col1, col2, SUM, left, right) / n;
                    return square / n - avg * avg;
                }
                case STANDARD_DEVIATION: {
                    T square = Query(col1, col2, SQUARE_SUM, left, right);
                    T avg = Query(col1, col2, SUM, left, right) / n;
                    return sqrt(square / n - avg * avg);
                }
                case CORRELATION: {
                    T cor = Query(col1, col2, MUL_SUM, left, right);
                    T sum1 = Query(col1, col2, SUM, left, right);
                    T sum2 = Query(col2, col1, SUM, left, right);
                    T square1 = Query(col1, col2, SQUARE_SUM, left, right);
                    T square2 = Query(col2, col1, SQUARE_SUM, left, right);
                    T std1 = sqrt(square1 - sum1 * sum1 / n);
                    T std2 = sqrt(square2 - sum2 * sum2 / n);
                    return (cor - sum1 * sum2 / n) / (std1 * std2);
                }
                case COVARIANCE: {
                    T cor = Query(col1, col2, MUL_SUM, left, right);
                    T sum1 = Query(col1, col2, SUM, left, right);
                    T sum2 = Query(col2, col1, SUM, left, right);
                    return (cor / n) - ((sum1 / n) * (sum2 / n));
                }
            }
            return 0;
        }

        void LoadChunkSingleColumn(SizeType col1, SizeType left_chunk, SizeType right_chunk, SizeType chunk_size){
            auto sum1_key = make_column_info(col1, -1, SUM);
            auto sq_sum1_key = make_column_info(col1, -1, SQUARE_SUM);
            auto sum1_it = FindSegmentTree(sum1_key);
            auto sq_sum1_it = FindSegmentTree(sq_sum1_key);
            auto sum1_res = sum1_it->second.st->Query(left_chunk, right_chunk).first;
            auto sq_sum1_res = sq_sum1_it->second.st->Query(left_chunk, right_chunk).first;

            DataPayload<int> pos;
            pos.sum1 = 0;
            pos.sq_sum1 = 0;

            std::vector<std::pair<int64_t, T>> sum1_bulk;
            std::vector<std::pair<int64_t, T>> sq_sum1_bulk;

            while(true){
                int now = right_chunk + 1;
                if(pos.sum1 < sum1_res->size() && sum1_res->at(pos.sum1).first < now)
                    now = sum1_res->at(pos.sum1).first;
                if(pos.sq_sum1 < sq_sum1_res->size() && sq_sum1_res->at(pos.sq_sum1).first < now)
                    now = sq_sum1_res->at(pos.sq_sum1).first;
                if(now == right_chunk + 1)
                    break;
                int flag = 0;
                if(pos.sum1 < sum1_res->size() && sum1_res->at(pos.sum1).first == now){
                    if(sum1_res->at(pos.sum1).first++ == sum1_res->at(pos.sum1).second)
                        pos.sum1++;
                    flag |= SUM1_FLAG;
                }
                if(pos.sq_sum1 < sq_sum1_res->size() && sq_sum1_res->at(pos.sq_sum1).first == now){
                    if(sq_sum1_res->at(pos.sq_sum1).first++ == sq_sum1_res->at(pos.sq_sum1).second)
                        pos.sq_sum1++;
                    flag |= SQ_SUM1_FLAG;
                }

                auto tmp = data_->FetchDataCombine(col1, -1, flag, now * chunk_size, (now + 1) * chunk_size - 1);

                if (flag & SUM1_FLAG){
                    sum1_bulk.push_back(std::make_pair(now, tmp.sum1));
                }
                if (flag & SQ_SUM1_FLAG){
                    sq_sum1_bulk.push_back(std::make_pair(now, tmp.sq_sum1));
                }
            };
            VariableInsert(sum1_it->second.st, sum1_bulk);
            VariableInsert(sq_sum1_it->second.st, sq_sum1_bulk);
        }

        void LoadChunkMultiColumn(SizeType col1, SizeType col2, SizeType left_chunk, SizeType right_chunk, SizeType chunk_size){
            auto mul_key = make_column_info(col1, col2, MUL_SUM);
            auto sum1_key = make_column_info(col1, -1, SUM);
            auto sq_sum1_key = make_column_info(col1, -1, SQUARE_SUM);
            auto sum2_key = make_column_info(col2, -1, SUM);
            auto sq_sum2_key = make_column_info(col2, -1, SQUARE_SUM);

            auto mul_it = FindSegmentTree(mul_key);
            auto sum1_it = FindSegmentTree(sum1_key);
            auto sq_sum1_it = FindSegmentTree(sq_sum1_key);
            auto sum2_it = FindSegmentTree(sum2_key);
            auto sq_sum2_it = FindSegmentTree(sq_sum2_key);

            auto mul_res = mul_it->second.st->Query(left_chunk, right_chunk).first;
            auto sum1_res = sum1_it->second.st->Query(left_chunk, right_chunk).first;
            auto sq_sum1_res = sq_sum1_it->second.st->Query(left_chunk, right_chunk).first;
            auto sum2_res = sum2_it->second.st->Query(left_chunk, right_chunk).first;
            auto sq_sum2_res = sq_sum2_it->second.st->Query(left_chunk, right_chunk).first;

            DataPayload<int> pos;
            pos.mul = 0;
            pos.sum1 = 0;
            pos.sq_sum1 = 0;
            pos.sum2 = 0;
            pos.sq_sum2 = 0;

            std::vector<std::pair<int64_t, T>> mul_bulk;
            std::vector<std::pair<int64_t, T>> sum1_bulk;
            std::vector<std::pair<int64_t, T>> sq_sum1_bulk;
            std::vector<std::pair<int64_t, T>> sum2_bulk;
            std::vector<std::pair<int64_t, T>> sq_sum2_bulk;

            while(true){
                int now = right_chunk + 1;
                if(pos.mul < mul_res->size() && mul_res->at(pos.mul).first < now)
                    now = mul_res->at(pos.mul).first;
                if(pos.sum1 < sum1_res->size() && sum1_res->at(pos.sum1).first < now)
                    now = sum1_res->at(pos.sum1).first;
                if(pos.sq_sum1 < sq_sum1_res->size() && sq_sum1_res->at(pos.sq_sum1).first < now)
                    now = sq_sum1_res->at(pos.sq_sum1).first;
                if(pos.sum2 < sum2_res->size() && sum2_res->at(pos.sum2).first < now)
                    now = sum2_res->at(pos.sum2).first;
                if(pos.sq_sum2 < sq_sum2_res->size() && sq_sum2_res->at(pos.sq_sum2).first < now)
                    now = sq_sum2_res->at(pos.sq_sum2).first;
                if(now == right_chunk + 1)
                    break;
                int flag = 0;
                if(pos.mul < mul_res->size() && mul_res->at(pos.mul).first == now){
                    if(mul_res->at(pos.mul).first++ == mul_res->at(pos.mul).second)
                        pos.mul++;
                    flag |= MUL_FLAG;
                }
                if(pos.sum1 < sum1_res->size() && sum1_res->at(pos.sum1).first == now){
                    if(sum1_res->at(pos.sum1).first++ == sum1_res->at(pos.sum1).second)
                        pos.sum1++;
                    flag |= SUM1_FLAG;
                }
                if(pos.sq_sum1 < sq_sum1_res->size() && sq_sum1_res->at(pos.sq_sum1).first == now){
                    if(sq_sum1_res->at(pos.sq_sum1).first++ == sq_sum1_res->at(pos.sq_sum1).second)
                        pos.sq_sum1++;
                    flag |= SQ_SUM1_FLAG;
                }
                if(pos.sum2 < sum2_res->size() && sum2_res->at(pos.sum2).first == now){
                    if(sum2_res->at(pos.sum2).first++ == sum2_res->at(pos.sum2).second)
                        pos.sum2++;
                    flag |= SUM2_FLAG;
                }
                if(pos.sq_sum2 < sq_sum2_res->size() && sq_sum2_res->at(pos.sq_sum2).first == now){
                    if(sq_sum2_res->at(pos.sq_sum2).first++ == sq_sum2_res->at(pos.sq_sum2).second)
                        pos.sq_sum2++;
                    flag |= SQ_SUM2_FLAG;
                }

                auto tmp = data_->FetchDataCombine(col1, col2, flag, now * chunk_size, (now + 1) * chunk_size - 1);

                if (flag & MUL_FLAG){
                    mul_bulk.push_back(std::make_pair(now, tmp.mul));
                }
                if (flag & SUM1_FLAG){
                    sum1_bulk.push_back(std::make_pair(now, tmp.sum1));
                }
                if (flag & SQ_SUM1_FLAG){
                    sq_sum1_bulk.push_back(std::make_pair(now, tmp.sq_sum1));
                }
                if (flag & SUM2_FLAG){
                    sum2_bulk.push_back(std::make_pair(now, tmp.sum2));
                }
                if (flag & SQ_SUM2_FLAG){
                    sq_sum2_bulk.push_back(std::make_pair(now, tmp.sq_sum2));
                }
            };
            VariableInsert(mul_it->second.st, mul_bulk);
            VariableInsert(sum1_it->second.st, sum1_bulk);
            VariableInsert(sq_sum1_it->second.st, sq_sum1_bulk);
            VariableInsert(sum2_it->second.st, sum2_bulk);
            VariableInsert(sq_sum2_it->second.st, sq_sum2_bulk);

        }

        void VariableInsert(SegmentTreePtr st, std::vector<std::pair<int64_t, T>>& values){
            using namespace std;
            SizeType origin_size = st->size();
            chunk_number_ += values.size();
            st->BulkInsert(values);
            SizeType col_chunk_number = data_->GetNumberOfRows() / GetChunkSize();
            if (origin_size < col_chunk_number && st->size() == col_chunk_number) {
                auto values = st->GetAllValues();
                st = dynamic_pointer_cast<SegmentTree<T>>(
                        make_shared<PrefixSumArray<T>>(values, st->get_n(),
                                                       st->get_supporter()));
            } else if (origin_size <= col_chunk_number * CONVERT_FACTOR &&
                       st->size() > col_chunk_number * CONVERT_FACTOR) {
                auto nptr = dynamic_pointer_cast<LinkSegmentTree<T>>(st);
                st = dynamic_pointer_cast<SegmentTree<T>>(
                        ConvertLinkToArray<T>(nptr));
            }
        }

        T Query(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType left, SizeType right) {
#ifdef DEBUG
            std::cout << "query " << aggregate_type << " " << col1 << " " << col2 << " " << left << " " << right << std::endl;
#endif
            assert(left < right);
            using namespace std;
            right--;

            auto key = make_column_info(col1, col2, aggregate_type);

            auto it = FindSegmentTree(key);
            SizeType chunk_size = it->second.chunk_size;
            SizeType col_chunk_number = data_->GetNumberOfRows() / chunk_size;
            T ans;
            bool flag = false;
            SizeType left_chunk = left / chunk_size, right_chunk = right / chunk_size;
            if (left_chunk == right_chunk && right - left + 1 != chunk_size)
                return data_->FetchData(col1, col2, aggregate_type, left, right);
            if (left % chunk_size) {
                left_chunk++;
                ans = data_->FetchData(col1, col2, aggregate_type, left, left_chunk * chunk_size - 1);
                flag = true;
            }
            if ((right + 1) % chunk_size != 0) {
                right_chunk--;
                if (flag)
                    ans = Merge(aggregate_type, ans,
                                data_->FetchData(col1, col2, aggregate_type, (right_chunk + 1) * chunk_size,
                                                 right));
                else
                    ans = data_->FetchData(col1, col2, aggregate_type, (right_chunk + 1) * chunk_size,
                                           right);
                flag = true;
            }
            if (left_chunk <= right_chunk) {
                auto result = it->second.st->Query(left_chunk, right_chunk);
                assert(result.first->empty());
                if (flag)
                    ans = Merge(aggregate_type, ans, result.second);
                else
                    ans = result.second;
            }
            return ans;
        }


#ifdef MEASURE_FETCH_TIME

        double
#else
        void
#endif
        BuildColumn(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType chunk_size) {
            using namespace std;
            auto key = make_column_info(col1, col2, aggregate_type);
            auto it = st_dict_.find(key);
            SizeType chunk_number = data_->GetNumberOfRows() / chunk_size;
            chunk_number_ += chunk_number;
            if (it == st_dict_.end() || it->second.chunk_size != chunk_size) {
                if (aggregate_type != MIN && aggregate_type != MAX) {
                    shared_ptr<vector<pair<int64_t, T>>> values = make_shared<vector<pair<int64_t, T>>>();

                    for (SizeType i = 0; i < chunk_number; i++)
                        values->push_back(make_pair(i, data_->FetchData(col1, col2, aggregate_type, i * chunk_size,
                                                                        (i + 1) * chunk_size - 1)));
                    st_dict_.insert(make_pair(key, SegmentTreeInfo(chunk_size, dynamic_pointer_cast<SegmentTree<T>>(
                            make_shared<PrefixSumArray<T>>(values, 0, chunk_number - 1,
                                                           GetSupport<T>(aggregate_type))))));
                } else {
                    st_dict_.insert(make_pair(key, SegmentTreeInfo(chunk_size, dynamic_pointer_cast<SegmentTree<T>>(
                            make_shared<ArraySegmentTree<T>>(0, chunk_number - 1,
                                                             GetSupport<T>(aggregate_type))))));
                    it = st_dict_.find(key);

                    vector<pair<int64_t, T>> values;
                    for (SizeType i = 0; i < chunk_number; i++)
                        values.push_back(make_pair(i, data_->FetchData(col1, col2, aggregate_type, i * chunk_size,
                                                                       (i + 1) * chunk_size - 1)));

                    it->second.st->BulkInsert(values);
                }
            }

#ifdef MEASURE_FETCH_TIME
            Timer timer;
            timer.start();
#endif
#ifdef MEASURE_FETCH_TIME
            timer.end();
#endif
#ifdef MEASURE_FETCH_TIME
            return timer.getDiff();
#endif
        }


        inline SizeType GetChunkSize() {
            return default_chunk_size_;
        }

        inline SizeType SetChunkSize(SizeType size) {
            default_chunk_size_ = size;
        }

        inline SizeType GetChunkNumber() {
            return chunk_number_;
        }

    private:
        static inline ColumnInfo make_column_info(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type) {
            using namespace std;
            if (MultiColumn(aggregate_type) && col1 > col2)
                std::swap(col1, col2);
            if (!MultiColumn(aggregate_type))
                col2 = -1;
            return make_pair(make_pair(col1, col2), aggregate_type);
        }

        typename ColSegMap::iterator FindSegmentTree(ColumnInfo key){
            using namespace std;
            auto it = st_dict_.find(key);
            AggregateTypeEnum aggregate_type = key.second;
            if (it == st_dict_.end()) {
                SizeType chunk_size = GetChunkSize();
                st_dict_.insert(make_pair(key, SegmentTreeInfo(chunk_size, dynamic_pointer_cast<SegmentTree<T>>(
                        make_shared<LinkSegmentTree<T>>(0, data_->GetNumberOfRows() / chunk_size - 1,
                                                        GetSupport<T>(aggregate_type))))));
                it = st_dict_.find(key);
            }
            return it;
        }

        SizeType chunk_number_ = 0;
        SizeType default_chunk_size_ = 50;
        std::map<ColumnInfo, SegmentTreeInfo> st_dict_;
        std::shared_ptr<Data<T>> data_;

};


#endif //QUERY_HANDLER_H
