//
// Created by Xinding Wei on 16/8/18.
//

#ifndef QUERY_HANDLER_H
#define QUERY_HANDLER_H

#include "segmenttree.h"
#include "aggregatetype.h"
#include <map>
#include <set>
#include "data.h"
#include "tools/timer.hh"
#include <thread>
#include <mutex>
#include <vector>
#include <memory>

//#define MEASURE_FETCH_TIME

#ifdef MEASURE_FETCH_TIME

#include "tools/timer.hh"

#endif

//#define OUT_OF_MEMORY

// For now, we only implement it in linked version
#ifdef OUT_OF_MEMORY
#define LINKED_IMPLEMENT
#endif
//#define LINKED_IMPLEMENT

typedef std::pair<std::pair<int64_t, int64_t>, int> ColumnInfo;

template<class T>
class QueryHandler {
    private:
        typedef std::shared_ptr<SegmentTree<T>> SegmentTreePtr;
        typedef std::shared_ptr<RangeKeyValueDataStructure<T>> RangeKeyValueDataStructurePtr;

        struct RangeKeyValueDataStructureInfo {
            RangeKeyValueDataStructureInfo(SizeType chunk_size, RangeKeyValueDataStructurePtr st) : chunk_size(chunk_size), st(st) {
            }

            RangeKeyValueDataStructureInfo(const RangeKeyValueDataStructureInfo &x) {
                this->chunk_size = x.chunk_size;
                this->st = x.st;
            }

            RangeKeyValueDataStructureInfo &operator=(const RangeKeyValueDataStructureInfo &x) {
                this->chunk_size = x.chunk_size;
                this->st = x.st;
            }

            SizeType chunk_size;
            RangeKeyValueDataStructurePtr st;
        };

    public:
        QueryHandler(std::shared_ptr<Data<T>> data) : data_(data) {

        }

        inline SizeType memory_overhead(){return memory_overhead_;}

        void BuildAllColumns(int number_thread);

        T
        QueryStatistic(SizeType col1, SizeType col2, StatisticTypeEnum statistic_type, SizeType left, SizeType right);

        T Query(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType left, SizeType right);

#ifdef MEASURE_FETCH_TIME
        double
#else
        void
#endif
        BuildColumn(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType chunk_size);

        void Update(SizeType col, SizeType pos, T value);

        void Delete(SizeType row);

        inline SizeType GetChunkSize() {
            return default_chunk_size_;
        }

        inline void SetChunkSize(SizeType size) {
            default_chunk_size_ = size;
        }

        inline SizeType GetChunkNumber() {
            return chunk_number_;
        }

#ifdef MEASURE_FETCH_TIME
        inline double GetConstructTime() {
            return construct_time_;
        }
#endif

#ifdef OUT_OF_MEMORY
        inline void SetMemoryLimit(SizeType limit) {
            memory_limit_ = limit;
            CheckMemory();
        }
        static inline SizeType GetChunkSizePageThreshold(){
            return PAGE_SIZE / sizeof(T);
        }
#endif

    private:
        static inline ColumnInfo make_column_info(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type) {
            using namespace std;
            return make_pair(make_pair(col1, col2), aggregate_type);
        }
#ifdef MEASURE_FETCH_TIME
        double construct_time_ = 0;
#endif

#ifdef OUT_OF_MEMORY
        int phase_ = 1;
        SizeType memory_limit_ = -1;
        int now_pos_ = 0;
        std::set<ColumnInfo> st_chunk_[2];

        void CheckMemory(){
            using namespace std;
//            cout << "memory: " << this->memory_overhead_ + (dynamic_pointer_cast< BufferPoolData<T> >(data_))->get_memory_overhead() << endl;
            if(memory_limit_ != -1
               && memory_limit_ < this->memory_overhead_ + (dynamic_pointer_cast< BufferPoolData<T> >(data_))->get_memory_overhead()){
                switch (phase_){
                    case 1:{
                        SizeType left_memory = memory_limit_ - dynamic_pointer_cast< BufferPoolData<T> >(data_)->get_memory_overhead();
                        cout << "Phase1 " << left_memory << "\n";
//                        cout << "Data: " << dynamic_pointer_cast< BufferPoolData<T> >(data_)->get_memory_overhead() << endl;
                        while(left_memory < this->memory_overhead_) {
                            cout << "ST takes: " << this->memory_overhead_ << endl;
                            while (!st_chunk_[now_pos_ ^ 1].empty() && left_memory < this->memory_overhead_) {
                                auto it = st_chunk_[now_pos_ ^ 1].begin();
                                ColumnInfo key = *it;
                                auto tmp = st_dict_.find(key);
                                auto it_st = tmp->second;
                                tmp->second.chunk_size <<= 1;
                                this->memory_overhead_ -= it_st.st->memory_overhead();
                                dynamic_pointer_cast<LinkSegmentTree<T> >(it_st.st)->RemoveLastLayer();
                                this->memory_overhead_ += it_st.st->memory_overhead();
                                st_chunk_[now_pos_ ^ 1].erase(it);
                                st_chunk_[now_pos_].insert(key);
                            }
                            if (st_chunk_[now_pos_ ^ 1].empty() && left_memory < this->memory_overhead_) {
                                if((default_chunk_size_ << 1) > GetChunkSizePageThreshold()){
                                    phase_ = 2;
                                    break;
                                }
                                now_pos_ ^= 1;
                                default_chunk_size_ <<= 1;
                            }
                        }
                        if(left_memory > this->memory_overhead_)
                            break;
                    }
                    case 2:{
                        SizeType left_memory = memory_limit_ - this->memory_overhead_;
                        cout << "Phase2 " << left_memory << "\n";
                        dynamic_pointer_cast<BufferPoolData<T> >(data_)->SetLimit(left_memory);
                        break;
                    }
                    case 3:{
                        assert(0 && "Not implement");
                        break;
                    }
                    default:
                        break;
                }
            }
        }
#endif
        SizeType chunk_number_ = 0;
        SizeType default_chunk_size_ = 50;
        SizeType memory_overhead_ = 0;
        std::map<ColumnInfo, RangeKeyValueDataStructureInfo> st_dict_;
        std::shared_ptr<Data<T>> data_;

};

template<class T>
void QueryHandler<T>::BuildAllColumns(int number_thread){
    using namespace std;
    int col = data_->GetNumberOfColumns();
    int64_t row = data_->GetNumberOfRows();
    int type = SUM, col1 = 0, col2 = 0;
    int chunk_size = GetChunkSize();
    mutex mt, dict_mt;
    vector<thread> thread_que;
    for (int i = 0; i < number_thread; i++)
        thread_que.emplace_back([&](int num){
            int type_now = SUM, col1_now = num, col2_now = -1;
            while(true){
                auto key = make_column_info(col1_now, col2_now, type_now);
                SizeType chunk_number = data_->GetNumberOfRows() / chunk_size;

                shared_ptr<vector<pair<int64_t, T> > > values = make_shared<vector<pair<int64_t, T>> >();

                for (SizeType i = 0; i < chunk_number; i++)
                    values->push_back(make_pair(i, data_->FetchData(col1_now, col2_now, type_now, i * chunk_size,
                                                                    (i + 1) * chunk_size - 1)));
#ifndef LINKED_IMPLEMENT
                auto tree_ptr = dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                               make_shared<PrefixSumArray<T> >(values, chunk_number,
                                                              GetSupport<T>(type_now)));
#else
                auto tree_ptr = dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                        make_shared<LinkSegmentTree<T> >(chunk_number, GetSupport<T>(type_now), *values));
#endif
                dict_mt.lock();
                st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, tree_ptr)));
                memory_overhead_ += tree_ptr->memory_overhead();
                dict_mt.unlock();
                switch (type_now){
                    case SUM:{
                        col1_now += number_thread;
                        if(col1_now >= col){
                            col1_now %= col;
                            type_now = SQUARE_SUM;
                        }
                        break;
                    }
                    case SQUARE_SUM:{
                        col1_now += number_thread;
                        if(col1_now >= col){
                            col2_now = col1_now % col;
                            col1_now = 0;
                            type_now = MUL_SUM;
                        }
                        break;
                    }
                    case MUL_SUM:{
                        col2_now += number_thread;
                        while(col2_now >= col){
                            col1_now++;
                            if(col1_now >= col)
                                return;
                            col2_now -= col - col1_now;
//                                   if (col1_now >= col)
                        }
                        break;
                    }
                }
            }
        }, i);
    for (int i = 0; i < number_thread; i++)
        thread_que[i].join();
    return ;
}

template<class T>
void QueryHandler<T>::Update(SizeType col, SizeType pos, T value) {
    using namespace std;
    StatisticTypeEnum aggregate_type = SUM;
    T origin_val = data_->FetchData(col, -1, pos, pos, aggregate_type);
    auto it = st_dict_.find(make_column_info(col, -1, aggregate_type));
    if (it != st_dict_.end()){
        if (data_->GetNumberOfRows() / it->second.chunk_size >= pos / it->second.chunk_size){
            auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
            auto result = st_it->Query(pos / it->second.chunk_size, pos / it->second.chunk_size);
            if(result.first->empty())
                st_it->Insert(pos, result.second - origin_val + value);
        }
    }
    aggregate_type = SQUARE_SUM;
    if (it != st_dict_.end()){
        if (data_->GetNumberOfRows() / it->second.chunk_size >= pos / it->second.chunk_size){
            auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
            auto result = st_it->Query(pos / it->second.chunk_size, pos / it->second.chunk_size);
            if(result.first->empty())
                st_it->Insert(pos, result.second - origin_val * origin_val + value * value);
        }
    }
    aggregate_type = MUL_SUM;
    for (SizeType col2 = 0; col2 < data_->GetNumberOfColumns(); col2++)
        if (col != col2){
            auto key = make_column_info(col, col2, aggregate_type);
            if (col > col2)
                swap(key.first.first, key.first.second);
            auto it = st_dict_.find(key);
            if (it != st_dict_.end()){
                if (data_->GetNumberOfRows() / it->second.chunk_size >= pos / it->second.chunk_size){
                    auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
                    auto result = st_it->Query(pos / it->second.chunk_size, pos / it->second.chunk_size);
                    if(result.first->empty()) {
                        T origin_val2 = data_->FetchData(col, -1, pos, pos, aggregate_type);
                        st_it->Insert(pos, result.second - origin_val * origin_val2 + value * origin_val2);
                    }
                }
            }
        }
}

//Dummy implement
template<class T>
void QueryHandler<T>::Delete(SizeType row) {
    using namespace std;
    StatisticTypeEnum aggregate_type = SUM;

    T value = 0;
    for (int col = 0; col < data_->GetNumberOfColumns(); col++) {
        T origin_val = data_->FetchData(col, -1, row, row, aggregate_type);
        auto it = st_dict_.find(make_column_info(col, -1, aggregate_type));
        if (it != st_dict_.end()) {
            if (data_->GetNumberOfRows() / it->second.chunk_size >= row / it->second.chunk_size) {
                auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
                auto result = st_it->Query(row / it->second.chunk_size, row / it->second.chunk_size);
                if (result.first->empty())
                    st_it->Insert(row, result.second - origin_val + value);
            }
        }
        aggregate_type = SQUARE_SUM;
        if (it != st_dict_.end()) {
            if (data_->GetNumberOfRows() / it->second.chunk_size >= row / it->second.chunk_size) {
                auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
                auto result = st_it->Query(row / it->second.chunk_size, row / it->second.chunk_size);
                if (result.first->empty())
                    st_it->Insert(row, result.second - origin_val * origin_val + value * value);
            }
        }
        aggregate_type = MUL_SUM;
        for (SizeType col2 = col + 1; col2 < data_->GetNumberOfColumns(); col2++)
            if (col != col2) {
                auto key = make_column_info(col, col2, aggregate_type);
                if (col > col2)
                    swap(key.first.first, key.first.second);
                auto it = st_dict_.find(key);
                if (it != st_dict_.end()) {
                    if (data_->GetNumberOfRows() / it->second.chunk_size >= row / it->second.chunk_size) {
                        auto st_it = dynamic_pointer_cast<SegmentTree<T>>(it->second.st);
                        auto result = st_it->Query(row / it->second.chunk_size, row / it->second.chunk_size);
                        if (result.first->empty()) {
                            T origin_val2 = data_->FetchData(col, -1, row, row, aggregate_type);
                            st_it->Insert(row, result.second - origin_val * origin_val2 + value * origin_val2);
                        }
                    }
                }
            }
    }
}

template<class T>
T
QueryHandler<T>::QueryStatistic(SizeType col1, SizeType col2, StatisticTypeEnum statistic_type, SizeType left, SizeType right) {
    SizeType n = right - left;
    switch (statistic_type) {
        case MEAN:
            return Query(col1, col2, SUM, left, right) / n;
        case VARIANCE: {
            T square = Query(col1, col2, SQUARE_SUM, left, right);
            T avg = Query(col1, col2, SUM, left, right) / n;
            return square / n - avg * avg;
        }
        case STANDARD_DEVIATION: {
            T square = Query(col1, col2, SQUARE_SUM, left, right);
            T avg = Query(col1, col2, SUM, left, right) / n;
            return sqrt(square / n - avg * avg);
        }
        case CORRELATION: {
            T cor = Query(col1, col2, MUL_SUM, left, right);
            T sum1 = Query(col1, col2, SUM, left, right);
            T sum2 = Query(col2, col1, SUM, left, right);
            T square1 = Query(col1, col2, SQUARE_SUM, left, right);
            T square2 = Query(col2, col1, SQUARE_SUM, left, right);
            T std1 = sqrt(square1 - sum1 * sum1 / n);
            T std2 = sqrt(square2 - sum2 * sum2 / n);
            return (cor - sum1 * sum2 / n) / (std1 * std2);
        }
        case COVARIANCE: {
            T cor = Query(col1, col2, MUL_SUM, left, right);
            T sum1 = Query(col1, col2, SUM, left, right);
            T sum2 = Query(col2, col1, SUM, left, right);
            return (cor / n) - ((sum1 / n) * (sum2 / n));
        }
    }
    return 0;
}

template<class T>
T QueryHandler<T>::Query(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType left, SizeType right) {
    assert(left < right);
    using namespace std;
    right--;
    if (MultiColumn(aggregate_type) && col1 > col2)
        std::swap(col1, col2);
    if (!MultiColumn(aggregate_type))
        col2 = -1;
    auto key = make_column_info(col1, col2, aggregate_type);
    auto it = st_dict_.find(key);
    SizeType chunk_size;
    if (it == st_dict_.end()) {
        chunk_size = GetChunkSize();
        st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                make_shared<LinkSegmentTree<T> >(data_->GetNumberOfRows() / chunk_size,
                                                GetSupport<T>(aggregate_type))))));
        it = st_dict_.find(key);
        memory_overhead_ += it->second.st->memory_overhead();
#ifdef OUT_OF_MEMORY
        st_chunk_[now_pos_].insert(key);
#endif
    } else
        chunk_size = it->second.chunk_size;
    SizeType col_chunk_number = data_->GetNumberOfRows() / chunk_size;
    T ans;
    bool flag = false;
    SizeType left_chunk = left / chunk_size, right_chunk = right / chunk_size;
    if (left_chunk == right_chunk && right - left + 1 != chunk_size)
        return data_->FetchData(col1, col2, aggregate_type, left, right);
    if (left % chunk_size) {
        left_chunk++;
        ans = data_->FetchData(col1, col2, aggregate_type, left, left_chunk * chunk_size - 1);
        flag = true;
    }
    if ((right + 1) % chunk_size != 0) {
        right_chunk--;
        if (flag)
            ans = Merge(aggregate_type, ans,
                        data_->FetchData(col1, col2, aggregate_type, (right_chunk + 1) * chunk_size,
                                         right));
        else
            ans = data_->FetchData(col1, col2, aggregate_type, (right_chunk + 1) * chunk_size,
                                   right);
        flag = true;
    }
    if (left_chunk <= right_chunk) {
        // Deal with update operations
        // Now we don't consider the situation
        int64_t chunk_number = data_->GetNumberOfRows() / chunk_size;
        while(it->second.st->get_n() < chunk_number){
            dynamic_pointer_cast<SegmentTree<T> >(it->second.st)->AddTopLayer();
        }

        auto result = it->second.st->Query(left_chunk, right_chunk);
        if (!result.first->empty()) {
//            cout << "query: " << left_chunk << ' ' << right_chunk << endl;
//            cout << result.first->size() << endl;
            memory_overhead_ -= it->second.st->memory_overhead();
            vector<pair<int64_t, T>> values;
            std::shared_ptr<std::vector<T>> tmp;
            for (SizeType i = 0; i < result.first->size(); i++) {
                chunk_number_ += result.first->at(i).second - result.first->at(i).first + 1;
//                cout << result.first->at(i).first << ' ' << result.first->at(i).second << endl;
                for (SizeType j = result.first->at(i).first; j <= result.first->at(i).second; j++)
                    values.push_back(make_pair(j, data_->FetchData(col1, col2, aggregate_type,
                                                                   j * chunk_size,
                                                                   (j + 1) * chunk_size - 1)));
            }
            SizeType origin_size = it->second.st->size();
            dynamic_pointer_cast<SegmentTree<T> >(it->second.st)->BulkInsert(values);
#ifndef LINKED_IMPLEMENT
            if (origin_size < col_chunk_number && it->second.st->size() == col_chunk_number) {
                        auto values = it->second.st->GetAllValues();
                        it->second.st = dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                                make_shared<PrefixSumArray<T> >(values, it->second.st->get_n(),
                                                                       it->second.st->get_supporter()));
            } else if (origin_size <= col_chunk_number * CONVERT_FACTOR &&
                       it->second.st->size() > col_chunk_number * CONVERT_FACTOR) {
                auto nptr = dynamic_pointer_cast<LinkSegmentTree<T> >(it->second.st);
                it->second.st = dynamic_pointer_cast<SegmentTree<T> >(ConvertLinkToArray<T>(nptr));
            }
#endif
            result = it->second.st->Query(left_chunk, right_chunk);
            memory_overhead_ += it->second.st->memory_overhead();

#ifdef OUT_OF_MEMORY
                CheckMemory();
#endif
//            cout << result.first->size() << endl;
//            for (SizeType i = 0; i < result.first->size(); i++) {
//                cout << result.first->at(i).first << ' ' << result.first->at(i).second << endl;
//            }
            assert(result.first->empty());
        }
        if (flag)
            ans = Merge(aggregate_type, ans, result.second);
        else
            ans = result.second;
    }
    return ans;
}

template<class T>
#ifdef MEASURE_FETCH_TIME
double
#else
void
#endif
QueryHandler<T>::BuildColumn(SizeType col1, SizeType col2, AggregateTypeEnum aggregate_type, SizeType chunk_size) {
    using namespace std;

#ifdef MEASURE_FETCH_TIME
    double fetch_time = 0;
#endif

    auto key = make_column_info(col1, col2, aggregate_type);
    auto it = st_dict_.find(key);
    SizeType chunk_number = data_->GetNumberOfRows() / chunk_size;

    if (it == st_dict_.end() || it->second.chunk_size != chunk_size) {
#ifndef LINKED_IMPLEMENT
        if (aggregate_type != MIN && aggregate_type != MAX) {
                    shared_ptr<vector<pair<int64_t, T>>> values = make_shared<vector<pair<int64_t, T>> >();

                    for (SizeType i = 0; i < chunk_number; i++)
                        values->push_back(make_pair(i, data_->FetchData(col1, col2, aggregate_type, i * chunk_size,
                                                                        (i + 1) * chunk_size - 1)));
                    auto tree_ptr = dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                            make_shared<PrefixSumArray<T> >(values, chunk_number,
                                                           GetSupport<T>(aggregate_type)));
                    st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, tree_ptr)));
                    memory_overhead_ += tree_ptr->memory_overhead();
                } else {
                    auto tree_ptr = dynamic_pointer_cast<SegmentTree<T> >(
                            make_shared<ArraySegmentTree<T> >(chunk_number,
                                                             GetSupport<T>(aggregate_type)));
                    st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, tree_ptr)));

                    memory_overhead_ += tree_ptr->memory_overhead();

                    it = st_dict_.find(key);

                    vector<pair<int64_t, T>> values;
                    for (SizeType i = 0; i < chunk_number; i++)
                        values.push_back(make_pair(i, data_->FetchData(col1, col2, aggregate_type, i * chunk_size,
                                                                       (i + 1) * chunk_size - 1)));

                    dynamic_pointer_cast<SegmentTree<T> >(it->second.st)->BulkInsert(values);
                }
#else
        shared_ptr<vector<pair<int64_t, T>>> values = make_shared<vector<pair<int64_t, T>> >();

#ifdef MEASURE_FETCH_TIME
        Timer timer;
        timer.start();
#endif
        for (SizeType i = 0; i < chunk_number; i++)
            values->push_back(make_pair(i, data_->FetchData(col1, col2, aggregate_type, i * chunk_size,
                                                            (i + 1) * chunk_size - 1)));
#ifdef MEASURE_FETCH_TIME
        timer.end();
        fetch_time = timer.getDiff();
#endif
        auto tree_ptr = dynamic_pointer_cast<RangeKeyValueDataStructure<T> >(
                make_shared<LinkSegmentTree<T> >(chunk_number,
                                                GetSupport<T>(aggregate_type), *values));
        st_dict_.insert(make_pair(key, RangeKeyValueDataStructureInfo(chunk_size, tree_ptr)));
        memory_overhead_ += tree_ptr->memory_overhead();
#endif
#ifdef MEASURE_FETCH_TIME
        return fetch_time;
#endif
    }
}

#endif //QUERY_HANDLER_H
