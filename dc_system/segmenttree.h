//
// Created by Xinding Wei on 16/7/18.
//

#ifndef SEGMENTTREE_H_
#define SEGMENTTREE_H_

#include <inttypes.h>
#include <assert.h>
#include <vector>
#include <iostream>
#include <queue>
#include <set>
#include "supporter.h"
#include "config.h"
#include <utility>

#define LINK_BASED_SEGMENT_TREE 0
#define ARRAY_BASED_SEGMENT_TREE 1
#define PREFIX_ARRAY 2

//#define OUT_OF_MEMORY

static inline int64_t upper_2_power(int64_t x) {
    int64_t ans = 1;
    while (ans < x)
        ans <<= 1;
    return ans;
}

typedef std::vector<std::pair<int64_t, int64_t>> PositionVector;
typedef std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> PositionVectorPtr;

template<class T>
class RangeKeyValueDataStructure;

template<class T>
class SegmentTree;

template<class T>
class ArraySegmentTree;

template<class T>
class LinkSegmentTree;

template<class T>
class RangeKeyValueDataStructure{
    public:
        RangeKeyValueDataStructure(int64_t n, std::shared_ptr<Supporter<T>> supporter) :
              n_(n), supporter_(supporter){

        }

        std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T> Query(int64_t lb, int64_t rb) {
#ifdef OUT_OF_MEMORY
//            this->Restore();
#endif
            return QueryImplement(lb, rb);
        }

        inline int64_t size() { return value_number_; }

        inline int64_t memory_overhead(){return memory_overhead_;}

        int64_t get_n() { return n_; }

        std::shared_ptr<std::vector<std::pair<int64_t, T>>> GetAllValues(){
#ifdef OUT_OF_MEMORY
//            this->Restore();
#endif
            return GetAllValuesImplement();
        };

        virtual std::shared_ptr<std::vector<std::pair<int64_t, T>>> GetAllValuesImplement() = 0;

        std::shared_ptr<Supporter<T>> get_supporter() { return supporter_; }

        virtual int DataStructureType() = 0;
#ifdef OUT_OF_MEMORY
        virtual void Dump(const std::string&){
            assert(0 && "Not implement");
        }

        // Do nothing if there is no dump file
        virtual void Restore(){
            assert(0 && "Not implement");
        }
#endif
    protected:
        int64_t n_;
        std::shared_ptr<Supporter<T>> supporter_;
        int64_t value_number_ = 0;
        int64_t memory_overhead_ = 0;
#ifdef OUT_OF_MEMORY
        std::string dump_file_ = "";
#endif
    private:

        virtual std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
        QueryImplement(int64_t lb, int64_t rb) = 0;
};

template<class T>
std::shared_ptr<ArraySegmentTree<T>> ConvertLinkToArray(std::shared_ptr<LinkSegmentTree<T>> lst);

template<class T>
class SegmentTree :public RangeKeyValueDataStructure<T>{
    public:
        SegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter) :
                RangeKeyValueDataStructure<T>(n, supporter) {
        }

        virtual ~SegmentTree() {}

        void Insert(int64_t key, T val) {
            std::vector<std::pair<int64_t, T>> tmp;
            tmp.push_back(std::make_pair(key, val));
            this->BulkInsert(tmp);
        }

        void BulkInsert(const std::vector<std::pair<int64_t, T> > &values) {
            BulkInsertImplement(values);
        }

        virtual void AddTopLayer() = 0;

        virtual void AddBottomLayer() = 0;

        virtual int DataStructureType() = 0;

    private:

        virtual void BulkInsertImplement(const std::vector<std::pair<int64_t, T> > &values) = 0;

        virtual std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
        QueryImplement(int64_t lb, int64_t rb) = 0;
};

// A space-efficient implement
template<class T>
class LinkSegmentTree : public SegmentTree<T> {
    private:
        struct Node {
            Node() {}

            ~Node() {}

            Node *left = NULL, *right = NULL;
            T val;
            bool valid = false;
        };

    public:
        LinkSegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter);

        LinkSegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter,
                        const std::vector<std::pair<int64_t, T> > &values);

        ~LinkSegmentTree();

        std::shared_ptr<std::vector<std::pair<int64_t, T>>> GetAllValuesImplement() override {
            auto ans = std::make_shared<std::vector<std::pair<int64_t, T>>>();
            GetAllValuesRecursion(ans, 0, this->n_ - 1, root_);
            return ans;
        }

        void AddTopLayer(){
            this->n_ <<= 1;
            Node * new_root = AllocateANode();
            new_root->left = root_;
            new_root->valid = false;
            new_root->val = root_->val;
            root_ = new_root;
        }

        void AddBottomLayer(){
            this->n_ <<= 1;
        }

        virtual int DataStructureType(){return LINK_BASED_SEGMENT_TREE;}

#ifdef OUT_OF_MEMORY
        virtual void Dump(const std::string& file_name){
            this->dump_file_ = file_name;

            //TODO: implement

            root_ = NULL;
        }
        virtual void Load(){

            //TODO: Load from file
        }

        void RemoveLastLayer(){
            RemoveLastLayerRecursion(root_, 0, this->n_ - 1);
            this->n_ >>= 1;
        }
#endif

    private:


        void GetAllValuesRecursion(std::shared_ptr<std::vector<std::pair<int64_t, T>>> target, int64_t l, int64_t r,
                                   Node *now) {
            if (l == r) {
                target->push_back(std::pair<int64_t, T>(l, now->val));
                return;
            }
            int64_t mid = (l + r) >> 1;
            if (now->left)
                GetAllValuesRecursion(target, l, mid, now->left);
            if (now->right)
                GetAllValuesRecursion(target, mid + 1, r, now->right);
        }

        std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
        QueryImplement(int64_t lb, int64_t rb) override;

        void BulkInsertImplement(const std::vector<std::pair<int64_t, T> > &values) override {
            BulkInsertRecursion(values, 0, 0, this->n_ - 1, root_);
        }

        T QueryRecursion(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> empty, int64_t lb, int64_t rb,
                         int64_t l, int64_t r, Node *now);

        int64_t BulkInsertRecursion(const std::vector<std::pair<int64_t, T> > &values, int64_t pos, int64_t l,
                                    int64_t r,
                                    Node *now);

        inline static void
        PushRange(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> range, int64_t l, int64_t r) {
            if (!range->empty() && range->back().second + 1 == l) {
                l = range->back().first;
                range->pop_back();
            }
            range->push_back(std::make_pair(l, r));
        }

#ifdef OUT_OF_MEMORY
        void ReleaseMemory(){
            if(root_)
                ReleaseMemoryRecursion(root_);
        }

        void ReleaseMemoryRecursion(Node* now){
            if(now->left)
                ReleaseMemoryRecursion(now->left);
            if(now->right)
                ReleaseMemoryRecursion(now->right);
            delete now;
        }

        void RemoveLastLayerRecursion(Node* now, int64_t l, int64_t r){
            if(l == r){
                this->memory_overhead_ -= sizeof(Node);
                delete now;
                count_--;
                return ;
            }
            int64_t  mid = (l + r) >> 1;
            if(now->left){
                RemoveLastLayerRecursion(now->left, l, mid);
                if(l == mid)
                    now->left = NULL;
            }
            if(now->right){
                RemoveLastLayerRecursion(now->right, mid + 1, r);
                if(mid + 1 == r)
                    now->right = NULL;
            }
        }
#endif

        inline Node *AllocateANode() {
            this->memory_overhead_ += sizeof(Node);
            count_++;
#ifdef OUT_OF_MEMORY
            return new Node;
#else
            if (pos_ == size_) {
                size_ <<= 1;
                this->memory_overhead_ += size_ * sizeof(Node);
                nodes_.push_back(new Node[size_]);
                pos_ = 0;
            }
            return nodes_.back() + (pos_++);
#endif
        }

        Node *root_ = NULL;
        int64_t count_ = 0;
#ifndef OUT_OF_MEMORY
        //root_ == NULL means ST is dumped.
        std::vector<Node *> nodes_;
        int64_t pos_ = 16, size_ = 16;
#endif
};


template<class T>
LinkSegmentTree<T>::LinkSegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter) : SegmentTree<T>(
        upper_2_power(n),
        supporter) {
    this->memory_overhead_ = sizeof(LinkSegmentTree<T>);
    root_ = AllocateANode();
}

template<class T>
LinkSegmentTree<T>::LinkSegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter,
                                    const std::vector<std::pair<int64_t, T> > &values)
        : SegmentTree<T>(upper_2_power(n), supporter) {
    this->memory_overhead_ = sizeof(LinkSegmentTree<T>);
    root_ = AllocateANode();
    this->BulkInsert(values);
}

template<class T>
LinkSegmentTree<T>::~LinkSegmentTree() {
#ifdef OUT_OF_MEMORY
    //TODO: Remove dump file
    ReleaseMemory();
#else
    for (int i = 0; i < nodes_.size(); i++)
        delete[](nodes_[i]);
#endif
}

template<class T>
std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
LinkSegmentTree<T>::QueryImplement(int64_t lb, int64_t rb) {
    assert(lb <= rb);
    std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> empty = std::make_shared<std::vector<std::pair<int64_t, int64_t>>>();
    T ans = QueryRecursion(empty, lb, rb, 0, this->n_ - 1, root_);
    return make_pair(empty, ans);
}

template<class T>
T LinkSegmentTree<T>::QueryRecursion(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> empty, int64_t lb,
                                     int64_t rb, int64_t l, int64_t r, Node *now) {
    int64_t mid = (l + r) >> 1;
    if (lb == l && rb == r) {
        if (!now->valid) {
            if (!now->left && !now->right)
                empty->push_back(std::make_pair(l, r));
            else {
                if (now->left) {
                    if (!now->left->valid)
                        QueryRecursion(empty, lb, std::min(rb, mid), l, mid, now->left);
                } else
                    PushRange(empty, lb, std::min(rb, mid));
                if (now->right) {
                    if (!now->right->valid)
                        QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, now->right);
                } else
                    PushRange(empty, std::max(lb, mid + 1), rb);
            }
        }
        return now->val;
    }
    bool flag = false;
    T ans;
    if (lb <= mid) {
        if (now->left) {
            ans = QueryRecursion(empty, lb, std::min(rb, mid), l, mid, now->left);
            flag = true;
        } else
            PushRange(empty, lb, std::min(rb, mid));
    }
    if (rb > mid) {
        if (now->right)
            if (flag)
                ans = this->supporter_->Merge(ans,
                                              QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, now->right));
            else
                ans = QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, now->right);
        else
            PushRange(empty, std::max(lb, mid + 1), rb);
    }
    return ans;
}

template<class T>
int64_t
LinkSegmentTree<T>::BulkInsertRecursion(const std::vector<std::pair<int64_t, T> > &values, int64_t pos, int64_t l,
                                        int64_t r, Node *now) {
    while (pos < values.size() && values[pos].first < l)
        pos++;
    if (pos >= values.size())
        return pos;
    if (l == r) {
        now->val = values[pos].second;
        if (!now->valid) {
            now->valid = true;
            this->value_number_++;
        }
        return pos + 1;
    }
    int64_t mid = (l + r) >> 1;
    if (values[pos].first <= mid) {
        if (!now->left)
            now->left = AllocateANode();
        pos = BulkInsertRecursion(values, pos, l, mid, now->left);
    }
    if (pos < values.size() && values[pos].first <= r) {
        if (!now->right)
            now->right = AllocateANode();
        pos = BulkInsertRecursion(values, pos, mid + 1, r, now->right);
    }
    if (now->left && now->left->valid && now->right && now->right->valid) {
        now->val = this->supporter_->Merge(now->left->val, now->right->val);
        now->valid = true;
    }
    return pos;
}

template<class T>
class ArraySegmentTree : public SegmentTree<T> {
    private:
        struct Node {
            Node() {}

            ~Node() {}

            T val;
            bool valid = false;
        };

    public:
        ArraySegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter);

        ArraySegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter,
                         const std::vector<std::pair<int64_t, T> > &values);

        ~ArraySegmentTree();

        std::shared_ptr<std::vector<std::pair<int64_t, T>>> GetAllValuesImplement() override {
            auto ans = std::make_shared<std::vector<std::pair<int64_t, T>>>();
            for (int64_t i = 0; i < this->n_; i++) {
                if (nodes_[i + bias - 1].valid) {
                    ans->push_back(std::pair<int64_t, T>(i, nodes_[i + bias - 1].val));
                }
            }
            return ans;
        }

        void AddTopLayer(){
            this->n_ <<= 1;
            this->memory_overhead_ = sizeof(ArraySegmentTree<T>) + sizeof(Node) * ((bias << 2) - 1);
            Node *nodes= new Node[(bias << 2) - 1];
            for (int64_t i = 0, offset = 0; i <= (bias << 1) - 1; i++) {
                if(i == (2ll << offset) - 1)
                    offset++;
                nodes[i + (1 << offset)].val = nodes_[i].val;
                nodes[i + (1 << offset)].valid = nodes_[i].valid;
            }
            // TODO: compute invalid value
            delete []nodes_;
            nodes_ = nodes;
            bias <<= 1;
        }

        void AddBottomLayer(){
            this->n_ <<= 1;
            this->memory_overhead_ = sizeof(ArraySegmentTree<T>) + sizeof(Node) * ((bias << 2) - 1);
            Node *nodes= new Node[(bias << 2) - 1];
            for (int64_t i = 0; i <= (bias << 1) - 1; i++)
                nodes[i] = nodes_[i];
            // TODO: compute invalid value
            delete []nodes_;
            nodes_ = nodes;
            bias <<= 1;
        }

        virtual int DataStructureType(){return ARRAY_BASED_SEGMENT_TREE;}

    private:

        void BulkInsertImplement(const std::vector<std::pair<int64_t, T> > &values) override;

        std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
        QueryImplement(int64_t lb, int64_t rb) override;

        T QueryRecursion(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, int64_t lb, int64_t rb, int64_t l,
                         int64_t r, int64_t now);

        inline static void
        PushRange(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> range, int64_t l, int64_t r) {
            if (!range->empty() && range->back().second + 1 == l) {
                l = range->back().first;
                range->pop_back();
            }
            range->push_back(std::make_pair(l, r));
        }

        int64_t bias;
        Node *nodes_;
};


template<class T>
ArraySegmentTree<T>::ArraySegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter,
                                      const std::vector<std::pair<int64_t, T> > &values) : SegmentTree<T>(n,
                                                                                                          supporter),
                                                                                           bias(upper_2_power(n)) {
//    assert(min <= max);
    this->memory_overhead_ = sizeof(ArraySegmentTree<T>) + sizeof(Node) * ((bias << 1) - 1);
    nodes_ = new Node[(bias << 1) - 1];
    BulkInsert(values);
}

template<class T>
ArraySegmentTree<T>::ArraySegmentTree(int64_t n, std::shared_ptr<Supporter<T>> supporter)
        : SegmentTree<T>(n,
                         supporter),
          bias(
                  upper_2_power(n)) {
    this->memory_overhead_ = sizeof(ArraySegmentTree<T>) + sizeof(Node) * ((bias << 1) - 1);
    nodes_ = new Node[(bias << 1) - 1];
}

template<class T>
ArraySegmentTree<T>::~ArraySegmentTree() {
    delete[]nodes_;
}

template<class T>
void ArraySegmentTree<T>::BulkInsertImplement(const std::vector<std::pair<int64_t, T> > &values) {
    if (values.empty())
        return;
    std::shared_ptr<std::vector<int64_t>> now = std::make_shared<std::vector<int64_t>>(), last = std::make_shared<std::vector<int64_t>>();
    last->resize(values.size());
    for (int64_t i = 0; i < values.size(); i++) {
        last->at(i) = values[i].first + bias - 1;
        nodes_[last->at(i)].val = values[i].second;
        if (!nodes_[last->at(i)].valid) {
            this->value_number_++;
            nodes_[last->at(i)].valid = true;
        }
    }
    while (!(last->size() == 1 && last->front() == 0)) {
        for (int64_t i = 0; i < last->size(); i++) {
            int64_t key = (last->at(i) - 1) >> 1;
            if (now->empty() || now->back() != key) {
                now->push_back(key);
                if (nodes_[(key << 1) + 1].valid && nodes_[(key << 1) + 2].valid) {
                    nodes_[key].val = this->supporter_->Merge(nodes_[(key << 1) + 1].val,
                                                              nodes_[(key << 1) + 2].val);
                    nodes_[key].valid = true;
                }
            }
        }
        last->clear();
        swap(now, last);
    }
}

template<class T>
std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
ArraySegmentTree<T>::QueryImplement(int64_t lb, int64_t rb) {
    assert(lb <= rb);
    assert(rb <= this->n_);
    std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> empty = std::make_shared<std::vector<std::pair<int64_t, int64_t>>>();
    T ans = QueryRecursion(empty, lb, rb, 0, bias - 1, 0);
    return make_pair(empty, ans);
}

template<class T>
T ArraySegmentTree<T>::QueryRecursion(std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>> empty, int64_t lb,
                                      int64_t rb, int64_t l, int64_t r, int64_t now) {
    int64_t mid = (l + r) >> 1;
    if (lb == l && rb == r) {
        if (!nodes_[now].valid) {
            if (l != r) {
                if (!nodes_[(now << 1) + 1].valid)
                    QueryRecursion(empty, lb, std::min(rb, mid), l, mid, (now << 1) + 1);
                if (!nodes_[(now << 1) + 2].valid)
                    QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, (now << 1) + 2);
            } else
                PushRange(empty, l, r);
        }
        return nodes_[now].val;
    }
    bool flag = false;
    T ans;
    if (lb <= mid) {
        ans = QueryRecursion(empty, lb, std::min(rb, mid), l, mid, (now << 1) + 1);
        flag = true;
    }
    if (rb > mid) {
        if (flag)
            ans = this->supporter_->Merge(ans,
                                          QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, (now << 1) + 2));
        else
            ans = QueryRecursion(empty, std::max(lb, mid + 1), rb, mid + 1, r, (now << 1) + 2);
    }
    return ans;
}

template<class T>
std::shared_ptr<ArraySegmentTree<T>> ConvertLinkToArray(std::shared_ptr<LinkSegmentTree<T>> lst) {
    using namespace std;
    auto values = lst->GetAllValues();
    auto ast = make_shared<ArraySegmentTree<T>>(lst->get_n(), lst->get_supporter());
    ast->BulkInsert(*values);
    return ast;
}

template<class T>
class PrefixSumArray : public RangeKeyValueDataStructure<T> {

    public:
        PrefixSumArray(std::shared_ptr<std::vector<std::pair<int64_t, T>>> data, int64_t n,
                       std::shared_ptr<Supporter<T>> supporter) : RangeKeyValueDataStructure<T>(n,
                                                                              supporter) {
            data_.resize(n);
            this->memory_overhead_ = sizeof(PrefixSumArray<T>);
            this->memory_overhead_ += sizeof(T) * n;
            assert(data->at(0).first == 0);
            data_[0] = data->at(0).second;
            for (SizeType i = 1; i < data->size(); i++) {
                assert(i == data->at(i).first);
                data_[i] = data_[i - 1] + data->at(i).second;
            }
            this->value_number_ = n;
        }

        std::pair<std::shared_ptr<std::vector<std::pair<int64_t, int64_t>>>, T>
        QueryImplement(int64_t lb, int64_t rb) override {
            using namespace std;
            shared_ptr<vector<pair<int64_t, int64_t>>> empty = make_shared<vector<pair<int64_t, int64_t>>>();
            assert(lb <= rb && rb < data_.size());
            return pair<shared_ptr<vector<pair<int64_t, int64_t>>>, T>(empty, data_[rb] -
                                                                                  (lb > 0 ? data_[lb - 1] : 0));
        }

        std::shared_ptr<std::vector<std::pair<int64_t, T>>> GetAllValuesImplement() override{
            auto ans = std::make_shared<std::vector<std::pair<int64_t, T>>>();
            ans->resize(data_.size());
            for (int i = 0; i < this->n_; i++){
                if (i == 0){
                    ans->at(i) = std::pair<int64_t, T>(i, data_[i]);
                }
                else{
                    ans->at(i) = std::pair<int64_t, T>(i, data_[i] - data_[i - 1]);
                }
            }
            return ans;
        }

        virtual int DataStructureType(){return PREFIX_ARRAY;}

    private:
        std::vector<T> data_;
};

#endif //DC_DS_SEGMENTTREE_H
