#include <iostream>
#include "segmenttree.h"
#include <random>
#include "queryhandler.h"
#include "queryhandler_mod1.h"
#include "tools/timer.hh"
#include "tools/perf.hh"
#include <fstream>
using namespace std;

struct Query{
    StatisticTypeEnum statistic_type;
    int col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
        return *this;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};


int main(int argc, char** argv) {
    int64_t column_size = atoi(argv[1]);
    int64_t row_size = atoi(argv[2]);
    int chunk_size = atoi(argv[3]);
    int thread_number = atoi(argv[4]);
//    int64_t query_number = 10000;
//    int64_t ub = row_size / 10, lb = row_size / 20;
//
//    vector<Query> q_list;
//    Query tmp;
//
//    srand(174);
//    for (int i = 0; i < query_number; i++) {
//        tmp.col1 = rand() % column_size;
//        tmp.col2 = rand() % column_size;
//        if(tmp.col1 > tmp.col2)swap(tmp.col1, tmp.col2);
//        int qlen = rand() % (ub - lb + 1) + lb;
//        tmp.statistic_type = (rand() % 5) + 1;
//        tmp.left = rand() % (row_size - qlen + 1);
//        tmp.right = tmp.left + qlen;
//        q_list.push_back(tmp);
//    }

    QueryHandler<double> qh(
            dynamic_pointer_cast<Data<double>>(GenerateDummyMatrixData<double>(column_size, row_size)));
    qh.SetChunkSize(chunk_size);
    Perf perf("cpu-cycles,cache-misses,cache-references",false);
    Timer timer;
    perf.Start();
    timer.start();
    qh.BuildAllColumns(thread_number);
    timer.end();
    perf.End();
    cout << perf.GetResults()[1] << "\n";
    timer.printDiff();cout << "\n";
    return 0;
}