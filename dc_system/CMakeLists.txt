cmake_minimum_required(VERSION 3.0)
project(mst)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -std=c++11 -g -lpqxx -lpthread -O3")

set(SOURCE_FILES supporter.cc queryhandler.cc queryhandler.h aggregatetype.h)
#set(SOURCE_FILES ${SOURCE_FILES} queryhandler_mod1.h)

add_executable(mst_test test.cpp ${SOURCE_FILES})
#add_executable(mst_mod1_test mod1_test.cpp ${SOURCE_FILES})
add_executable(exp_query ${SOURCE_FILES} exp/exp_query.cc)

add_executable(exp_load ${SOURCE_FILES} exp/exp_load.cc)
target_compile_definitions(exp_load PUBLIC -DMEASURE_FETCH_TIME)

add_executable(query_process ${SOURCE_FILES} queryprocess.cc)
add_executable(data_generator data_generator.cc)
add_executable(exp_query_process exp/exp_query_process.cc)
#add_executable(exp_query_mod1_process exp/exp_query_mod1_process.cc)
add_executable(test_space testspace.cpp)
add_executable(multi_thread_build multi_thread_build.cpp)
add_executable(query_database_process query_database_process.cc)

add_executable(exp_query_process_linked exp/exp_query_process.cc)
add_executable(exp_update exp/exp_update.cc)
target_compile_definitions(exp_query_process_linked PUBLIC -DLINKED_IMPLEMENT)
target_compile_definitions(exp_update PUBLIC -DLINKED_IMPLEMENT)


add_executable(exp_query_process_out_of_memory exp/exp_query_process_out_of_memory.cc)
target_compile_definitions(exp_query_process_out_of_memory PUBLIC -DOUT_OF_MEMORY)

add_executable(exp_query_process_memory_pre_built exp/exp_query_process_memory_pre_built.cc)
target_compile_definitions(exp_query_process_memory_pre_built PUBLIC -DOUT_OF_MEMORY)

add_executable(exp_query_process_updates exp/exp_query_process_updates.cc)
target_compile_definitions(exp_query_process_updates PUBLIC -DLINKED_IMPLEMENT)