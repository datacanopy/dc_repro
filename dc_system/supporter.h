//
// Created by Xinding Wei on 16/8/16.
//

#ifndef MST_SUPPORTER_H
#define MST_SUPPORTER_H

#include <climits>
#include <cfloat>
#include <algorithm>
#include <vector>
#include <memory>
#include "aggregatetype.h"

template<class T>
class Supporter {
    public:
        Supporter() {}

        virtual ~Supporter() {}

        virtual inline T Merge(T a, T b) const = 0;

    protected:
};

template<class T>
class SumSupporter : public Supporter<T> {
    public:
        SumSupporter() {}

        inline T Merge(T a, T b) const override { return a + b; }

    private:
};

template<class T>
class MaxSupporter : public Supporter<T> {
    public:
        MaxSupporter() {}

        inline T Merge(T a, T b) const override { return std::max(a, b); }

    private:
};

template<class T>
class MinSupporter : public Supporter<T> {
    public:
        MinSupporter() {}

        inline T Merge(T a, T b) const override { return std::min(a, b); }

    private:
};

template<class T>
inline std::shared_ptr<Supporter<T>> GetSupport(AggregateTypeEnum aggregate_type) {
    using namespace std;
    static shared_ptr<Supporter<T>> sum_supporter = make_shared<SumSupporter<T>>(),
            min_supporter = make_shared<MinSupporter<T>>(), max_supporter = make_shared<MaxSupporter<T>>();
    switch (aggregate_type) {
        case SUM:
            return sum_supporter;
        case SQUARE_SUM:
            return sum_supporter;
        case MIN:
            return min_supporter;
        case MAX:
            return max_supporter;
        case MUL_SUM:
            return sum_supporter;
        case COUNT:
            return sum_supporter;
    }
    return nullptr;
}

template<class T>
inline T Merge(AggregateTypeEnum aggregate_type, T val1, T val2) {
    switch (aggregate_type) {
        case SUM:
            return val1 + val2;
        case SQUARE_SUM:
            return val1 + val2;
        case MIN:
            return std::min(val1, val2);
        case MAX:
            return std::max(val1, val2);
        case MUL_SUM:
            return val1 + val2;
        case COUNT:
            return val1 + val2;
    }
    return 0;
}


#endif //MST_SUPPORTER_H
