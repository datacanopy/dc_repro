// Created by Xinding Wei on 16/9/1.
//
#include "../queryhandler.h"
#include "../tools/timer.hh"
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <cstdio>
#include "../tools/memory_usage.h"
using namespace std;


struct Query{
    StatisticTypeEnum statistic_type;
    int col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};


int main(int argc, char *argv[]){
    srand(123);
    int chunk_size, row_number, col_number;
    bool build = false;
    string query_filename;
    if (argc >= 5){
        col_number = atoi(argv[1]);
        row_number = atoi(argv[2]);
        chunk_size = atoi(argv[3]);
        query_filename = argv[4];
//        if(argc > 5)
//            build = atoi(argv[5]);

        cout << "Col: " << col_number << endl;
        cout << "Query File is " << query_filename << endl;
        cout << "Chunk Size is " << chunk_size << endl;

    }else{
        cout<<"Usage ./test <col number> <row number> <chunk size> <query filename>"<<endl;
        return 1;
    }

    shared_ptr<Data<double>> data = GenerateDummyMatrixData<double>(col_number, row_number);

    FILE *fin_query;

    fin_query = fopen(query_filename.c_str() , "r");

    vector<Query> queries;
    Query tmp;
    while(!feof(fin_query)){
        fscanf(fin_query, "%d %d %d %d %d\n", &tmp.statistic_type, &tmp.col1, &tmp.col2, &tmp.left, &tmp.right);
        queries.push_back(tmp);
    }

    fclose(fin_query);

    cout << "Number of Queries: " << queries.size() << endl;

    vector<double> dummy;
    dummy.resize(queries.size());
    Timer timer;

    QueryHandler<double> qh(data);
    qh.SetChunkSize(chunk_size);

//    if (build) {
//        qh.BuildAllColumns(1);
//    }

    double update_time = 0, query_time = 0, delete_time = 0;
    for (int i = 0; i < queries.size(); i++){
//        if(queries[i].statistic_type == -1){
//            timer.end();
//            timer.printDiff();
//            cout << " " << qh.GetChunkNumber() << " " << value_involved << endl;
//            timer.start();
//        }
//        else{
        timer.start();
        if(queries[i].statistic_type == -2)
            qh.Delete(rand() % row_number);
        else if(queries[i].statistic_type == -1)
            qh.Update(rand() % col_number, rand() % row_number, rand());
        else
            dummy[i] = qh.QueryStatistic(queries[i].col1, queries[i].col2,
                                         queries[i].statistic_type,
                                         queries[i].left, queries[i].right);
        timer.end();
        if(queries[i].statistic_type == -2)
            delete_time += timer.getDiff();
        else if(queries[i].statistic_type == -1)
            update_time += timer.getDiff();
        else
            query_time += timer.getDiff();
    }
    cout << update_time << endl << query_time << endl << delete_time << endl;
    return 0;
}