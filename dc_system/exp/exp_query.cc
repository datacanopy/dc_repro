//
// Created by Xinding Wei on 16/9/10.
//
#include "../queryhandler.h"
#include "../tools/perf.hh"
#include "../tools/timer.hh"
#include "../tools/csv.hh"
#include <string>
#include <iostream>
#include <memory>
using namespace std;

struct Query{
    StatisticTypeEnum statistic_type;
    SizeType col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};

class QueryGenerator{
    public:
        QueryGenerator(SizeType column, SizeType size, SizeType chunk_size, SizeType length)
                : column(column), size(size), chunk_size(chunk_size), length(length){
        }
        Query GenerateQuery(){
            Query ans;
            ans.statistic_type = (rand() % 4) + 1;
            ans.col1 = rand() % column;
            ans.col2 = -1;
            if(ans.statistic_type == MUL_SUM){
                do{
                    ans.col2 = rand() % column;
                }while(ans.col1 == ans.col2);
            }
            ans.left = rand() % (size - GetQuerySize());
            ans.right = ans.left + length;
            return ans;
        }
        SizeType GetQuerySize(){
            return length;
        }
    private:
        SizeType column;
        SizeType size;
        SizeType chunk_size;
        SizeType length;
};



void exp1(){
    long int column = 100, size = pow(10, 3), chunk_size = pow(10, 4);
    long int n = 10000000, length = pow(10, 5);

    auto data = GenerateDummyMatrixData<int64_t>(column, size);
    string filename = "../result/exp_query_no1";
    Csv* writer = new Csv(filename);
    writer->write("Chunk size, Column, Size, Time, Cache misses, Cpu cycles\n");

    vector<Query> queries;
    queries.resize(n*2);
    set<Query, function<bool (Query, Query)>> qset([]( Query a, Query b) -> bool{
        if (a.statistic_type < b.statistic_type)
            return true;
        else if (a.statistic_type > b.statistic_type)
            return false;
        else if (a.col1 < b.col1)
            return true;
        else if (a.col1 > b.col1)
            return false;
        else if (a.col2 < b.col2)
            return true;
        else if (a.col2 > b.col2)
            return false;
        else if (a.left < b.left)
            return true;
        else if (a.left > b.left)
            return false;
        else if (a.right < b.right)
            return true;
        else if (a.right > b.right)
            return false;
        return false;
    });
    Query tmp;
    QueryGenerator qg(column, size, chunk_size, length);
    for (int i = 0; i < n * 3; i++){
        do{
            tmp = qg.GenerateQuery();
        }while(qset.find(tmp) != qset.end());
        queries.push_back(tmp);
        qset.insert(tmp);
    }

    Timer timer;
    Perf perf("cpu-cycles,cache-misses,cache-references",false);
    for (int i = 0; i < 5; i++){
        QueryHandler<int64_t> tqh(data);
        tqh.SetChunkSize(chunk_size);
        int now = 0, cnt = 0;
        int64_t sum = 0;
        perf.Start();
        timer.start();

//        for (int j = 0; j < 2 * n; j++){
//            int p = rand() % 8;
//            if(now > 0 && (p < i || now > 2 * n - (i * n / 8)) && cnt < i * n / 8){
//                int k = rand() % now;
//                sum = max(sum, tqh.QueryStatistic(queries[k].statistic_type,
//                                   queries[k].col1, queries[k].col2,
//                                   queries[k].left, queries[k].right));
//                cnt++;
//            }
//            else{
//                sum = max(sum, int64_t(rand() % (now+1)));
//                sum = max(sum, tqh.QueryStatistic(queries[now].statistic_type,
//                                   queries[now].col1, queries[now].col2,
//                                   queries[now].left, queries[now].right));
//                now++;
//            }
//        }

        for (int j = 0; j < 2 * n - (i * n / 8); j++){
            sum = max(sum, tqh.QueryStatistic(queries[j].statistic_type,
                                   queries[j].col1, queries[j].col2,
                                   queries[j].left, queries[j].right));
        }
        for (int j = 0; j < i * n / 8; j++){
            int k = rand() % n;
            sum = max(sum, tqh.QueryStatistic(queries[k].statistic_type,
                                   queries[k].col1, queries[k].col2,
                                   queries[k].left, queries[k].right));
        }


        timer.end();
        perf.End();
        writer->write(chunk_size);
        writer->write(column);
        writer->write(size);
        writer->write(timer.getDiff());
        writer->write(perf.GetResults()[1]);
        writer->write(perf.GetResults()[0]);
        writer->write("\n");
        timer.printDiff();
        cout << "dummy " << sum << endl << "Chunk Loaded " << tqh.GetChunkNumber() << endl ;
    }

    delete writer;
}

int main(int argc, char *argv[]){
    exp1();
    return 0;
}