//
// Created by Xinding Wei on 16/9/1.
//
#include "../queryhandler.h"
#include "../tools/perf.hh"
#include "../tools/timer.hh"
#include "../tools/csv.hh"
#include <string>
#include <iostream>
#include <memory>
using namespace std;

int main(int argc, char *argv[]){
    long int column = 25, size = pow(10, atoi(argv[1])), chunk_size;
    stringstream ss;
    ss << "../result/exp_load_" << argv[1];
    string filename = ss.str();
    auto mat = make_shared<vector<vector<DataType>>>();
    mat->resize(column);
    for(int i = 0; i < column; i++){
        mat->at(i).resize(size);
        for (int j = 0; j < size; j++)
            mat->at(i)[j] = rand();
    }
    Csv* writer = new Csv(filename);
    writer->write("Chunk size, Column, Size, Time, Cache misses, Cpu cycles, Fetch Time\n");
    Perf perf("cpu-cycles,cache-misses,cache-references",false);
    Timer timer;
    auto data = make_shared<MatrixData<DataType>>(mat, NullNames(column));
    shared_ptr<QueryHandler<DataType>> a;
    {
        a = make_shared<QueryHandler<DataType>>(data);
        chunk_size = 100;
        cout << "For " << chunk_size << " chunk size:\n";
        double fetch_time = 0;
        timer.start();
        perf.Start();
        for (int i = 0; i < column; i++) {
            fetch_time += a->BuildColumn(i, -1, SUM, chunk_size);
            fetch_time += a->BuildColumn(i, -1, MAX, chunk_size);
            fetch_time += a->BuildColumn(i, -1, MIN, chunk_size);
            fetch_time += a->BuildColumn(i, -1, SQUARE_SUM, chunk_size);
            for (int j = i + 1; j < column; j++)
                fetch_time += a->BuildColumn(i, j, MUL_SUM, chunk_size);
        }
        perf.End();
        timer.end();
        writer->write(chunk_size);
        writer->write(column);
        writer->write(size);
        writer->write(timer.getDiff());
        writer->write(perf.GetResults()[1]);
        writer->write(perf.GetResults()[0]);
        writer->write(fetch_time);
        writer->write("\n");
        timer.printDiff();
    }
    delete writer;
    return 0;
}