// Created by Xinding Wei on 16/9/1.
//
#include "../queryhandler_mod1.h"
#include "../tools/timer.hh"
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <cstdio>
using namespace std;


struct Query{
    StatisticTypeEnum statistic_type;
    int col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};


int main(int argc, char *argv[]){
    srand(123);
    int chunk_size, row_number, col_number;
    string query_filename;
    if (argc == 5){
        col_number = atoi(argv[1]);
        row_number = atoi(argv[2]);
        chunk_size = atoi(argv[3]);
        query_filename = argv[4];

        cout << "Col: " << col_number << endl;
        cout << "Query File is " << query_filename << endl;
        cout << "Chunk Size is " << chunk_size << endl;

    }else{
        cout<<"Usage ./test <col number> <row number> <chunk size> <chunk_size>"<<endl;
        return 1;
    }


    shared_ptr<Data<double>> data = GenerateDummyMatrixData<double>(col_number, row_number);

    FILE *fin_query;

    fin_query = fopen(query_filename.c_str() , "r");

    vector<Query> queries;
    Query tmp;
    while(!feof(fin_query)){
        fscanf(fin_query, "%d %d %d %d %d\n", &tmp.statistic_type, &tmp.col1, &tmp.col2, &tmp.left, &tmp.right);
        queries.push_back(tmp);
    }

    fclose(fin_query);

    cout << "Number of Queries: " << queries.size() << endl;

    vector<double> dummy;
    dummy.resize(queries.size());
    QueryHandlerMod1<double> qh(data);
    qh.SetChunkSize(chunk_size);
    Timer timer;
    timer.start();
    int64_t value_involved = 0;
    for (int i = 0; i < queries.size(); i++){
        if(queries[i].statistic_type == -1){
            timer.end();
            timer.printDiff();
            cout << " " << qh.GetChunkNumber() << " " << value_involved << endl;
            timer.start();
        }
        else{
            dummy[i] = qh.QueryStatistic(queries[i].col1, queries[i].col2,
                                         queries[i].statistic_type,
                                         queries[i].left, queries[i].right);
            int n = queries[i].right - queries[i].left;
            switch (queries[i].statistic_type){
                case MEAN:
                    value_involved += n;
                    break;
                case VARIANCE: {
                    value_involved += 2 * n;
                    break;
                }
                case STANDARD_DEVIATION: {
                    value_involved += 2 * n;
                    break;
                }
                case CORRELATION: {
                    value_involved += 5 * n;
                    break;
                }
                case COVARIANCE: {
                    value_involved += 3 * n;
                    break;
                }
            }
        }
    }
    timer.end();
    timer.printDiff();
    cout << " " << qh.GetChunkNumber() << " " << value_involved << endl;
    return 0;
}