// Created by Xinding Wei on 16/9/1.
//
#include "../queryhandler.h"
#include "../tools/timer.hh"
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <cstdio>
#include "../tools/memory_usage.h"
using namespace std;


struct Query{
    StatisticTypeEnum statistic_type;
    int col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};

void ExecuteQueries(QueryHandler<double> &qh, const string& query_filename){
    double ans = 0;
    cout << "Query File is " << query_filename << endl;

    FILE *fin_query;

    fin_query = fopen(query_filename.c_str() , "r");

    vector<Query> queries;
    Query tmp;
    while(!feof(fin_query)){
        fscanf(fin_query, "%d %d %d %d %d\n", &tmp.statistic_type, &tmp.col1, &tmp.col2, &tmp.left, &tmp.right);
        queries.push_back(tmp);
    }

    fclose(fin_query);
    cout << "Number of Queries: " << queries.size() << endl;
    vector<double> dummy;
    dummy.resize(queries.size());
    Timer timer;

    for (int i = 0; i < queries.size(); i++){
        timer.start();

        dummy[i] = qh.QueryStatistic(queries[i].col1, queries[i].col2,
                                     queries[i].statistic_type,
                                     queries[i].left, queries[i].right);
        timer.end();
        timer.printDiff();
        cout << endl;
    }
//    cout << ans << endl;
//    return ans;
}

int main(int argc, char *argv[]){
    srand(123);
    int chunk_size, row_number, col_number;
    string query_filename;
    if (argc >= 5){
        col_number = atoi(argv[1]);
        row_number = atoi(argv[2]);
        chunk_size = atoi(argv[3]);
        query_filename = argv[4];

        cout << "Chunk Size is " << chunk_size << endl;

    }else{
        cout<<"Usage ./test <col number> <row number> <chunk size> <query filename>"<<endl;
        return 1;
    }

    double ans = 0;
    shared_ptr<MatrixData<double>> data = GenerateDummyMatrixData<double>(col_number, row_number);

    {
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");

        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_2");

        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_3");

        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_4");
    }

    col_number >>= 2;
    row_number >>= 2;
    data = GenerateDummyMatrixData<double>(col_number, row_number);

    {
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");
    }

    {
        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_2");
    }

    {
        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_3");
    }

    {
        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_4");
    }

    col_number >>= 2;
    row_number >>= 2;
    data = GenerateDummyMatrixData<double>(col_number, row_number);
    {
        QueryHandler<double> qh(data);
        qh.SetChunkSize(chunk_size);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");

        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");

        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");

        row_number <<= 1;
        data->SetNumberOfRows(row_number);
        col_number <<= 1;
        data->SetNumberOfColumns(col_number);
        cout << "Col: " << col_number << endl;
        cout << "Row: " << row_number << endl;
        ExecuteQueries(qh, query_filename + "_1");
    }
//    cout << ans;
    return 0;
}