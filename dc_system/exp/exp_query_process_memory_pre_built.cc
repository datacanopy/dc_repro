// Created by Xinding Wei on 16/9/1.
//
#include "../queryhandler.h"
#include "../tools/timer.hh"
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <cstdio>
#include "../tools/memory_usage.h"
using namespace std;


struct Query{
    StatisticTypeEnum statistic_type;
    int col1, col2, left, right;

    Query(){
        statistic_type = col1 = col2 = left = right = 0;
    }
    Query(const Query& x){
        (*this) = x;
    }

    Query& operator = (const Query& x){
        statistic_type = x.statistic_type;
        col1 = x.col1;
        col2 = x.col2;
        left = x.left;
        right = x.right;
    }
    bool operator == (const Query& x) const{
        return statistic_type == x.statistic_type
               && col1 == x.col1
               && col2 == x.col2
               && left == x.left
               && right == x.right;
    }
};


int main(int argc, char *argv[]){
    srand(123);
    int chunk_size, row_number, col_number;
    long long memory_limit = -1;
    string query_filename;
    if (argc >= 5){
        col_number = atoi(argv[1]);
        row_number = atoi(argv[2]);
        chunk_size = atoi(argv[3]);
        query_filename = argv[4];
        if (argc > 5)
            memory_limit = atoll(argv[5]);

        cout << "Col: " << col_number << endl;
        cout << "Query File is " << query_filename << endl;
        cout << "Chunk Size is " << chunk_size << endl;
        cout << "Memory limit is " << memory_limit << "bytes" << endl;

    }else{
        cout<<"Usage ./test <col number> <row number> <chunk size> <query filename> <memory limit>"<<endl;
        return 1;
    }

    shared_ptr<Data<double>> data = make_shared<BufferPoolData<double>>(col_number, row_number);

    FILE *fin_query;

    fin_query = fopen(query_filename.c_str() , "r");

    vector<Query> queries;
    Query tmp;
    while(!feof(fin_query)){
        fscanf(fin_query, "%d %d %d %d %d\n", &tmp.statistic_type, &tmp.col1, &tmp.col2, &tmp.left, &tmp.right);
        queries.push_back(tmp);
    }

    fclose(fin_query);

    cout << "Number of Queries: " << queries.size() << endl;

    vector<double> dummy;
    dummy.resize(queries.size());
    Timer timer;


    {
        double ans = 0;
        QueryHandler<double> qh(data);
        qh.SetChunkSize(row_number);
        for (int i = 0; i < queries.size(); i++) {
            timer.start();

            dummy[i] = qh.QueryStatistic(queries[i].col1, queries[i].col2,
                                         queries[i].statistic_type,
                                         queries[i].left, queries[i].right);
            timer.end();
            ans += timer.getDiff();
        }
        cout << ans << endl;
    }

    QueryHandler<double> qh(data);
    qh.SetChunkSize(chunk_size);

    {
        for (int i = 0; i < col_number; i++) {
            for (int j = i + 1; j < col_number; j++)
                qh.QueryStatistic(i, j, CORRELATION, 0, row_number);
        }
    }

    double ans = 0;
    long long now = memory_limit;
    ans = 0;
    qh.SetMemoryLimit(now);
        for (int i = 0; i < queries.size(); i++) {
            timer.start();

            dummy[i] = qh.QueryStatistic(queries[i].col1, queries[i].col2,
                                         queries[i].statistic_type,
                                         queries[i].left, queries[i].right);
            timer.end();
            ans += timer.getDiff();
        }
    cout << ans << endl;
    return 0;
}