#include <iostream>
#include "segmenttree.h"
#include <random>
#include "queryhandler.h"
#include "queryhandler_mod1.h"
using namespace std;

int size = 10000;
int query_size = 1000;

void TestSegmentTreeQuery(std::shared_ptr<SegmentTree<int64_t>> a) {
    vector<int> tmp;
    for (int i = 0; i < size; i++) {
        tmp.push_back(1);//rand());
        a->Insert(i, tmp.back());
    }
    for (int i = 0; i < query_size; i++) {
        int l = rand() % size, r = rand() % size;
        if (l > r)
            swap(l, r);
        auto v = a->Query(l, r);
//        if(!v.first->empty()){
//            for (auto element : *v.first)
//                cout << element.first << ' ' << element.second << endl;
//        }
        assert(v.first->empty());
        int64_t c = 0;
        for (int j = l; j <= r; j++)
            c += tmp[j];
        assert(v.second == c);
    }
}

void TestSegmentTreeEmpty(std::shared_ptr<SegmentTree<int64_t>> a) {
    vector<int64_t> exist;
    exist.resize(size + 1);
    for (int i = 0; i < size; i++) {
        int w = rand() % size;
        a->Insert(w, rand());
        exist[w] = true;
    }

    for (int k = 0; k <= query_size; k++) {
        int l = rand() % size, r = rand() % size;
        if (l > r)
            swap(l, r);
        auto v = a->Query(l, r);
        int now = 0;
        for (int i = l; i <= r; i++)
            if (!exist[i]) {
                int j = i;
                while (j <= r && !exist[j])
                    j++;
                j--;
                assert(v.first->at(now).first == i && v.first->at(now).second == j);
                i = j;
                now++;
            }
    }
}

void TestSegmentTreeBulk(std::shared_ptr<SegmentTree<int64_t>> a) {
    vector<bool> exist;
    vector<int64_t> tmp;
    tmp.resize(size);
    exist.resize(size);
    for (int i = 0; i < size; i++) {
        int w = rand() % (size + 1);
        tmp[w] = rand();
        exist[w] = true;
    }
    vector<std::pair<int64_t, int64_t>> values;
    for (int i = 0; i < size; i++)
        if (exist[i])
            values.push_back(make_pair(i, tmp[i]));
    a->BulkInsert(values);
    for (int k = 0; k <= query_size; k++) {
        int l = rand() % size, r = rand() % size;
        if (l > r)
            swap(l, r);
        auto v = a->Query(l, r);
        int now = 0;
        int64_t count = 0;
        for (int i = l; i <= r; i++)
            if (!exist[i]) {
                int j = i;
                while (j <= r && !exist[j])
                    j++;
                j--;
                assert(v.first->at(now).first == i && v.first->at(now).second == j);
                i = j;
                now++;
            } else {
                count += tmp[i];
            }
        if (now == 0) {
            assert(v.second == count);
        }
    }
}

void TestConvert() {

    std::shared_ptr<SegmentTree<int64_t>> a;
    a = make_shared<LinkSegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));
    vector<bool> exist;
    vector<int64_t> tmp;
    tmp.resize(size);
    exist.resize(size);
    for (int i = 0; i < size; i++) {
        int w = rand() % size;
        tmp[w] = rand();
        exist[w] = true;
    }
    vector<std::pair<int64_t, int64_t>> values;
    for (int i = 0; i < size; i++)
        if (exist[i])
            values.push_back(make_pair(i, tmp[i]));
    a->BulkInsert(values);
    auto s = a->size();
    a = ConvertLinkToArray(dynamic_pointer_cast<LinkSegmentTree<int64_t>>(a));
    cout << "size: " << s << ' ' << a->size() << endl;
    assert(s == a->size());
    for (int k = 0; k <= query_size; k++) {
        int l = rand() % size, r = rand() % size;
        if (l > r)
            swap(l, r);
        auto v = a->Query(l, r);
        int now = 0;
        int64_t count = 0;
        for (int i = l; i <= r; i++)
            if (!exist[i]) {
                int j = i;
                while (j <= r && !exist[j])
                    j++;
                j--;
                assert(v.first->at(now).first == i && v.first->at(now).second == j);
                i = j;
                now++;
            } else {
                count += tmp[i];
            }
        if (now == 0) {
            assert(v.second == count);
        }
    }
}

void TestQueryHandler() {
    int row = 100000;
    int column = 10;
    int query_times = 100000;
    auto matrix = make_shared<vector<vector<int64_t>>>();
    matrix->resize(column);
    for (int i = 0; i < column; i++) {
        matrix->at(i).resize(row);
        for (int j = 0; j < row; j++)
            matrix->at(i)[j] = rand() % 100;
    }
    QueryHandler<int64_t> xxx(
            dynamic_pointer_cast<Data<int64_t>>(make_shared<MatrixData<int64_t>>(matrix, NullNames(column))));
    while (query_times--) {
        int pos[3];
        do {
            for (int i = 0; i < 3; i++)
                pos[i] = rand() % row;
            sort(pos, pos + 3);
        } while (pos[0] == pos[1] || pos[1] == pos[2]);
        int col = rand() % column;
        int64_t a1 = xxx.Query(col, 0, SUM, pos[0], pos[1]);
        int64_t count = 0;
        for (int i = pos[0]; i < pos[1]; i++)
            count += matrix->at(col)[i];
        assert(count == a1);
        int64_t a2 = xxx.Query(col, 0, SUM, pos[1], pos[2]);
        int64_t a3 = xxx.Query(col, 0, SUM, pos[0], pos[2]);
        assert(a1 + a2 == a3);
    }

    QueryHandler<int64_t> yyy(
            dynamic_pointer_cast<Data<int64_t>>(make_shared<MatrixData<int64_t>>(matrix, NullNames(column))));
    for (int i = 0; i < column; i++)
        yyy.BuildColumn(i, 0, MUL_SUM, 50);
    query_times = query_size;
    while (query_times--) {
        int pos[3];
        do {
            for (int i = 0; i < 3; i++)
                pos[i] = rand() % row;
            sort(pos, pos + 3);
        } while (pos[0] == pos[1] || pos[1] == pos[2]);
        int col = rand() % column;
        int64_t a1 = yyy.Query(col, 0, SUM, pos[0], pos[1]);
        int64_t count = 0;
        for (int i = pos[0]; i < pos[1]; i++)
            count += matrix->at(col)[i];
        assert(count == a1);
        int64_t a2 = yyy.Query(col, 0, SUM, pos[1], pos[2]);
        int64_t a3 = yyy.Query(col, 0, SUM, pos[0], pos[2]);
        assert(a1 + a2 == a3);
    }


}

void TestRandomInsert(std::shared_ptr<SegmentTree<int64_t>> a){
    vector<int64_t> tmp;
    for (int64_t i = 0; i < size; i++)
        tmp.push_back(i);
    random_shuffle(tmp.begin(), tmp.end());
    for (int64_t i = 0; i < size; i++)
        a->Insert(tmp[i], i);
    auto result = a->Query(0, size - 1);
    assert(result.first->empty());
}

int main() {
//    TestQueryHandler();
//    return 0;
    std::cout << "Hello, World!" << std::endl;
//    auto x = ConvertLinkToArray(make_shared<LinkSegmentTree<int64_t>>(size, make_shared<SumSupporter<int64_t>>()));
    std::shared_ptr<SegmentTree<int64_t>> a;

//    a = make_shared<ArraySegmentTree<int64_t>>(0, size - 1, dynamic_pointer_cast<Supporter<int64_t>>(
//            make_shared<SumSupporter<int64_t>>(
//            )));

//    TestRandomInsert(a);

//    return 0;

    cout << "Test Link-based Query\n";
    a = make_shared<LinkSegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));

    TestSegmentTreeQuery(a);

    cout << "Test Link-based Empty\n";
    a = make_shared<LinkSegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>()));

    TestSegmentTreeEmpty(a);

    cout << "Test Link-based Bulk\n";
    a = make_shared<LinkSegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));

    TestSegmentTreeBulk(a);

    cout << "Test Array-based Query\n";
    a = make_shared<ArraySegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));

    TestSegmentTreeQuery(a);

    cout << "Test Array-based Empty\n";
    a = make_shared<ArraySegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));

    TestSegmentTreeEmpty(a);

    cout << "Test Array-based Bulk\n";
    a = make_shared<ArraySegmentTree<int64_t>>(size, dynamic_pointer_cast<Supporter<int64_t>>(
            make_shared<SumSupporter<int64_t>>(
            )));

    TestSegmentTreeBulk(a);

    cout << "Test Convert\n";
    TestConvert();

    cout << "Test Query Handler\n";
    TestQueryHandler();

    return 0;
}