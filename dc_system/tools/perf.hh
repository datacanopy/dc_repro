#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <assert.h>
#include <string.h>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

void *status;

class Perf {

    private:
        char *events;

        void runPerf();

        int perf_id;
        bool power;
        int pipefd[2];
        std::vector<long int> results;
    public:
        Perf(char *e, bool p);

        void Start();

        void End();

        vector<long int> GetResults();
};


Perf::Perf(char *e, bool p) {
    events = e;
    power = p;
    pipe(pipefd);
}

void Perf::runPerf() {

    int perf_pid = getppid();
    char perf_pid_opt[9];
    snprintf(perf_pid_opt, 24, "%d", perf_pid);
    char *perfargs_system[7] = {"perf", "stat", "-a", "-e",
                                events, NULL, NULL};

    char *perfargs[9] = {"perf", "stat", "-x,", "-e",
                         events, "-p",
                         perf_pid_opt, NULL, NULL};
    if (power)
        execvp("perf", perfargs_system);
    else
        execvp("perf", perfargs);
    assert(0 && "perf failed to start");

}

/*
*	Use this function to fork a process that runs perf and attaches it to 
*	the current process.
* */
void Perf::Start() {
    int pid = fork();

    if (pid > 0) {
        sleep(1);
        perf_id = pid;
        return;
    } else if (pid == 0) {

        close(pipefd[0]);
        dup2(pipefd[1], 1);
        dup2(pipefd[1], 2);

        runPerf();

    }
    return;
}

/*
*	Use this function to stop profiling using perf
* */

void Perf::End() {
    close(pipefd[1]);
    int status = 0;
    kill(perf_id, SIGINT);
    wait(&status);
    char buffer[1024];

    read(pipefd[0], buffer, sizeof(buffer));

    if (!results.empty()) {
        results.erase(results.begin(), results.end());
    }
    printf(buffer);

    /* A weird parser */
    char *result = strtok(buffer, ",,\n");

    int s = 0;

    while (result != NULL) {
        if (s % 2 == 0) {
            results.push_back(atol(result));
        }

        s++;
        result = strtok(NULL, ",,\n");
    }
    pipe(pipefd);
}

/*
*	Get results as a vector of ints
* */

vector<long int> Perf::GetResults() {
    return results;
}
    
