#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>
#include <iostream>

class Timer {

    private:
        timeval ts_beg;
        timeval ts_end;

    public:
        Timer(){}

        void start(){
            //ts_beg=0;
            gettimeofday(&ts_beg, NULL);
        }

        void end(){
            //ts_end=0;
            gettimeofday(&ts_end, NULL);
        }

        void printDiff(){
            std::cout << getDiff();
        }

        float getDiff(){
            return ts_end.tv_sec - ts_beg.tv_sec + (ts_end.tv_usec - ts_beg.tv_usec) / 1000000.0;;
        }
};


#endif