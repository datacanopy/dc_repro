This folder contains multiple tools that are important to 
profile experiments and report on them. 

timer.hh
-=-=-=-=

A timer wrapper class. 

csv.hh
-=-=-=

A simple CSV writer with two functions:

* write(float* array, int array_size);

Writes an array of int seperated by commas.

* write(String str);

writes any string.

run_perf.hh
-=-=-=-=-=-

A simple perf profiler.

* startPerf

* endPerf

* getResults(): Returns an array of integers. len(array) is 
the number of counters you profiled, and array[i] is the 
value of the ith counter in the order you specified in the 
constructor.

Profiler.hpp
-=-=-=-=-=-=

http://muehe.org/posts/profiling-only-parts-of-your-code-with-perf/