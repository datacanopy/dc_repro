#ifndef CSV_HH
#define CSV_HH

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/**

USAGE: 

Csv* writer = new Csv(filename);
writer->write("blah string");
writer->write(float_array,size);

**/

class Csv {
    private:
        string filename;
        ofstream outdata;

    public:
        Csv(const string &f);

        ~Csv();

        void write(float *data, int size);

        void write(const string &data);

        void write(long int *data, int size);

        void write(double data);
};

Csv::Csv(const string &f) {
    filename = f + ".csv";
    outdata.open(filename.c_str());
}

Csv::~Csv() {
    outdata.close();
}

void Csv::write(double data) {
    outdata << data << ",";
}

void Csv::write(float *data, int size) {

    for (int i = 0; i < size; ++i) {
        outdata << data[i];
        if (i != size - 1)
            outdata << ",";
    }
    outdata << endl;
}

void Csv::write(long int *data, int size) {

    for (int i = 0; i < size; ++i) {
        outdata << data[i];
        if (i != size - 1)
            outdata << ",";
    }
    outdata << endl;
}

void Csv::write(const string &data) {
    outdata << data;
}

#endif
