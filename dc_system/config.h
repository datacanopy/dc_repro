#ifndef CONFIG_H_
#define CONFIG_H_

/*
*	Some typedefs
**/

/*Data is of this type*/
typedef int DataType;

/*Results are of this type*/
typedef float ResultType;

/*Aggregate types*/
typedef float AggregateType;

/*Accumulators used in statistic and aggregate functions are of this type*/
typedef long double AccumulatorType;

/*Size variable*/
typedef int64_t SizeType;

//by byte
#define PAGE_SIZE 4096
#define CONVERT_FACTOR (1.0/3.0)

/** 
*	Aggregate types
*/

//const int SUM = 0;
//const int SUM2 = 1;
//const int SUMXY = 2;

/**
*	Some paramteres
*/

//const SizeType CHUNKSIZE = 100;
//
//
//struct SingleAggregate{
//	int column_id; /*The position in the columns array*/
//	int type;
//
//	long int start;
//	long int end;
//
//	AggregateType value;
//
//};
//
//struct DoubleAggregate{
//	int column1_id; /*The position in the columns array*/
//	int column2_id; /*The position in the columns array*/
//	int type;
//
//	long int start;
//	long int end;
//
//	AggregateType value;
//
//};





#endif