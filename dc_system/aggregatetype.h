//
// Created by Xinding Wei on 16/8/20.
//

#ifndef MST_AGGREGATETYPE_H_H
#define MST_AGGREGATETYPE_H_H

typedef int AggregateTypeEnum;

#define SUM 1
#define SQUARE_SUM 2
#define MIN 3
#define MAX 4
#define MUL_SUM 5
#define COUNT 6

inline bool MultiColumn(AggregateTypeEnum aggregate_type) {
    switch (aggregate_type) {
        case MUL_SUM:
            return true;
        default:
            return false;
    }
}

typedef int StatisticTypeEnum;

#define MEAN 1
#define VARIANCE 2
#define STANDARD_DEVIATION 3
#define CORRELATION 4
#define COVARIANCE 5
#endif //MST_AGGREGATETYPE_H_H
