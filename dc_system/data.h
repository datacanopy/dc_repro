#ifndef DATA_H_
#define DATA_H_


#include "config.h" /*Typedefs and structs*/
#include "aggregatetype.h"
#include "supporter.h"
#include <random>
#include <pqxx/pqxx>
#include <sstream>
#include <unordered_map>


//#include "csv.h" /*LineReader*/



static std::shared_ptr<std::vector<std::shared_ptr<std::string>>> NullNames(SizeType number_of_columns) {
    auto ans = std::make_shared<std::vector<std::shared_ptr<std::string>>>();
    ans->resize(number_of_columns);
    for (int i = 0; i < number_of_columns; i++)
        ans->at(i) = std::make_shared<std::string>("");
    return ans;
}

template<class T>
static T Convert(AggregateTypeEnum aggregate_type, T x, T y) {
    switch (aggregate_type) {
        case SUM:
            return x;
        case SQUARE_SUM:
            return x * x;
        case COUNT:
            return 1;
        case MIN:
            return x;
        case MAX:
            return x;
        case MUL_SUM:
            return x * y;
    }
    return x;
}

template<class T>
struct DataPayload{
        T mul = 0, sq_sum1 = 0, sum1 = 0, sq_sum2 = 0, sum2 = 0;
};

#define MUL_NO 0
#define SUM1_NO 1
#define SQ_SUM1_NO 2
#define SUM2_NO 3
#define SQ_SUM2_NO 4

#define MUL_FLAG (1 << MUL_NO)
#define SUM1_FLAG (1 << SUM1_NO)
#define SQ_SUM1_FLAG (1 << SQ_SUM1_NO)
#define SUM2_FLAG (1 << SUM2_NO)
#define SQ_SUM2_FLAG (1 << SQ_SUM2_NO)

typedef int AggregateTypeFlag;

/**
*	The Data class, which contains an array of columns and functions to generate, load, and access data.
*/
template<class T>
class Data {

    public:

        Data(SizeType number_of_columns, SizeType number_of_rows,
             std::shared_ptr<std::vector<std::shared_ptr<std::string>>> names) : number_of_columns_(number_of_columns),
                                                                                 number_of_rows_(number_of_rows),
                                                                                 names_(names) {
            assert(names_->size() == number_of_columns_);
        }

        virtual ~Data() {}

        /*
        *	Data access functions
        **/

        virtual SizeType GetNumberOfColumns() { return number_of_columns_; }

        virtual SizeType GetNumberOfRows() { return number_of_rows_; }

//        virtual std::shared_ptr<Column<T>> GetColumn(SizeType column_number) = 0;

//        virtual std::shared_ptr<Column<T>> *GetColumn(std::string name) = 0;

        std::shared_ptr<std::vector<std::shared_ptr<std::string>>> GetColumnNames() { return names_; }

        std::shared_ptr<std::string> GetColumnName(SizeType column_number) { return names_->at(column_number); }

        virtual std::shared_ptr<std::vector<T>> FetchRawData(SizeType column, SizeType left, SizeType right) = 0;

        virtual T FetchData(SizeType column1, SizeType column2, AggregateTypeEnum aggregate_type, SizeType left,
                            SizeType right) = 0;

        virtual DataPayload<T> FetchDataCombine(SizeType column1, SizeType column2, int aggregate_flag, SizeType left,
                                         SizeType right) = 0;

    protected:
        SizeType number_of_columns_, number_of_rows_;
    private:
        std::shared_ptr<std::vector<std::shared_ptr<std::string>>> names_;
//		std::vector<Column*> columns_;

};

template<class T>
class DummyData : public Data<T> {
    public:
        DummyData(SizeType number_of_columns, SizeType number_of_rows) : Data<T>(number_of_columns, number_of_rows,
                                                                                 NullNames(number_of_columns)) {
        }

        virtual ~DummyData() {}

//        std::shared_ptr<Column<T>> GetColumn(SizeType column_number) {
//            return dynamic_pointer_cast<Column<T>>(
//                    make_shared<DummyColumn<T>>(this->GetNumberOfRows(), this->GetColumnName(column_number)));
//        }

        std::shared_ptr<std::vector<T>> FetchRawData(SizeType column, SizeType left, SizeType right) override{
            using namespace std;
            auto ans = make_shared<std::vector<T>>();
            ans->resize(right - left + 1);
            for (int i = left; i <= right; i++)
                ans->at(i) = rand();
            return ans;
        }

        T FetchData(SizeType column1, SizeType column2, AggregateTypeEnum aggregate_type, SizeType left,
                    SizeType right) override {
            return rand();
        }

        DataPayload<T> FetchDataCombine(SizeType column1, SizeType column2, int aggregate_flag, SizeType left,
                                         SizeType right) override{
            DataPayload<T> ans;
            ans.sum1 = rand();
            ans.sq_sum1 = rand();
            ans.mul = rand();
            ans.sum2 = rand();
            ans.sq_sum2 = rand();
            return ans;
        }

};

template<class T>
class MatrixData : public Data<T> {
    public:
        // Assume that the lengths of all vector are equal.
        MatrixData(std::shared_ptr<std::vector<std::vector<T>>> matrix,
                   std::shared_ptr<std::vector<std::shared_ptr<std::string>>> names) : Data<T>(matrix->size(),
                                                                                               matrix->front().size(),
                                                                                               names), matrix_(matrix) {
            assert(matrix->size() && matrix->front().size());
            SizeType row = matrix->front().size();
            for (SizeType i = 0; i < matrix->size(); i++)
                assert(matrix->at(i).size() == row);
        }

        ~MatrixData() {}

        std::shared_ptr<std::vector<T>> FetchRawData(SizeType column, SizeType left, SizeType right) override{
            using namespace std;
            auto ans = make_shared<std::vector<T>>();
            ans->resize(right - left + 1);
            for (int i = left; i <= right; i++)
                ans->at(i) = matrix_->at(column)[i];
            return ans;
        }

        T FetchData(SizeType column1, SizeType column2, AggregateTypeEnum aggregate_type, SizeType left,
                    SizeType right) override {
            assert(0 <= column1 && column1 < this->GetNumberOfColumns());
#ifdef DEBUG
            std::cout << "fetch " << aggregate_type << " " << column1 << " " << column2 << " " << left << " " << right << std::endl;
#endif
            T ans;
            if (MultiColumn(aggregate_type)) {
                ans = Convert<T>(aggregate_type, matrix_->at(column1)[left], matrix_->at(column2)[left]);
                for (SizeType i = left + 1; i <= right; i++)
                    ans = Merge<T>(aggregate_type, ans,
                                   Convert<T>(aggregate_type, matrix_->at(column1)[i], matrix_->at(column2)[i]));
            } else {
                ans = Convert<T>(aggregate_type, matrix_->at(column1)[left], 0);
                for (SizeType i = left + 1; i <= right; i++)
                    ans = Merge<T>(aggregate_type, ans, Convert<T>(aggregate_type, matrix_->at(column1)[i], 0));
            }
            return ans;
        }

        DataPayload<T> FetchDataCombine(SizeType column1, SizeType column2, int aggregate_flag, SizeType left,
                                SizeType right) override{
            DataPayload<T> ans;
            bool mul_flag = aggregate_flag & MUL_FLAG;
            bool sum1_flag = aggregate_flag & SUM1_FLAG;
            bool sq_sum1_flag = aggregate_flag & SQ_SUM1_FLAG;
            bool sum2_flag = aggregate_flag & SUM2_FLAG;
            bool sq_sum2_flag = aggregate_flag & SQ_SUM2_FLAG;
            if(mul_flag)
                ans.mul = Convert<T>(MUL_SUM, matrix_->at(column1)[left], matrix_->at(column2)[left]);
            if(sum1_flag)
                ans.sum1 = Convert<T>(SUM, matrix_->at(column1)[left], 0);
            if(sq_sum2_flag)
                ans.sq_sum1 = Convert<T>(SQUARE_SUM, matrix_->at(column1)[left], 0);
            if(sum2_flag)
                ans.sum2 = Convert<T>(SUM, matrix_->at(column2)[left], 0);
            if(sq_sum2_flag)
                ans.sq_sum2 = Convert<T>(SQUARE_SUM, matrix_->at(column2)[left], 0);
            for (SizeType i = left + 1; i <= right; i++){
                if(mul_flag)
                    ans.mul = Merge<T>(MUL_SUM, ans.mul, Convert<T>(MUL_SUM, matrix_->at(column1)[i], matrix_->at(column2)[i]));
                if(sum1_flag)
                    ans.sum1 = Merge<T>(SUM, ans.sum1, Convert<T>(SUM, matrix_->at(column1)[i], 0));
                if(sq_sum1_flag)
                    ans.sq_sum1 = Merge<T>(SQUARE_SUM, ans.sq_sum1, Convert<T>(SQUARE_SUM, matrix_->at(column1)[i], 0));
                if(sum2_flag)
                    ans.sum2 = Merge<T>(SUM, ans.sum2, Convert<T>(SUM, matrix_->at(column2)[i], 0));
                if(sq_sum2_flag)
                    ans.sq_sum2 = Merge<T>(SQUARE_SUM, ans.sq_sum2, Convert<T>(SQUARE_SUM, matrix_->at(column2)[i], 0));
            }
            return ans;
        }

        virtual SizeType GetNumberOfColumns() { return matrix_->size(); }

        virtual SizeType GetNumberOfRows() { return matrix_->front().size(); }

        void SetNumberOfColumns(SizeType columns){
            SizeType old_col = matrix_->size();
            SizeType row = matrix_->at(0).size();
            matrix_->resize(columns);
            for (SizeType i = old_col; i < columns; i++)
                matrix_->at(i).resize(row);
        }

        void SetNumberOfRows(SizeType rows){
            for (SizeType i = 0; i < matrix_->size(); i++)
                matrix_->at(i).resize(rows);
        }

    private:
        std::shared_ptr<std::vector<std::vector<T>>> matrix_;

};

template<class T>
class PostgreSQLData : public Data<T> {
    public:
        PostgreSQLData(SizeType number_of_columns, SizeType number_of_rows, const std::string &dbstring, const std::string &tablename)
                : connect_(dbstring), tablename_(tablename),
                  Data<T>(number_of_columns, number_of_rows,
                                              NullNames(number_of_columns)) {
        }

        virtual ~PostgreSQLData() {
            connect_.disconnect();
        }

//        std::shared_ptr<Column<T>> GetColumn(SizeType column_number) {
//            return dynamic_pointer_cast<Column<T>>(
//                    make_shared<DummyColumn<T>>(this->GetNumberOfRows(), this->GetColumnName(column_number)));
//        }

        std::shared_ptr<std::vector<T>> FetchRawData(SizeType column, SizeType left, SizeType right) override{
            assert(0 && "Not Implement now.");
        }

        T FetchData(SizeType column1, SizeType column2, AggregateTypeEnum aggregate_type, SizeType left,
                    SizeType right) override {
            using namespace std;
            using namespace pqxx;
            stringstream ss;
            ss << "select ";
            switch (aggregate_type){
                case SUM:{
                    ss << "sum(col" << column1 << ") ";
                    break;
                }
                case MIN:{
                    ss << "min(col" << column1 << ") ";
                    break;
                }
                case MAX:{
                    ss << "max(col" << column1 << ") ";
                    break;
                }
                case SQUARE_SUM:{
                    ss << "sum(col" << column1 << " * col" << column1 << ") ";
                    break;
                }
                case MUL_SUM:{
                    ss << "sum(col" << column1 << " * col" << column2 << ") ";
                    break;
                }
            }
            ss << " from test where id between " << left << " and " << right << ";";
            nontransaction N(connect_);
            result R(N.exec(ss.str()));
            return R.begin()[0].as<double>();
        }

        DataPayload<T> FetchDataCombine(SizeType column1, SizeType column2, int aggregate_flag, SizeType left,
                                        SizeType right) override{
            return DataPayload<T>();
        }
    private:
        pqxx::connection connect_;
        const std::string tablename_;

        int GetNumberOfColumn(){
//            using namespace pqxx;
//            nontransaction N(connect_);
//            N.exec( sql );
        }
};

template<class T>
inline std::shared_ptr<MatrixData<T>> GenerateDummyMatrixData(SizeType column, SizeType row){
    using namespace std;
    auto matrix = make_shared<vector<vector<T>>>();
    matrix->resize(column);
    for (int i = 0; i < column; i++) {
        matrix->at(i).resize(row);
        for (int j = 0; j < row; j++)
            matrix->at(i)[j] = rand() % 1000000;
    }
    return make_shared<MatrixData<T>>(matrix, NullNames(column));
}


#ifdef OUT_OF_MEMORY
class LRUQueue{
    public:
        LRUQueue(SizeType n):n_(n){
            first_ = 0;
            last_ = n - 1;
            pred_.resize(n);
            succ_.resize(n);
            flag_.resize(n, true);
            now_ = n;
            for (SizeType i = 0; i < n; i++){
                pred_[i] = i - 1;
                succ_[i] = i + 1;
            }
            succ_[n - 1] = -1;
        }
        void RemoveFromQueue(SizeType col){
            assert(0 <= col && col < n_);
            assert(flag_[col]);
            if(succ_[col] != -1)
                pred_[succ_[col]] = pred_[col];
            if(pred_[col] != -1)
                succ_[pred_[col]] = succ_[col];
            if(first_ == col)
                first_ = succ_[col];
            if(last_ == col)
                last_ = pred_[col];
            succ_[col] = pred_[col] = -1;
            flag_[col] = false;
            now_--;
        }
        void InsertToQueue(SizeType col){
            assert(0 <= col && col < n_);
            assert(!flag_[col]);
            pred_[col] = -1;
            succ_[col] = first_;
            if(first_ != -1) {
                pred_[first_] = col;
            }
            first_ = col;
            flag_[col] = true;
            now_++;
        }
        SizeType get_last(){return last_;}
        bool empty(){ return now_ == 0; }
    private:
        const SizeType n_;
        SizeType first_, last_, now_;
        std::vector<SizeType> pred_, succ_, flag_;
};

template<class T>
class BufferPoolData : public Data<T>{
    public:
        BufferPoolData(SizeType number_of_columns, SizeType number_of_rows):
                Data<T>(number_of_columns, number_of_rows, NullNames(number_of_columns)) {
            if((number_of_rows / chunk_size_) * chunk_size_ < number_of_rows)
                number_of_row_chunk_ = number_of_rows / chunk_size_ + 1;
            else
                number_of_row_chunk_ = number_of_rows / chunk_size_;
            memory_overhead_ = sizeof(BufferPoolData<T>);
            for (SizeType i = 0; i < number_of_columns; i++) {
                for (SizeType j = 0; j < number_of_row_chunk_; j++){
                    data_[i * number_of_row_chunk_ + j] = new T[chunk_size_];
                    memory_overhead_ += sizeof(T) * chunk_size_;
                    for (SizeType k = 0; k < chunk_size_; k++)
                        data_[i * number_of_row_chunk_ + j][k] = rand();
                }
            }
            lru_ = std::make_shared<LRUQueue>(number_of_columns * number_of_row_chunk_);
        }

        void SetLimit(SizeType limit){
            limit_ = limit;
            CheckMemory();
        }

        SizeType get_memory_overhead(){ return memory_overhead_; }

        SizeType LeastMemoryOverhead(){return 0;}

        std::shared_ptr<std::vector<T>> FetchRawData(SizeType column, SizeType left, SizeType right) override{
            assert(0 && "Not Implement now.");
        }

        T FetchData(SizeType column1, SizeType column2, AggregateTypeEnum aggregate_type, SizeType left,
                    SizeType right) override {
            using namespace std;
            assert(0 <= column1 && column1 < this->GetNumberOfColumns());
            double ans = 0;
            SizeType left_page = left / chunk_size_, right_page = right / chunk_size_;
            for (SizeType i = left_page; i <= right_page; i++){
                if (MultiColumn(aggregate_type)){
                    T *p1 = AccessPage(column1 * number_of_row_chunk_ + i), *p2 = AccessPage(column2 * number_of_row_chunk_ + i);
                    for (SizeType j = max(i * chunk_size_, left) - i * chunk_size_; j <= min((i + 1) * chunk_size_ - 1, right) - i * chunk_size_; j++)
                        ans += Convert<T>(aggregate_type, p1[j], p2[j]);
                }
                else{
                    T* p1 = AccessPage(column1 * number_of_row_chunk_ + i);
                    for (SizeType j = max(i * chunk_size_, left) - i * chunk_size_; j <= min((i + 1) * chunk_size_ - 1, right) - i * chunk_size_; j++)
                        ans += Convert<T>(aggregate_type, p1[j], 0);
                }
            }
            return ans;
        }

        DataPayload<T> FetchDataCombine(SizeType column1, SizeType column2, int aggregate_flag, SizeType left,
                                        SizeType right) override{
            assert(0 && "Not Implement now.");
        }


        void Dump(SizeType num){
            using namespace std;
            auto it = data_.find(num);
            assert(it != data_.end());
            stringstream ss;
            ss << "dump_" << num << ".dat";
            FILE *fout = fopen(ss.str().c_str(), "wb");
            fwrite((void*)(it->second), sizeof(T), chunk_size_, fout);
            fclose(fout);
            delete [](it->second);
            data_.erase(it);
            memory_overhead_ -= sizeof(T) * chunk_size_;
            lru_->RemoveFromQueue(num);
        }

        void Restore(SizeType num){
            using namespace std;
            auto it = data_.find(num);
            assert(it == data_.end());
            stringstream ss;
            ss << "dump_" << num << ".dat";
            data_[num] = new T[chunk_size_];
            FILE *fin = fopen(ss.str().c_str(), "rb");
            fread((void*)(data_[num]), sizeof(T), chunk_size_, fin);
            fclose(fin);
            memory_overhead_ += sizeof(T) * chunk_size_;
            lru_->InsertToQueue(num);
            CheckMemory();
        }

        void CheckMemory(){
            while(limit_ != -1 && memory_overhead_ > limit_){
                assert(!lru_->empty());
                Dump(lru_->get_last());
//                lru_->RemoveFromQueue(lru_->get_last());
            }
        }

        inline T* AccessPage(SizeType num){
            auto it  = data_.find(num);
            if (it == data_.end()){
                Restore(num);
                it  = data_.find(num);
            }
            return it->second;
        }

    private:

        //by bytes
        SizeType memory_overhead_, limit_ = -1;
        std::unordered_map<SizeType, T*> data_;
        SizeType chunk_size_ = PAGE_SIZE / sizeof(T);
        SizeType number_of_row_chunk_;
        std::shared_ptr<LRUQueue> lru_;
};

#endif

#endif  // DATA_H_