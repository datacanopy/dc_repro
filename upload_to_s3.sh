# Upload packaged code
# aws s3 cp ./dc_reprozip/dc_reprozip_small.rpz s3://dcreprozip/reprozip/
# aws s3 cp ./dc_reprozip/dc_reprozip_full.rpz s3://dcreprozip/reprozip/

# Upload the script
tar -cvf ./dc_reprozip.tar.gz dc_reprozip/*.sh
#
aws s3 rm s3://dcreprozip/scripts/dc_reprozip.tar.gz
#
aws s3 cp ./dc_reprozip.tar.gz s3://dcreprozip/scripts/
#
rm dc_reprozip.tar.gz
