C='\033[0;35m'
NC='\033[0m' # No Color

echo  -e "${C}[DC] Installing Reprounzip ${NC}"
sudo pip install reprounzip[all]
sudo /usr/local/bin/reprounzip usage_report --disable

#echo -e "${C}[DC] Downloading packaged code ${NC}"
#wget https://bitbucket.org/datacanopy/dc_repro/raw/39ac5f3e29d18cb94b3a06db8054a6f643de2240/dc_reprozip.rpz

echo -e "${C}[DC] Unpacking using Reprounzip ${NC}"
sudo /usr/local/bin/reprounzip chroot setup dc_reprozip.rpz dc_repro

echo -e "${C}[DC] Reprounzip has unpacked our code and environment; you can run the small workflow using 'run_small.sh' OR the full version using 'run_full.sh'${NC}"
