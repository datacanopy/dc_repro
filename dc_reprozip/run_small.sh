C='\033[0;35m'
NC='\033[0m' # No Color

echo  -e "${C}[DC] Downloading the packaged code [small] ${NC}"

wget ""

echo  -e "${C}[DC] Running [small] ${NC}"

sudo /usr/local/bin/reprounzip chroot run dc_repro --cmdline python run.py small
cp -r dc_repro/root/home/awasay/temp/repro_test/dc_repro/graph/ ./graph
