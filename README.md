# Data Canopy (SIGMOD 2017 reproducibility) #
Data Canopy (Link to paper): 
https://dl.acm.org/doi/10.1145/3035918.3064051

This repo contains the source code to run all experiments and generate all experimental 
graphs presented in our paper. We use reprozip to package our code and take care of all 
dependencies. Here, we provide an overview of the experiments and instructions to run 
the packaged code. 

### Overview ###
We provide two versions of the experiments:

* The small version of the experiments (with reduced data sets and workloads) that runs in less than 30 minutes and requires a main memory size of 32GB. 
* The full version of the experiments that runs in X hours and requires a main memory size of 800 GB. 

We highly recommend that you run the small version of the experiments first to test 
the script before running the full version of the experiments.

### Hardware requirements ###

In our paper, all experiments are conducted on a server with an Intel Xeon CPU 
E7-4820 processor, running at 2 GHz with 16 MB L3 cache and 1 TB of main memory. 
This server machine runs Debian OS with kernel 3.16.7 and is configured with a 
hard disk of 300GB operating at 15KRPM. 

We also tested the small version on AWS machine m4.2xlarge (32 GB of main memory). 
To run the full version of the experiments, you will need a machine with 800 GB 
of main memory.

### Required Packages ###

* Programming Language: C, C++, and Python (2.7.x)
* Compiler Information: GCC (4.9.2)
* Required Packages: libpqxx, MonetDB, NumPy, Pandas, R (3.3.1), ModelTools.

We use reprozip to package our code and it takes care of all the aforementioned 
dependencies.


### Running using reprounzip ###

#### Step 0: Download code and scripts ####

Download the packaged code and the scripts (setup.sh, run_small.sh, run_full.sh, and clean_up.sh):

```
wget https://s3-us-west-2.amazonaws.com/dcreprozip/scripts/dc_reprozip.tar.gz
tar -xvf dc_reprozip.tar.gz
rm dc_reprozip.tar.gz
cd dc_reprozip
```

#### Step 1: Setup ####

Install reprounzip, and unpack the code:

```
bash setup.sh
```

#### Step 2a: Run small version ####

```
bash run_small.sh
```

#### Step 2b: Run full version ####

```
bash run_full.sh
```

#### Step 3: Clean up ####

Clean up directories created by reprounzip and chroot:

```
bash clean_up.sh
```
#### Results ####
On the successful completion of our script, all resulting figures will be in the 'graph' 
folder. The name of every graph corresponds to the name of the figure in the paper. For 
example, 'figure12.pdf' corresponds to Figure 12 in the paper. 

Please note that only experimental graphs from the main paper that evaluate as well as 
compare Data Canopy with other systems will be generated i.e., Figure 11, 12, 13, 14, 
15, and 16. Other graphs are motivational or analytical graphs. 

### Contact ###

If you have any questions, please reach out to Abdul Wasay (aw.awasay@gmail.com). 